<?php
// Create connection
include("../config.php"); 

include_once "excel/class.writeexcel_workbook.inc.php";
require_once "excel/class.writeexcel_worksheet.inc.php";




	$query = "SELECT us.id as Advertiserid,us.uname as AdvertiserName,cam.title as Campaign,cam.id as Campaignid,us.email as Email,us.paypal_email as PaypalEmail,ecb.budget as Amount ,cam.created as Date
                        FROM espy_campaign AS cam
                        JOIN ad_signup AS us ON cam.advertiser_id = us.id
                        JOIN espy_campaign_budget AS ecb ON cam.id = ecb.campaign_id ";
	$result = mysql_query($query);
	$row = mysql_fetch_assoc($result);

$fname = tempnam("/tmp", "stocks.xls");
$workbook = &new writeexcel_workbook($fname);
$worksheet =& $workbook->addworksheet();



# Set the column width for columns 1, 2, 3 and 4
$worksheet->set_column(0, 0, 30);
$worksheet->set_column(0, 1, 30);
$worksheet->set_column(0, 2, 35);
$worksheet->set_column(0, 3, 34);
$worksheet->set_column(0, 4, 60);
$worksheet->set_column(0, 5, 20);
$worksheet->set_column(0, 6, 20);
$worksheet->set_column(0, 7, 20);


# Create a format for the column headings
$header =& $workbook->addformat(array('align' => 'center', 'bold' => 1));
$header->set_bold();
$header->set_size(12);
$header->set_color('blue');


# Create a format for the stock price
$f_price =& $workbook->addformat();
$f_price->set_align('center');
$f_price->set_num_format('$0.00');

# Create a format for the stock volume
$f_volume =& $workbook->addformat();
$f_volume->set_align('center');

$email =& $workbook->addformat();
$email->set_size(10);

# Create a format for the price change. This is an example of a conditional
# format. The number is formatted as a percentage. If it is positive it is
# formatted in green, if it is negative it is formatted in red and if it is
# zero it is formatted as the default font colour (in this case black).
# Note: the [Green] format produces an unappealing lime green. Try
# [Color 10] instead for a dark green.
#
$f_change =& $workbook->addformat();
$f_change->set_align('left');
$f_change->set_num_format('[Green]0.0%;[Red]-0.0%;0.0%');

# Write out the data


/************************ This code is for header display *******************/
   $i=="0";

$top = array ('Advertiser id', 'Advertiser Name', 'Campaign', 'Campaign id','Email', 'Paypal Email','Amount', 'Date');

	foreach($top as $name )
	{
	  $worksheet->write(0, $i, $name, $header);
	$i++;
	}




/************************ This code is for header display **********************/


     

/************************ This code is for all rows display **********************/


	$k="1";
	 while($row = mysql_fetch_assoc($result))
	{
     $j=0;
foreach($row as $name => $value)
	{


         $worksheet->write($k, $j, $value, $f_volume);


	$j++;
	}
     $k++;
	}

	/************************ This code is for all rows display *********************/

$workbook->close();

header("Content-Type: application/x-msexcel; name=\"payment.xls\"");
header("Content-Disposition: inline; filename=\"payment.xls\"");
$fh=fopen($fname, "rb");
fpassthru($fh);
unlink($fname);

?>
