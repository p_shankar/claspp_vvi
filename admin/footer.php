<hr>
  <div class="modal hide fade" id="myModal">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">�</button>
      <h3>Settings</h3>
    </div>
    <div class="modal-body">
      <p>Here settings can be configured...</p>
    </div>
    <div class="modal-footer"> <a href="#" class="btn" data-dismiss="modal">Close</a> <a href="#" class="btn btn-primary">Save changes</a> </div>
  </div>
  <footer>
    <p class="pull-left">&copy; <a href="http://www.claspp.com" target="_blank">Claspp</a> 2012</p>
  </footer>
</div>
<!--/.fluid-container--> 

<!-- external javascript
	================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 

<!-- jQuery --> 
<script src="js/jquery-1.7.2.min.js"></script> 
<!-- jQuery UI --> 
<script src="js/jquery-ui-1.8.21.custom.min.js"></script> 
<!-- custom dropdown library --> 
<script src="js/bootstrap-dropdown.js"></script> 
<!-- library for creating tabs --> 
<script src="js/bootstrap-tab.js"></script> 
<!-- library for advanced tooltip --> 
<script src="js/bootstrap-tooltip.js"></script> 
<!-- popover effect library --> 
<script src="js/bootstrap-popover.js"></script> 
<!-- accordion library (optional, not used in demo) --> 
<script src="js/bootstrap-collapse.js"></script> 
<!-- library for cookie management --> 
<script src="js/jquery.cookie.js"></script> 
<!-- data table plugin --> 
<script src='js/jquery.dataTables.min.js'></script> 
<!-- select or dropdown enhancer --> 
<script src="js/jquery.chosen.min.js"></script> 
<!-- checkbox, radio, and file input styler --> 
<script src="js/jquery.uniform.min.js"></script> 
<!-- plugin for gallery image view --> 
<script src="js/jquery.colorbox.min.js"></script> 
<!-- rich text editor library --> 
<script src="js/jquery.cleditor.min.js"></script> 
<!-- file manager library --> 
<script src="js/jquery.elfinder.min.js"></script> 
<!-- star rating plugin --> 
<script src="js/jquery.raty.min.js"></script> 
<!-- for iOS style toggle switch --> 
<script src="js/jquery.iphone.toggle.js"></script> 
<!-- autogrowing textarea plugin --> 
<script src="js/jquery.autogrow-textarea.js"></script> 
<!-- multiple file upload plugin --> 
<script src="js/jquery.uploadify-3.1.min.js"></script> 
<!-- history.js for cross-browser state change on ajax --> 
<script src="js/jquery.history.js"></script> 
<!-- application script for Charisma demo --> 
<script src="js/charisma.js"></script>
</body>
</html>