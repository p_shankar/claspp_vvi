<?php
/*
|-----------------
| Chip Error Manipulation
|------------------
*/

error_reporting(-1);

/*
|-----------------
| Chip Constant Manipulation
|------------------
*/

define( "CHIP_DEMO_FSROOT",				__DIR__ . "/" );

/*
|-----------------
| Chip CSV Class
|------------------
*/

require_once("class.chip_csv.php");
$object = new Chip_csv();

/*
|---------------------------
| PHP Array to write in CSV file
|---------------------------
*/

$csv_write_array = array(
	0	=>	array( "Name", "Code", "Price" ),
	1	=>	array( "Apple", "1001", "$1" ),
	2	=>	array( "Orange", "1002", "$2" ),
	3	=>	array( "Banana", "1003", "$0.50" ),
	4	=>	array( "Grapes", "1004", "$5" ),
);

/*
|---------------------------
| CSV File to Write
|---------------------------
*/

$csv_file = CHIP_DEMO_FSROOT . "write/fruits.csv";

/*
|---------------------------
| CSV Inputs
|---------------------------
*/

$args = array (
		'csv_file'			=>	$csv_file,
		'csv_delimiter'		=>	",",
		'csv_write_array'	=>	$csv_write_array,
	);

$csv_output = $object->get_write( $args );
//$object->chip_print( $csv_output );

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" media="all" href="style.css" />
<title>Chip CSV Parser</title>
</head>

<body>

<div id="wrap">
  <div id="wrapdata">
    
    
    <div id="header">
      <div id="headerdata">      
        
        <div class="chipboxw1 chipstyle1">
          <div class="chipboxw1data">          
            <h2 class="margin0">Chip CSV Parser</h2>
          </div>
        </div>
        
            
      </div>
    </div>
    
    <div id="content">
      <div id="contentdata">
        
        <?php if( $csv_output['csv_file_write'] == TRUE ): ?>
        <div class="chipboxw1 chipstyle2">
          <div class="chipboxw1data">          
            <h2 class="margin0">File Written Successfully</h2>
          </div>
        </div>
        <?php endif; ?>
        
        <div class="chipboxw1 chipstyle1">
          <div class="chipboxw1data">          
              CSV Parser Writing Demo
          </div>
        </div>
        
      </div>
    </div>
    
     <div id="footer">
      <div id="footerdata">
        
        <div class="chipboxw1 chipstyle1">
          <div class="chipboxw1data">          
            &copy; <a href="http://www.tutorialchip.com/">TutorialChip</a>
          </div>
        </div>
        
      </div>
    </div>
    
  </div>
</div>

</body>
</html>