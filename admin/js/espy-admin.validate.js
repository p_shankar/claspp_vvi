/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 */


$('document').ready(function(){

    /*******************************************************************
  *********** script to get site url in admin panel ************
  ******************************************************************/
    var loc = window.location;
    var admin_url = loc.protocol + "//" + loc.host;
    var paths = loc.pathname.split('/');
    var total = paths.length;
    $.each(paths, function(key, path) {
        if(key < (total-1) ){
            admin_url = admin_url+path+'/';
        }
    });
   

    /*******************************************************************
    ******************* navigation menu *****************************
    *****************************************************************
   
    $('.menu_tab').click(function(){
        $('.current').removeClass('current');
        $('.display_content').removeClass('display_content');
        $(this).parent().addClass('current');
        var show_tab_class = $(this).attr('rel');
       
        $(show_tab_class).addClass('display_content'); 
	$(show_tab_class+'_content').addClass('display_content');
    });*/

   
   
    /*******************************************************************
    ******************* view user info*****************************
    *****************************************************************
   
    $('.view_user').click(function(){
	
        var view_id = $(this).attr('id');
 	var parent = $(this).parent();
	
        $.ajax({
            type: 'POST',
            
            url: admin_url+'includes/espy-ajax.php',
            data: { 
                'view_id': view_id ,
                'action': 'viewUser'
            },
            success: function(msg){

                parent.after('<tr><td colspan="5">'+msg+'</td></tr>');
            }
        });
    });*/

    /*******************************************************************
    ******************* view advertiser *****************************
    *****************************************************************
    $('.view_advt').click(function(){
        var view_id = $(this).attr('id');
        $.ajax({
            type: 'POST',
            
            url: admin_url+'includes/espy-ajax.php',
            data: { 
                'view_id': view_id ,
                'action': 'viewAdvt'
            },
            success: function(msg){
                
                parent.after('<tr><td colspan="5">'+msg+'</td></tr>');
            }
        });
    });*/
   
   
   
   
    /*******************************************************************
    ******************* delete user *****************************
    ******************************************************************/
    $(".delete_user").click(function(){

        var cnfm = confirm("Are you sure ?");
        if (cnfm==true)
        {
        var d_id = $(this).attr('id');
        
        $.ajax({
            type: 'POST',
            
            url: admin_url+'includes/espy-ajax.php',
            data: { 
                'delete_id': d_id ,
                'action': 'deleteUser'
            },
            success: function(msg){
               
                if(msg == -1){                    
                    $('#admin_table_1').load(admin_url+'home.php?page=users #admin_table_1',
                        function() {


                        });                 
                }
            }
        });
        }
    });
    
    /*******************************************************************
    ******************* delete advertiser *****************************
    ******************************************************************/
    $(".delete_advt").click( function(){

         var cnfm = confirm("Are you sure ?");
        if (cnfm==true)
        {
        var d_id = $(this).attr('id');
        
        $.ajax({
            type: 'POST',
            
            url: admin_url+'includes/espy-ajax.php',
            data: { 
                'delete_id': d_id ,
                'action': 'deleteAdvt'
            },
            success: function(msg){
                
                if(msg == -1 ){
                    $('#admin_table_2').load(admin_url+'home.php?page=advt #admin_table_2',
                        function() {
                        
                        });                 
                }
            }
        });
        }
    });
   

    /*******************************************************************
	 ******************* admin-left-menu tabs **************************
	 *****************************************************************
	    $('#related-act-inner a').click(function(){
		var tab_id = $(this).attr('id');
		var tab_rel = $(this).attr('rel');
		$('.'+tab_id.substring(5)+'_content'+' div').hide(); 
		$('.'+tab_id.substring(5)+'_content'+' div'+tab_rel).show(); 
		$('.'+tab_id.substring(5)+'_content'+' div'+tab_rel+' div').show(); 
		
	
		});*/


    /***********************************************
 *     show left submenus on dashboard side    *
 **********************************************/

    $('.toggle_this').click(function(){
        var toggleClass  = $(this).attr('rel');
        $(toggleClass).toggle();
    });


    /***********************************************
 *     validate change password form in admin
 **********************************************/
    $("#change_pass_form").validate({
        rules: {
            old_pass: {
                required: true,
                minlength: 5
            },
            new_pass: {
                required: true,
                minlength: 5
            },
            confirm_pass: {
                required: true, 
                equalTo: "#new_pass",
                minlength: 5
            },
            user_name: {
                required: true,
                minlength: 5
            }
        },
        messages: {
            
    }
    });
    /***********************************************
 *     validate nonprofit form in admin
 **********************************************/
    $("#nonprofit").validate({
        rules: {
            user_name: {
                required: true
            },
            url: {
                required: true
            },
            des: {
                required: true
            }

        },
        messages: {


    }
    });
    /**************************************
     * validate Advt Form in admin
     ***************************************
 $("#update_advt").validate({
        rules: {
            name: {
                required: true,
                alphabets: true
            },
            last_name:{
            	required: true
            },
            uname: {
                required: true
            },
           phone: {
                required: true
            },
            website: {
                required: true
            },
            paypal_email: {
                required: true
            },

        },
        messages: {


    }
    });
    
    
    
    
    
    
    
    
    
    /**************************************
     * End validate Advt Form in admin
     ***************************************/
    
   /***********************************************
 *     validate FAQ's form in admin
 **********************************************/
    $("#faqs").validate({
        rules: {
            ques: {
                required: true,
                minlength: 6
            },
            ans: {
                required: true
            }

        },
        messages: {

    }
    });

      $.validator.addMethod("alphabets", function(value,element)
        {
           return this.optional(element) || /^[a-zA-Z]+$/.test(value);
        }, "Alphabets only");

});

function confirm_function() {
	
	if(confirm('Are You Sure To Delete')){
		
	}else {
		
		return false;
	}
	
	
}
	
