<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

?>

<div class="list_advt_content ">

    <?php if( ($_GET['menu']== 'a_advt' || $_GET['menu'] == '') && ($_GET['action'] != base64_encode('a_edit')) && ($_GET['action'] != base64_encode('advt_dtl'))){ ?>
    <!-- list_advt_1 -->
    <div id="list_advt_1">
    <script>
       $('document').ready(function(){
           $('.active_advt').click(function(){
               var id = $(this).attr('id');
               var value = $(this).attr('value');

               $.ajax({
                  type: "POST",
                  url:"includes/espy-ajax.php",
                  data:'advt_id='+id+'&action=s&value='+value,
                  success:function(response){

                      if(response == '1'){
                          var msg = '<h1>Advertiser activated</h1>';
                      }else{
                          var msg = '<h1>Advertiser deactivated</h1>';
                      }
                      $('.active_advt').val(response);
                      $.blockUI({
                                message: msg,
                                timeout: 1000
                            });

                  }

               });
           });
       });
   </script>

        <!--  start related-act-top_right -->
        <div id="admin-content-top">
            <h2>Advertisers</h2>
        </div><!-- end related-act-top_right -->

        <div id="admin-content-bottom" class="reload_info">
            <table class="display admin_table" id="admin_table_2">
                <thead>
                    <tr>
                        <th class="alpha1">Sr.</th>
                        <th class="alpha2">Name</th>
                        <th class="alpha3">Email</th>
                        <th class="alpha4">Website</th>
                        <th class="alpha5">Contact</th>
                        <th class="alpha6">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 1;
                    $users_list = getAdvertisers();
                    while ($row = mysql_fetch_array($users_list, MYSQL_ASSOC)) {
                        //print_r($users_list);

                        echo '<tr>
                                <td class=" td_1">' . $i++ . '</td>

                                <td id="' . $row['id'] . '" class="view_advt">
                                    <a href="home.php?page=advt&action='.base64_encode('advt_dtl').'&user_id='.base64_encode($row['id']).'"> ' . $row['name'].' '.$row['last_name'] . '
                                    </a>
                                </td>

                                <td class="mail">' . $row['email'] . '</td><td class="website">&nbsp;' . $row['website'] . '</td>

                                <td class="contact">&nbsp;' . $row['phone'] . '</td>

                                <td class="editdiv">
                                    <input type="checkbox" title="active/inactive" '.( $row["status"] == "yes" ? 'value="1" checked ' : 'value="0"').' name="active" id="'.base64_encode($row['id']).'" class="active_advt" />&nbsp;
                                    <a title="Edit" href="home.php?page=advt&action='.base64_encode('a_edit').'&user_id='.base64_encode($row['id']).'">
                                    <img src="images/edit.png"></a>&nbsp;
                                    <img title="Delete" id="' . base64_encode($row['id']) .'" class="delete_advt" src="images/ddelete.gif">

                                </td>
                             </tr>';
                    }
                    ?>
                </tbody>
            </table>
        </div><!--end admin-content-bottom-->

    </div><!-- end list_advt_1 -->
    <?php } ?>

<?php if( ($_GET['action'] == base64_encode('a_edit')) ){

    $advt_data = getAdvt(base64_decode($_GET['user_id']));
    $advt_data = mysql_fetch_assoc($advt_data);
                /*print_r($advt_data);*/
    ?>
    <!--  advt_edit_outer -->
    <div id="advt_edit_outer" class="edit_outer">

        <!--  start related-act-top_right -->
        <div id="admin-content-top">
            <h2>Advertisers</h2>
        </div><!-- end related-act-top_right -->


        <div id="admin-content-bottom">
            <script type="text/javascript" charset="utf-8">
                $(document).ready(function() {
                    $("#update_advt").validate();
                } );
            </script>



            <div id="advt_edit_inner" class="edit_inner">


                <form name = "update_advt" method = "post" id = "update_advt" action="home.php?page=advt&action=<?php echo base64_encode('a_edit');?>&user_id=<?php echo $_GET['user_id'] ; ?>">
                    <input type ="hidden" name = "submit_advt_id" value ="<?php echo base64_decode($_GET['user_id']) ; ?>" >
                    <div class="show_msg">
                        <span class="updated">Profile updated</span>
                    </div>

                    <label>
                        <span class="s_data">First Name:</span>
                        <span class="s_val">

                            <input type = "text" class="required alphabets" name ="name" value ="<?php echo $advt_data['name'] ; ?>" >

                        </span>
                    </label>
                    <label>
                        <span class="s_data">Last Name:</span>
                        <span class="s_val">

                            <input type = "text"  class="required alphabets" name ="last_name" value ="<?php echo $advt_data['last_name'] ; ?>" >

                        </span>
                    </label>
                    <label
                        ><span class="s_data">User Name:</span>
                        <span class="s_val">

                            <input readonly type = "text" class="required" name = "uname" value ="<?php echo $advt_data['uname'] ; ?>" >

                        </span>
                    </label>
                    <label>
                        <span class="s_data">Phone Number:</span>
                        <span class="s_val">

                            <input type = "text" class="required" name = "phone" value ="<?php echo $advt_data['phone'] ; ?>" >

                        </span>
                    </label>
                    <label>
                        <span class="s_data">Website:</span>
                        <span class="s_val">

                            <input type = "text" class="" name = "website" value ="<?php echo $advt_data['website'] ; ?>" >

                        </span>
                    </label>
                    <label>
                        <span class="s_data">Email:</span>
                        <span class="s_val">

                            <input type = "text" class="required email " readonly name = "email" value ="<?php echo $advt_data['email'] ; ?>" >

                        </span>
                    </label>
                    <label>
                        <span class="s_data">Paypal Email:</span>
                        <span class="s_val">

                            <input type = "text" class="required email"  name = "paypal_email" value ="<?php echo $advt_data['paypal_email'] ; ?>" >

                        </span>
                    </label>

                    <label>
                        <span class="s_data">Account Number:</span>
                        <span class="s_val">

                            <input type = "text" class="required number" name = "account_number" value ="<?php echo $advt_data['account_number'] ; ?>" >
                        </span>
                    </label>
                    <label>
                        <span class="s_data">Rounting Number:</span>
                        <span class="s_val">
                            <input type = "text" class="required number" name = "routing_number" value ="<?php echo $advt_data['routing_number'] ; ?>" >
                        </span>
                    </label>

                    <!--label>
                        <span class="s_data">Gender:</span>
                        <span class="s_val">
                            <select class="required" name ='gender'>
                                <option value="1" <?php echo  ($advt_data['gender'] == '1') ? 'selected' : '' ; ?> >Male</option>
                                <option value="0" <?php echo  ($advt_data['gender'] == '0') ?  'selected' : '' ;  ?> >Female</option>
                            </select>
                        </span>
                    </label>

                    <label>
                        <span class="s_data">City:</span>
                        <span class="s_val">
                            <input type = "text" class="required" name = "city" value ="<?php echo $advt_data['city'] ; ?>" >
                        </span>
                    </label>
                    <label>
                        <span class="s_data">State:</span>
                        <span class="s_val">
                            <input type = "text" class="required" name = "state" value ="<?php echo $advt_data['state'] ; ?>" >
                        </span>
                    </label>
                    <label>
                        <span class="s_data">Zip:</span>
                        <span class="s_val">
                            <input type = "text" class="required" name = "zip" value ="<?php echo $advt_data['zip'] ; ?>" >
                        </span>
                    </label>
                    <label>
                        <span class="s_data">Household Income:</span>
                        <span class="s_val">
                            <input type = "text" class="required" name = "household_income" value ="<?php echo $advt_data['household_income'] ; ?>" >
                        </span>
                    </label>
                    <label>
                        <span class="s_data">Ethnicity:</span>
                        <span class="s_val"><input type = "text" class="required" name = "ethnicity" value ="<?php echo $advt_data['ethnicity'] ; ?>" >
                        </span>
                    </label>
                    <label>
                        <span class="s_data">Education:</span>
                        <span class="s_val"><input type = "text" class="required" name = "education" value ="<?php echo $advt_data['education'] ; ?>" >
                        </span>
                    </label>
                    <label>
                        <span class="s_data">Religion:</span>
                        <span class="s_val"><input type = "text" class="required" name = "religion" value ="<?php echo $advt_data['religion'] ; ?>" >
                        </span>
                    </label>

                    <label>
                        <span class="s_data">Marital Status:</span><span class="s_val">
                            <select class="required" name ='marital_status' >
                                <option value="1" <?php echo $advt_data['marital_status'] == '1' ?  ' selected ' : '' ; ?> >Married</option>
                                <option value="0" <?php echo  $advt_data['marital_status'] == '0' ?  ' selected ' : '' ;?> >Unmarried</option>
                            </select>
                        </span>
                    </label>

                    <label><span class="s_data">Interest:</span>
                        <span class="s_val">
                            <select class="required" name ='interest' >
                                <option value="cricket" <?php echo  $advt_data['interest'] == 'cricket' ? 'selected' : '' ; ?> >Cricket</option>
                                <option value="football" <?php echo $advt_data['interest'] == 'football' ?  'selected' : '' ; ?> >Football</option>
                                <option value="chess" <?php echo  $advt_data['interest'] == 'chess' ? 'selected' : '' ; ?> >Chess</option>
                                <option value="other" <?php echo $advt_data['interest'] == 'other' ?  'selected' : '' ; ?> >Other</option>
                            </select>
                        </span>
                    </label>

                    <label><span class="s_data">Age group:</span>
                        <span class="s_val">
                            <select class="required" name ='age_group' >
                                <option value="1" <?php echo  $advt_data['age_group'] == '1' ? 'selected' : '' ; ?> >5-15</option>
                                <option value="2" <?php echo $advt_data['age_groupv'] == '2' ?  'selected' : '' ; ?> >16-25</option>
                                <option value="3" <?php echo  $advt_data['age_group'] == '3' ? 'selected' : '' ; ?> >26-35</option>
                                <option value="4" <?php echo $advt_data['age_group'] == '4' ?  'selected' : '' ; ?> >36-45</option>
                                <option value="5" <?php echo  $advt_data['age_group'] == '5' ? 'selected' : '' ; ?> >46-55</option>
                                <option value="6" <?php echo $advt_data['age_group'] == '6' ?  'selected' : '' ; ?> >56-65</option>
                                <option value="7" <?php echo  $advt_data['age_group'] == '7' ? 'selected' : '' ; ?> >66-75</option>
                                <option value="8" <?php echo $advt_data['age_group'] == '8' ?  'selected' : '' ; ?> >76-above</option>

                            </select>
                        </span>
                    </label>

                    <label>
                        <span class="s_data">Occupation:</span>
                        <span class="s_val">
                            <input type = "text" class="required" name = "occupation" value ="<?php echo $advt_data['occupation'] ; ?>" >
                        </span>
                    </label>

                    <label>
                        <span class="s_data">About User:</span>
                        <span class="s_val">
                            <textarea class="required" name = "about"><?php echo $advt_data['about'] ; ?></textarea>
                        </span>
                    </label-->

                    <label>
                        <span class="s_data">
                        <input type ="submit" name ="submit_a_data" value="update"></span>
                        <span class="s_val">
                        </span>
                    </label>


                </form><!--end form-->

            </div><!--end .edit_inner-->


        </div><!--end admin-content-bottom-->

    </div><!-- end advt_edit_outer -->
    <?php } ?>


<?php if($_GET['action']== base64_encode('advt_dtl')){
    $advt_data = getAdvt(base64_decode($_GET['user_id']));
    $advt_data = @mysql_fetch_assoc($advt_data);

    ?>
    <!-- advt_dtl_outer -->
    <div id="user_edit_outer" class="edit_inner">

        <!--  start related-act-top_right -->
        <div id="admin-content-top">
            <h2>Adevertiser Details</h2>
        </div><!-- end related-act-top_right -->

        <div id="admin-content-bottom">

            <div id="advt_edit_inner" class="advt_dtl">

                <div class="prof_pic"><!--img src="<?php echo $advt_data['image_url'] ; ?>" /--></div>

                <label>
                    <span class="s_data">First Name:</span>
                    <span class="s_val">
                        <p class=""><?php echo $advt_data['name'] ; ?></p >
                    </span>
                </label>

                <label>
                    <span class="s_data">Last Name:</span>
                    <span class="s_val">
                        <p class=""><?php echo $advt_data['last_name'] ; ?></p>
                    </span>
                </label>
                <label>
                    <span class="s_data">User Name:</span>
                    <span class="s_val">
                        <p class=""><?php echo $advt_data['uname'] ; ?></p>
                    </span>
                </label>
                <label>
                    <span class="s_data">Phone Number:</span>
                    <span class="s_val">
                        <p class=""><?php echo $advt_data['phone'] ; ?></p>
                    </span>
                </label>
                <label>
                    <span class="s_data">Website:</span>
                    <span class="s_val">
                        <p class=""><?php echo $advt_data['website'] ; ?></p>
                    </span>
                </label>
                <label>
                    <span class="s_data">Email:</span>
                    <span class="s_val">
                        <p class=""><?php echo $advt_data['email'] ; ?></p>
                    </span>
                </label>
                <label>
                    <span class="s_data">Paypal Email:</span>
                    <span class="s_val">
                        <p class=""><?php echo $advt_data['paypal_email'] ; ?></p>
                    </span>
                </label>
                <label>
                    <span class="s_data">Account Number:</span>
                    <span class="s_val">
                        <p class=""><?php echo $advt_data['account_number'] ; ?></p>
                    </span>
                </label>
                <label>
                    <span class="s_data">Rounting Number:</span>
                    <span class="s_val">
                        <p class=""><?php echo $advt_data['routing_number'] ; ?></p>
                    </span>
                </label>

                <!--label>
                    <span class="s_data">Gender:</span>
                    <span class="s_val">
                        <p class="">
                            <?php if ($advt_data['gender'] == '1' ){ echo 'Male'; }  ?>
                            <?php if ($advt_data['gender'] == '0' ){ echo 'Female'; }  ?>
                        </p>
                    </span>
                </label>

                <label>
                    <span class="s_data">City:</span>
                    <span class="s_val">
                        <p class=""><?php echo $advt_data['city'] ; ?></p>
                    </span>
                </label>
                <label>
                    <span class="s_data">State:</span>
                    <span class="s_val">
                        <p class=""><?php echo $advt_data['state'] ; ?></p>
                    </span>
                </label>
                <label>
                    <span class="s_data">Zip:</span>
                    <span class="s_val">
                        <p class=""><?php echo $advt_data['zip'] ; ?></p>
                    </span>
                </label>
                <label>
                    <span class="s_data">Household Income:</span>
                    <span class="s_val">
                    <p class=""><?php echo $advt_data['household_income'] ; ?></p></span>
                </label>
                <label>
                    <span class="s_data">Ethnicity:</span>
                    <span class="s_val">
                        <p class=""><?php echo $advt_data['ethnicity'] ; ?></p>
                    </span>
                </label>
                <label>
                    <span class="s_data">Education:</span>
                    <span class="s_val">
                        <p class=""><?php echo $advt_data['education'] ; ?></p>
                    </span>
                </label>
                <label>
                    <span class="s_data">Religion:</span>
                    <span class="s_val">
                        <p class=""><?php echo $advt_data['religion'] ; ?></p>
                    </span>
                </label>

                <label>
                    <span class="s_data">Marital Status:</span>
                    <span class="s_val">
                        <p class="">
                            <?php if ($advt_data['marital_status'] == '1' ){ echo 'Married';}  ?>
                            <?php if ($advt_data['marital_status'] == '0' ){ echo 'Unmarried';}  ?>
                        </p>
                    </span>
                </label>

                <label>
                    <span class="s_data">Interest:</span>
                    <span class="s_val">
                        <p class="">
                            <?php if ($advt_data['interest'] == 'cricket' ){ echo 'cricket';}  ?>
                            <?php if ($advt_data['interest'] == 'football' ){ echo 'football';}  ?>
                            <?php if ($advt_data['interest'] == 'chess' ){ echo 'chess';}  ?>
                            <?php if ($advt_data['interest'] == 'other' ){ echo 'other';}  ?>
                        </p>
                    </span>
                </label>

                <label><span class="s_data">Age group:</span>
                    <span class="s_val">
                        <p class="">
                            <?php if ($advt_data['age_group'] == '1' ){ echo '5-15';}  ?>
                            <?php if ($advt_data['age_group'] == '2' ){ echo '16-25';}  ?>
                            <?php if ($advt_data['age_group'] == '3' ){ echo '26-35<';}  ?>
                            <?php if ($advt_data['age_group'] == '4' ){ echo '36-45';}  ?>
                            <?php if ($advt_data['age_group'] == '5' ){ echo '46-55';}  ?>
                            <?php if ($advt_data['age_group'] == '6' ){ echo '56-65';}  ?>
                            <?php if ($advt_data['age_group'] == '7' ){ echo '66-75';}  ?>
                            <?php if ($advt_data['age_group'] == '8' ){ echo '76-above';}  ?>
                        </p>
                    </span>
                </label>

                <label>
                    <span class="s_data">Occupation:</span>
                    <span class="s_val">
                        <p class=""><?php echo $advt_data['occupation'] ; ?></p>
                    </span>
                </label>

                <label>
                    <span class="s_data">About User:</span>
                    <span class="s_val">
                        <p class=""><?php echo $advt_data['about'] ; ?></p>
                    </span>
                </label-->



            </div><!--end .advt_dtl_inner-->

        </div><!--end #admin-content-bottom-->

    </div><!-- end advt_dtl_outer -->
    <?php } ?>


</div>
