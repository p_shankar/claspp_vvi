<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

?>


<div class="dashboard_home_content">

<?php if($_GET['menu']== 'ads_list' || $_GET['menu']== '' && ($_GET['action'] != base64_encode('cpn_dtl'))){   ?>


    <script>
        $('document').ready(function(){
            $('.active_campaign').click(function(){
                var id = $(this).attr('id');
                var value = $(this).attr('value');

                $.ajax({
                    type: "POST",
                    url:"includes/espy-ajax.php",
                    data:'campaign_id='+id+'&action=campaign_status&value='+value,
                    success:function(response){

                        if(response == '1'){
                          var msg = '<h1>Status updated</h1>';
                      }else{
                          var msg = '<h1>Status updated</h1>';
                      }
                       $('.active_campaign').val(response);
                       
                        $.blockUI({
                            message: msg,
                            timeout: 1000
                        });
                    }

                });
            });
        });
    </script>
    <!-- dashboard_home_1 -->
    <div id="ads_1">

        <!--  start related-act-top_right -->
        <div id="admin-content-top">
            <h2>Campaigns </h2>
        </div><!-- end related-act-top_right -->

        <div id="admin-content-bottom">
            <table class="display admin_table" id="ads_table">
                <thead>
                    <tr>
                        <th class="alpha11">Sr.</th>
                        <th class="alpha12">Ad Name</th>
                        <th class="alpha13">Advertiser</th>
                        <th class="alpha14">Invoice</th>
                        <th class="alpha15">Active</th>
                    </tr>
                </thead>
                <tbody>


    <?php
    $ads = ad_listing();
    $i = 0;
    while ($row = mysql_fetch_assoc($ads)) {

        echo '<tr>
                                <td class="ads_td_1">'.++$i.'</td>
                                <td class="ads_td_2">
                                    <a href="home.php?page=ads&action='.base64_encode("cpn_dtl").'&cpn_id='.base64_encode($row["id"]).'">'.$row["title"].'</a>
                                </td>
                                <td class="ads_td_3">
                                    <a href="home.php?page=advt&action='.base64_encode("advt_dtl").'&user_id='.base64_encode($row["advertiser_id"]).'">'.$row["ad_name"].'</a>
                                </td>
                                <td class="ads_td_4">Invoice</td>
                                <td class="ads_td_5">
                                    <input title="active/inactive" class="active_campaign" id="'.base64_encode($row["id"]).'" type="checkbox" '.( $row["active"] == "yes" ? 'value="1" checked ' : 'value="0"').'>
                                </td>
                            </tr>';

    }
    ?>
                </tbody>
            </table>

        </div>

    </div><!-- end ads_1 -->
    <?php } ?>

<?php if($_GET['menu']== 'ads_mng'){ ?>
    <!-- ads_2 -->
    <div id="dashboard_home_2" class="">

        <!--  start related-act-top_right -->
        <div id="admin-content-top">
            <h2>Ad settings</h2>
        </div><!-- end related-act-top_right -->

        <div id="admin-content-bottom">
            <!--h2>No ad is available...!</h2-->



        </div>

    </div><!-- end ads_2 -->
    <?php } ?>

<?php if($_GET['action'] == base64_encode('cpn_dtl')){ ?>
    <!-- ads_3 -->
    <div id="dashboard_home_2" class="">

        <!--  start related-act-top_right -->
        <div id="admin-content-top">
            <h2>Campaign Detail</h2>
        </div><!-- end related-act-top_right -->

        <div id="admin-content-bottom">
            <?php
            $c_id = base64_decode($_GET['cpn_id']);



            $result_array  = campaign_details($c_id);

            $result_array = mysql_fetch_assoc($result_array);

            //print_r('<p>'.$result_array['fee'].'</p>');
            ?>

            <div class="campaign_inner">
                <!--label><?php //sprint_r($result_array);?></label-->

                <label class="">
                    <span class="spn_span">Title:</span>
                    <p><?php echo $result_array['title'] ?></p>
                </label>
                <label class="">
                    <span class="spn_span">Url:</span>
                    <p><?php echo $result_array['url'] ?></p>
                </label>
                <label class="">
                    <span class="spn_span">Budget:</span>
                    <p><?php echo '$ '.$result_array['budget'] ?></p>
                </label>
                <label class="">
                    <span class="spn_span">Per post payment:</span>
                    <p><?php echo '$ '.$result_array['per_post_payment'] ?></p>
                </label>
                <label class="">
                    <span class="spn_span">Payment status:</span>
                    <p><?php echo $result_array['payment_status'] ?></p>
                </label>
                <label class="">
                    <span class="spn_span">Active:</span>
                    <p><?php echo $result_array['active'] ?></p>
                </label>
                <label class="">
                    <span class="spn_span">Description</span>
                    <p><?php echo $result_array['description'] ?></p>
                </label>
                <label class="">
                    <span class="spn_span">Target countries</span>
                    <p><?php
                        $country_query = 'select * from espy_countries' ;
                        $country_results = mysql_query($country_query);


                        $country_arr = explode(',',$result_array['target_country']);



                        while($country_result = mysql_fetch_assoc($country_results)){

                            if(in_array($country_result['id'],$country_arr)){
                                $country_echo[] = $country_result['country'];
                                //print_r($country_results);
                            }
                            
                        }
                        $echo_country = implode(' , ' , $country_echo);
                        echo $echo_country;
                        ?>
                    </p>
                </label>
                <label class="">
                    <span class="spn_span">Target categories</span>
                    <p><?php
                        $category_query = 'select * from espy_campaign_categories' ;
                        $category_results = mysql_query($category_query);

                        $category_arr = explode(',',$result_array['target_category']);

                        while($category_result = mysql_fetch_assoc($category_results)){

                            if(in_array($category_result['id'],$category_arr)){
                                $category_echo[] = $category_result['category_name'];
                                //print_r($category_results);
                            }
                            
                        }
                        $echo_category = implode(' , ' , $category_echo);
                        echo $echo_category;
                        ?>
                    </p>
                </label>
                <label class="">
                    <span class="spn_span">Target age groups</span>
                    <p><?php
                        $age_query = 'select * from espy_age_groups' ;
                        $age_results = mysql_query($age_query);

                        $age_arr = explode(',',$result_array['age_groups']);

                        while($age_result = mysql_fetch_assoc($age_results)){

                            if(in_array($age_result['id'],$age_arr)){
                                $age_echo[] = $age_result['group_value'];
                            }
                            
                        }
                        $echo_age = implode(' , ' , $age_echo);
                        echo $echo_age;
                        ?>
                    </p>
                </label>
                <label class="">
                    <span class="spn_span">Target gender</span>
                    <p>
                        <?php
                        $gender_query = 'select * from espy_gender' ;

                        $gender_results = mysql_query($gender_query);

                        $gender_arr = explode(',',$result_array['gender']);

                        while($gender_result = mysql_fetch_assoc($gender_results)){

                            if(in_array($gender_result['id'],$gender_arr)){
                                $gender_echo[] = $gender_result['name'];
                            }
                            
                        }
                        $echo_gender = implode(' , ' , $gender_echo);
                        echo $echo_gender;
                        ?>
                    </p>
                </label>
                <label class="">
                    <span class="spn_span">Target zip code</span>
                    <p><?php
                        $zip_query = 'select * from espy_zip' ;

                        $zip_results = mysql_query($zip_query);

                        $zip_arr = explode(',',$result_array['zip_code']);

                        while($zip_result = mysql_fetch_assoc($zip_results)){

                            if(in_array($zip_result['id'],$zip_arr)){
                                $zip_echo[] = $zip_result['zip_value'];
                            }
                            
                        }
                        $echo_zip = implode(' , ' , $zip_echo);
                        echo $echo_zip;
                        ?></p>
                </label>
                <label class="">
                    <span class="spn_span">Target radius</span>
                    <p><?php
                        $radius_query = 'select * from espy_radius' ;

                        $radius_results = mysql_query($radius_query);

                        $radius_arr = explode(',',$result_array['zip_radius']);

                        while($radius_result = mysql_fetch_assoc($radius_results)){

                            if(in_array($radius_result['id'],$radius_arr)){
                                $radius_echo[] = $radius_result['radius_value'];
                            }
                            
                        }
                        $echo_radius = implode(' , ' , $radius_echo);
                        echo $echo_radius;
                        ?></p>
                </label>
                <label class="">
                    <span class="spn_span">Target household income</span>
                    <p><?php
                        $income_query = 'select * from espy_household_income' ;

                        $income_results = mysql_query($income_query);

                        $income_arr = explode(',',$result_array['household_income']);

                        while($income_result = mysql_fetch_assoc($income_results)){

                            if(in_array($income_result['id'],$income_arr)){
                                $income_echo[] = $income_result['income_value'];
                            }
                            
                        }
                        $echo_income = implode(' , ' , $income_echo);
                        echo $echo_income;
                        ?></p>
                </label>


            </div>

        </div>

    </div><!-- end ads_3 -->
    <?php } ?>

</div>