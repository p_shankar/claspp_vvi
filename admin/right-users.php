<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

?>

<div class="list_users_content ">

    <?php if( ($_GET['menu']== 'u_users' || $_GET['menu']== '') && ($_GET['action'] != base64_encode('u_edit')) && ($_GET['action'] != base64_encode('usr_dtl'))){ ?>
    <!-- list_users_1 -->
    <div id="list_users_1">
    <script>
       $('document').ready(function(){
           $('.active_user').click(function(){
               var id = $(this).attr('id');
               var value = $(this).attr('value');

               $.ajax({
                  type: "POST",
                  url:"includes/espy-ajax.php",
                  data:'user_id='+id+'&action=user_status&value='+value,
                  success:function(response){
                      if(response == '1'){
                          var msg = '<h1>User activated</h1>';
                      }else{
                          var msg = '<h1>User deactivated</h1>';
                      }
                      $('.active_user').val(response);
                      $.blockUI({
                                message: msg,
                                timeout: 1000
                            });

                  }

               });
           });
       });
   </script>

        <!--  start related-act-top_right -->
        <div id="admin-content-top">
            <h2>Users</h2>
        </div><!-- end related-act-top_right -->

        <div class="show_data"></div>


        <div id="admin-content-bottom">
        <div class="user_page">
            <table class="display admin_table" id="admin_table_1">
                <thead>
                    <tr>
                        <th class="alpha1">Sr.</th>
                        <th class="alpha2">Name</th>
                        <th class="alpha3">Email</th>
                        <th class="alpha4">Website</th>
                        <th class="alpha7">city</th>
                        <th class="alpha8">State</th>
                        <th class="alpha5">Contact</th>
                        <th class="alpha6">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 1;
                    $users_list = getAllUsers();
                    while ($row = mysql_fetch_array($users_list, MYSQL_ASSOC)) {
                        //print_r($users_list);

                        echo '<tr>
                                <td class=" td_1">' . $i++ . '</td>
                                <td id="' . base64_encode($row['id']) . '" class="view_user">
                                    <a href="home.php?page=users&action='.base64_encode('usr_dtl').'&user_id='.base64_encode($row['id']).'"> ' . $row['name'].' '.$row['last_name'] . '</a></td>
                                <td class="mail">&nbsp;' . $row['email'] . '</td>
                                <td class="website">&nbsp;' . $row['website'] . '</td>
                                <td class="alpha7">&nbsp;' . $row['city'] . '</td>
                                <td class="alpha8">&nbsp;' . $row['state'] . '</td>
                                <td class="contact">&nbsp;' . $row['phone'] . '</td>
                                <td class="editdiv" >
                                <input type="checkbox"  title="active/inactive" '.( $row["status"] == "yes" ? 'value="1" checked ' : 'value="0"').' name="active" id="'.base64_encode($row['id']).'" class="active_user" />&nbsp;
                                <a title="Edit"  href="home.php?page=users&action='.base64_encode('u_edit').'&user_id='.base64_encode($row['id']).'">
                                <img src="images/edit.png"></a>&nbsp;
                                <img title="Delete" id="' . base64_encode($row['id']) .'" class="delete_user" src="images/ddelete.gif">
                                </td>
                               </tr>';
                    }
                    ?>
                </tbody>
            </table>

            </div>
        </div><!--end admin-content-bottom-->

    </div><!-- end list_users_1 -->

     
    <?php } ?>

<?php if($_GET['action']== base64_encode('usr_dtl')){
    $user_data = getUser(base64_decode($_GET['user_id']));
    $user_data = mysql_fetch_assoc($user_data);

    ?>
    <!-- list_users_2 -->
    <div id="list_users_2" class="">

        <!--  start related-act-top_right -->
        <div id="admin-content-top">
            <h2>User Details</h2>
        </div><!-- end related-act-top_right -->

        <div id="admin-content-bottom">
            <div id="user_edit_inner" class="usr_dtl">

                    <div class="prof_pic"><!--img src="" /--></div>

                   <label>
                        <span class="s_data">First Name:</span>
                        <span class="s_val">
                            <p class=""><?php echo $user_data['name'] ; ?></p>
                        </span>
                    </label>
                    <label>
                        <span class="s_data">Last Name:</span>
                        <span class="s_val">
                            <p class=""><?php echo $user_data['last_name'] ; ?></p>
                        </span>
                    </label>
                    <label>
                        <span class="s_data">User Name:</span>
                        <span class="s_val">
                            <p class=""><?php echo $user_data['uname'] ; ?></p>
                        </span>
                    </label>
                    <label>
                        <span class="s_data">Phone Number:</span>
                        <span class="s_val">
                            <p class=""><?php echo $user_data['phone'] ; ?></p>
                        </span>
                    </label>
                    <label>
                        <span class="s_data">Website:</span>
                        <span class="s_val">
                            <p class=""><?php echo $user_data['website'] ; ?></p>
                        </span>
                    </label>
                    <label>
                        <span class="s_data">Email:</span>
                        <span class="s_val">
                            <p class=""><?php echo $user_data['email'] ; ?></p>
                        </span>
                    </label>
                    <label>
                        <span class="s_data">Paypal Email:</span>
                        <span class="s_val">
                            <p class=""><?php echo $user_data['paypal_email'] ; ?></p>
                        </span>
                    </label>

                    <label>
                        <span class="s_data">About User:</span>
                        <span class="s_val">
                            <p class=""><?php echo $user_data['about'] ; ?></p>
                        </span>
                    </label>
            </div>
            
        </div>

    </div><!-- end list_users_2 -->
    <?php } ?>

<?php if($_GET['action']== base64_encode('u_edit')){
    $user_data = getUser(base64_decode($_GET['user_id']));

    $user_data = mysql_fetch_assoc($user_data);

    ?>
    <!-- user_edit_outer (code to edit / update user inforation.. )-->
    <div id="user_edit_outer" class="edit_outer">

        <!--  start related-act-top_right -->
        <div id="admin-content-top">
            <h2>Edit User</h2>
        </div><!-- end related-act-top_right -->

        <div id="admin-content-bottom">

            <script type="text/javascript" charset="utf-8">
                $(document).ready(function() {
                    $("#update_user").validate();
                } );
            </script>
            <div id="user_edit_inner" class="edit_inner">


                <form name = "update_user" method = "post" id = "update_user" action="home.php?page=users&action=<?php echo base64_encode('u_edit');?>&user_id=<?php echo $_GET['user_id'] ; ?>">
                    <input type ="hidden" name = "submit_id" value ="<?php echo base64_decode($_GET['user_id']) ; ?>" >

                    <div class="show_msg">                        
                        <span class="updated">Profile updated</span>
                    </div>
                    
                    <label>
                        <span class="s_data">First Name:</span>
                        <span class="s_val">
                            <input type = "text" class="required alphabets" name ="name" value ="<?php echo $user_data['name'] ; ?>" >
                        </span>
                    </label>
                    <label>
                        <span class="s_data">Last Name:</span>
                        <span class="s_val">
                            <input type = "text" class="required alphabets" name ="last_name" value ="<?php echo $user_data['last_name'] ; ?>" >
                        </span>
                    </label>
                    <label>
                        <span class="s_data">User Name:</span>
                        <span class="s_val">
                            <input type = "text" class="required" name = "uname" value ="<?php echo $user_data['uname'] ; ?>" >
                        </span>
                    </label>
                    <label>
                        <span class="s_data">Phone Number:</span>
                        <span class="s_val">
                            <input type = "text" class="required number" minlength="8" maxlength="15"  name = "phone" value ="<?php echo $user_data['phone'] ; ?>" >
                        </span>
                    </label>
                    <label>
                        <span class="s_data">Website:</span>
                        <span class="s_val">
                            <input type = "text" name = "website" value ="<?php echo $user_data['website'] ; ?>" >
                        </span>
                    </label>
                    <label>
                        <span class="s_data">Email:</span>
                        <span class="s_val">
                            <input type = "text" class="required email" readonly name = "email" value ="<?php echo $user_data['email'] ; ?>" >
                        </span>
                    </label>
                    <label>
                        <span class="s_data">Paypal Email:</span>
                        <span class="s_val">
                            <input type = "text" class=" email" name = "paypal_email" value ="<?php echo $user_data['paypal_email'] ; ?>" >
                        </span>
                    </label>

                    <label>
                        <span class="s_data">About User:</span>
                        <span class="s_val">
                            <textarea class="" name = "about"><?php echo $user_data['about'] ; ?></textarea>
                        </span>
                    </label>

                    <label>
                        <span class="s_data">
                            <input type ="submit" name ="submit_u_data" value="update">
                        </span>
                        <span class="s_val">
                        </span>
                    </label>

                </form><!--end form-->

            </div><!--end .edit_inner-->

        </div><!--end #admin-content-bottom-->

    </div><!-- end user_edit_outer -->
    <?php } ?>

</div>