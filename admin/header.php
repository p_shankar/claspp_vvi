<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Claspp</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
<meta name="author" content="Claspp">

<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">

<!-- The styles -->
<link id="bs-css" href="css/bootstrap-cerulean.css" rel="stylesheet">
<style type="text/css">
body {
	padding-bottom: 40px;
}
.sidebar-nav {
	padding: 9px 0;
}
/*for show the image in edit */
.imgs{
	opacity:1 !important;
}
/*End for show the image in edit */


</style>
<link href="css/bootstrap-responsive.css" rel="stylesheet">
<link href="css/charisma-app.css" rel="stylesheet">
<link href='css/opa-icons.css' rel='stylesheet'>


    

    <!--  jquery core -->   
     <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>


    <!-- Custom jquery scripts -->
    <script src="js/jquery/custom_jquery.js" type="text/javascript"></script>

    <!--********** include blockUIs.js************-->
    <script src="js/blockUI.js"></script>
    
    <!--custom scripts-->
 <!--   <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script> -->
    <script type="text/javascript" language="javascript" src="../datatables/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="js/espy-admin.validate.js"></script>
	<script type="text/javascript" language="javascript" src="js/espy-admin1.validate.js"></script>
	
<!-- Start validation for Admin Part  ------>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="jquery.validate.min.js"></script>

<!--date ----->
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

  <script>
  $(document).ready(function(){
$( "#datepicker2" ).blur(function() {

 var first = $("#datepicker1").val();

var second =$("#datepicker2").val();

$("#mass_payment").load("mass_payment_page.php #mass_data", { 'start': first,'end':second } );


});

});
</script>

            <script type="text/javascript"> 
      $(document).ready( function() {
        $(".msg1").delay(3000).fadeOut();
      });
    </script>


          <script type="text/javascript" language="javascript">
            jQuery(function(){
                jQuery('#StartDate').datepicker({
dateFormat:"yy-mm-dd",
                    beforeShow : function(){
                    		
                        jQuery( this ).datepicker('option','maxDate', jQuery('#EndDate').val() );
                    }
                });
                jQuery('#EndDate').datepicker({
					dateFormat:"yy-mm-dd",
                    beforeShow : function(){
                    		
                        jQuery( this ).datepicker('option','minDate', jQuery('#StartDate').val() );
                    }
                });

		            })
        </script>

		 <link rel="stylesheet" href="css/calendar.css" />
<link rel="stylesheet" href="/resources/demos/style.css" />
<script>
$(function() {

	   jQuery('#datepicker1').datepicker({
		   showOn: "button",
buttonImage: "images/calendericon.png",
buttonImageOnly: true,
     dateFormat:"yy-mm-dd",
                    beforeShow : function(){
                    		
                        jQuery( this ).datepicker('option','maxDate', jQuery('#datepicker2').val() );
                    }
                });
				  jQuery('#datepicker2').datepicker({
showOn: "button",
buttonImage: "images/calendericon.png",
buttonImageOnly: true,
     dateFormat:"yy-mm-dd",
            beforeShow : function(){
                    		
                        jQuery( this ).datepicker('option','minDate', jQuery('#datepicker1').val() );
                    }
                });

});


/****************** for impor cvc file 
Created by : Akashdeep	
Created on : 10 September

***************/

$(function() {

	   jQuery('#datepicker_am').datepicker({
		   showOn: "button",
buttonImage: "images/calendericon.png",
buttonImageOnly: true,
     dateFormat:"yy-mm-dd",
                    beforeShow : function(){
                    		
                        jQuery( this ).datepicker('option','maxDate', jQuery('#datepicker_am2').val() );
                    }
                });
				  jQuery('#datepicker_am2').datepicker({
showOn: "button",
buttonImage: "images/calendericon.png",
buttonImageOnly: true,
     dateFormat:"yy-mm-dd",
            beforeShow : function(){
                    		
                        jQuery( this ).datepicker('option','minDate', jQuery('#datepicker_am').val() );
                    }
                });

});


</script>
<!-- end date		--->
<script type="text/javascript">

(function($,W,D)
{
    var JQUERY4U = {};

    JQUERY4U.UTIL =
    {
        setupFormValidation: function()
        {
            //form validation rule
            //Start For advertser
            $("#update_advt").validate({
                rules: {
                    name: {
                required: true,
              accept: "[a-zA-Z]+" 
            },
            last_name:{
            	required: true,
            	accept: "[a-zA-Z]+" 
            },
            uname: {
                required: true
            },
           phone: {
                required: true,
                digits:true,
                minlength: 9,
                maxlength:12
                
            },
            website: {
                required: true
            },
            paypal_email: {
                required: true
            },
		},
		 messages:{
              name: {
              required:	"Please Enter name",
                accept: "Please Enter Name[a-zA-Z]"
                 },
                   last_name:{
                        required:"Please provide a Last_name",
                        accept: "Please Enter Last_Name[a-zA-Z]"                       
                              },
                    uname:{
                          required: "Please Enter User Name"
                          },
                	phone:{
                		required: "Please Enter Phone Number",
                		digits:"Phone Number must Be in Digits",
                		 minlength:"Minlenght must be 9 digits",
                		 maxlength:"Maxlenght must be 12 digits"
                			},	
               		website:{
                		required: "Please Enter Website"
                		},
                 paypal_email:{
                		required: "Please Enter Paypal Email"
                	},		
                },
                submitHandler: function(form) {
                    form.submit();
                }
            });
            /****************  
             * 
             *End For Advertser
              *Start For Campaign 
             * 
             */
            
            
            $("#edit_comp").validate({
                rules: {
                    name: {
                required: true,
              accept: "[a-zA-Z]+" 
            },
            url:{
            	required: true            	 
            },
            des: {
                required: true
            },
		},
		 messages:{
              name: {
              required:	"Please Enter name",
                accept: "Please Enter Name[a-zA-Z]"
                 },
                   url:{
                        required:"Please provide a Url"
                   },
                    des:{
                          required: "Please Enter description"
                          },
                			
                },
                submitHandler: function(form) {
                    form.submit();
                }
            });
            
/***********************End For Campaign   ********************
 
 * 
 * Start For Update Faq
 * 
 * ****/

$("#faq_edit").validate({
                rules: {
                    ques1: {
                required: true
               },
            ans1:{
            	required: true            	 
            },
        },
		 messages:{
              ques1: {
              required:	"Please Enter Question"                
                 },
                   ans1:{
                        required:"Please provide Answer"
                   },
                },
                submitHandler: function(form) {
                    form.submit();
                }
            });
            /*
			* End for Update Faq
 			* Start for Create Faq
			 */
$("#faq_insert").validate({
                rules: {
                    ques1: {
                required: true
               },
            ans1:{
            	required: true            	 
            },
        },
		 messages:{
              ques1: {
              required:	"Please Enter Question"                
                 },
                   ans1:{
                        required:"Please provide Answer"
                   },
                },
                submitHandler: function(form) {
                    form.submit();
                }
            });
            /*
             * End For Create Faq
             * 
             * Start for non_profit insert
             */
            
            $("#non_pr_add").validate({
                rules: {
                    file: {
                required: true               
            },
            name:{
            	required: true,
            	accept: "[a-zA-Z]+" 
            },
            company: {
                required: true
            },
           contact: {
                required: true,
                digits:true,
                minlength: 9,
                maxlength:12
                
            },
           url: {
                required: true
            },
            des: {
                required: true
            },
		},
		 messages:{
              file: {
              required:	"Please Add a Image"
                 },
                   name:{
                        required:"Please provide a Name",
                        accept: "Please Enter Name[a-zA-Z]"                       
                              },
                    company:{
                          required: "Please Enter company"
                          },
                	contact:{
                		required: "Please Enter Phone Number",
                		digits:"Phone Number must Be in Digits",
                		 minlength:"Minlenght must be 9 digits",
                		 maxlength:"Maxlenght must be 12 digits"
                			},	
               		url:{
                		required: "Please Enter Url"
                		},
                 des:{
                		required: "Please Enter Description"
                	},		
                },
                submitHandler: function(form) {
                    form.submit();
                }
            });
            
            /*
         * End For non_profit insert
         * 
         * Start For non_profit update
         */    
        $("#non_pr_edit").validate({
                rules: {
             name:{
            	required: true,
            	accept: "[a-zA-Z]+" 
            },
            company: {
                required: true
            },
           contact: {
                required: true,
                digits:true,
                minlength: 9,
                maxlength:12
                
            },
           url: {
                required: true
            },
            des: {
                required: true
            },
		},
		 messages:{
                 name:{
                        required:"Please provide a Name",
                        accept: "Please Enter Name[a-zA-Z]"                       
                              },
                    company:{
                          required: "Please Enter company"
                          },
                	contact:{
                		required: "Please Enter Phone Number",
                		digits:"Phone Number must Be in Digits",
                		 minlength:"Minlenght must be 9 digits",
                		 maxlength:"Maxlenght must be 12 digits"
                			},	
               		url:{
                		required: "Please Enter Url"
                		},
                 des:{
                		required: "Please Enter Description"
                	},		
                },
                submitHandler: function(form) {
                    form.submit();
                }
            });
        
        
        
        
        
        
         /*
         * End For non_profit  update
         * 
         * 
       /*
		* begin for Email 20 june
		*
		*/
$.validator.addMethod("valueNotEquals", function(value, element, arg){
  return arg != value;
 }, "Value must not equal arg.");

$("#email_insert").validate({
                rules: {
           subject:{
            	required: true 
            },
          content:{
                required: true
            },
			header_email:{
                required: true
            },
			footer:{
                required: true
            },
           disclaimer :{
                required: true
            },
		email_type:{ valueNotEquals: "default" }

					},
		 messages:{
                  subject:{
                        required:"Please Enter email subject"                       
                              },
                   content:{
                          required: "Please Enter email content"
                          },
					 header_email:{
                          required: "Please Enter Header"
                          },
					 footer:{
                          required: "Please Enter footer"
                          },
					 disclaimer:{
                          required: "Please Enter disclaimer"
                          },
               	 email_type:{ valueNotEquals: "Please select an email type" }
                
						  },
                submitHandler: function(form) {
                    form.submit();
                }
            });
        
	/*
	*  Ending for Email
	*
	*/
        /*
		* begin for Profile 
		*
		*/

$("#change_pass_form").validate({

                rules: {
            user_name:{
            	required: true,
					equalTo: "#user_na"
            },
           old_pass: {
                required: true
            },
           new_pass: {
                required: true                
            },
          confirm_pass: {
                required: true,
				equalTo:"#new_pass"
            },
            		},
		 messages:{
                  user_name:{
                        required:"Please Enter User Name",
							equalTo:"User name Are not match with old one"
                              },
                   old_pass:{
                          required: "Please Enter Old Password"
                          },
                	 new_pass:{
                		required: "Please Enter New Password"
                			},	
               		 confirm_pass:{
                		required: "Please Enter Confirm Password",
						equalTo:"Password Are not match"
                		},
                       },
                submitHandler: function(form)
							{
						
						  var x =   $('#pass_veri').val();
						  //alert(x);
						  if(x == "Yes"){
                          form.submit();
						  }
                            }
            });
	/*
	*  Ending for Profile
	*
	*/
	    /*
		* begin for Profile 
		*
		*/

$("#chk").validate({
                rules: {
  		'case1[]': {
                required: true,
                minlength: 1
            }
            		},
		 messages:{
                  	 ' case1[]':{
                		required: "Please Select check box",
						minlength:"Please Select atleast one check box"
                		},
                       },
                submitHandler: function(form) {
							var test = confirm("Are You Sure You Want To delete ? ");
							if(test) {
                             form.submit();
							}
                }
            });
        
	/*
	*  Ending for Profile
	*
	*/
	/*
		* begin for email 
		*
		*/

$("#email_id").validate({
                rules: {
  		'email[]': {
                required: true,
                minlength: 1
            }
            		},
		 messages:{
                  	 ' email[]':{
                		required: "Please Select check box",
						minlength:"Please Select atleast one check box"
                		},
                       },
                submitHandler: function(form) {
							var test = confirm("Are You Sure You Want To Send  Email ? ");
							if(test) {
                             form.submit();
							}
                }
            });
        
	/*
	*  Ending for email
	*
	*/
               /*
		* begin for promo 24 july
		*
		*/
$("#promo").validate({
                rules: {
           StartDate:{
            	required: true 
            },
          EndDate:{
                required: true
            },			   
		discount:{ valueNotEquals: "default" }
					},
		 messages:{
                  StartDate:{
                        required:"Please Select Start date"                       
                              },
                   EndDate:{
                          required: "Please Select End Date"
                          },
	             	 discount:{ valueNotEquals: "Please select an Discount" }
                
						  },
                submitHandler: function(form) {
                    form.submit();
                }
            });
        
	/*
	*  Ending for promo
	*
	*/
        

$("#any_email").validate({
                rules: {
					name:{
						required: true 
					}
        
					},
		 messages:{
						 name:{
                        required:"Please select  user to send email "                       
                              }      
						  },
                submitHandler: function(form) {
                    form.submit();
                }
            });



  /******** update by akash sep 12 for cvc*******/
$("#read_cvc").validate({
                rules: {
					start_ms:{
						required: true 
					},
           end_ms:{
            	required: true 
            },
				 'upload_file[]':
					 {
					 required: true, 
					 accept: "csv" 
					 }
        
					},
		 messages:{
						 start_ms:{
                        required:"Please Select start date"                       
                              },
                  end_ms:{
                        required:"Please select end date"                       
                              },          
						'upload_file[]': 
							 {
					 required: "Select csv file", 
					 accept: "Please selcet only csv file" 
					 }
						  },
                submitHandler: function(form) {
                    form.submit();
                }
            });



        /********/
        }
    }

    //when the dom has loaded setup form validation rules
    $(D).ready(function($) {
        JQUERY4U.UTIL.setupFormValidation();
    });

})(jQuery, window, document);
</script>


<!--end Validation For Admin--->

</head>

<body>



<!-- topbar starts -->
<div class="navbar">
  <div class="navbar-inner">
    <div class="container-fluid"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a> <a class="brand" href="http://www.claspp.com/"> <img alt="Claspp" src="images/logo.png" /> <span></span></a>
      
      <!-- user dropdown starts -->
      <div class="btn-group pull-right" > <a class="btn dropdown-toggle" data-toggle="dropdown" href="#"> <i class="icon-user"></i><span class="hidden-phone"> admin</span> <span class="caret"></span> </a>
        <ul class="dropdown-menu">
          <li><a href="http://www.claspp.com/beta/logout.php">Logout</a></li>
        </ul>
      </div>
      <!-- user dropdown ends -->
      
      <div class="top-nav nav-collapse">
        <ul class="nav">
          <li><a href="http://www.claspp.com/beta">Visit Site</a></li>
        </ul>
      </div>
      <!--/.nav-collapse --> 
    </div>
  </div>
</div>

