<?php
session_start();
@ob_start();

include 'config.php';
include 'admin/includes/db-functions.php';
$users = getSpage(3);
$users = mysql_fetch_assoc($users);

include('header.php');
?>
<!--==============================content================================-->
<section id="content">
    <div class="wrapper_main">
        <div class="container_12">
            <div class="grid_13">
                <div class="grid_3">
                    <div class="menu_div">
                        <ul>
                            <li><a href="about.php">About Us</a></li>
                            <li><a href="users.php" class="active">Users</a></li>
                            <li><a href="advertisers.php">Advertisers</a></li>
                          <!--  <li><a href="adListings.php">AdListings</a></li>-->
                            <li><a href="contact.php">Contact Us</a></li>
                            <li><a href="faq.php">Faqs</a></li>
                        </ul>
                    </div>
                </div>
                <div class="grid_9">
                    <div class="inner_titlediv">
                        <h3><?php echo $users['title']; ?></h3>
                    </div>
                    <div class="inner_content_div">
                        <?php echo $users['content']; ?>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</section>
<section id="content">
    <div class="wrapper_main">
        <div class="container_12">
            <div class="grid_13 top_3">
                <div class="valueadvertiser">
                    <h2>VALUE ADVERTISERS</h2>
                    <div class="valueadvertiser_div">
                        <ul>
                            <li><img src="images/advertiser1.png" alt="EspyLink Value Advertisers"></li>
                            <li><img src="images/advertiser2.png" alt="EspyLink Value Advertisers"></li>
                            <li><img src="images/advertiser3.png" alt="EspyLink Value Advertisers"></li>
                            <li><img src="images/advertiser4.png" alt="EspyLink Value Advertisers"></li>
                            <li><img src="images/advertiser5.png" alt="EspyLink Value Advertisers"></li>
                            <li><img src="images/advertiser6.png" alt="EspyLink Value Advertisers"></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--==============================footer=================================-->
<?php
include('footer.php');
?>
</body>
</html>