<?php
session_start();
@ob_start();
include_once("config.php"); /**db file*/
require_once('inc/functions.php');
if ($_SESSION['log'] == 'true' ){
header('Location:admin/index.php');
}
else if( $_SESSION['userlog'] == 'true' ){
header('Location:user-login/User/publisher-dashboard.php');
}
else if ($_SESSION['advertiserlog'] == 'true' ){
header('Location:user-login/Advertiser/index.php');
}
?>
<script type="text/javascript" src="http://connect.facebook.net/en_US/all.js"></script>
<script type="text/javascript">
    FB.init({
        appId  : '<?=$fbconfig['appid']?>',
        status : true, // check login status
        cookie : true, // enable cookies to allow the server to access the session
        xfbml  : true  // parse XFBML
    });

</script>
			<?php


				/*when forgot password action is taken*/
				if(isset($_POST["forgot_submit"])) {

				$to = $_POST["email"];
				$mail_sent = send_forgot_email($to);

				if ($mail_sent){


				}
				else
				{
				echo '<p style="color: #91A136;
				font-size: 12px;
				left: 435px;
				position: absolute;
				top: 263px;">Your Email Id is  not valid</p>';
				}

				}

/*when login action is taken*/
if(isset($_POST["submit"])) {

		$email = $_POST["email"];
		$pass = $_POST["password"];

		if(isset($_POST["rememberme"]) && ($_POST["rememberme"] == 'true')) {
		setcookie($email,$pass,time()+3600*24*30);
		}

		$rs =  check_user($email,$pass);
		if(!empty($rs)) {

		$_SESSION['loggedin_id'] = $rs['id'];

		if($rs['type']=="user"){
		$_SESSION["userlog"] = "true";

		header('Location:user-login/User/publisher-dashboard.php');
		}

		else{

		$_SESSION["advertiserlog"] = "true";
		header('Location:user-login/Advertiser/index.php');
		}



		} else {

		header('Location:claspp-login.php?action='.base64_encode("inv_usr"));

		}
}


include('claspp-header.php');
include ('facebook/fbmain.php');
?>


<!--==============================content================================-->

<script type="text/javascript" charset="utf-8">
    jQuery(document).ready(function() {
        function getCookie(c_name)
        {
            var i,x,y,ARRcookies=document.cookie.split(";");
            for (i=0;i<ARRcookies.length;i++)
            {
                x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
                y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);

                x=x.replace(/^\s+|\s+$/g,"");
                if (x==c_name)
                {
                    return unescape(y);
                }
            }
        }

        jQuery("#user_login").validate();

        jQuery(".email").focus(function(){
            jQuery('.error_login').css('display','none');
        });
        jQuery(".pass").focus(function(){
            jQuery('.error_login').css('display','none');
        });

        jQuery(".email").blur(function(){
            var cookiename = jQuery(this).val();
            var password = getCookie(cookiename);
            jQuery('.pass').val(password);

        });
    } );
</script>

<header class="page-header">
    <div class="container clr">
        <h1 class="page-header-title">
            <?php
            if($_GET['action'] == base64_encode('forgotpassword')) {
                if($mail_sent == true){
                    echo 'Email sent';
                }else{
                    echo 'Reset password';
                }
            }else{
                echo 'Login to Claspp';
            }
            ?>
        </h1>
    </div>
</header>
<div class="container">
    <section id="primary" class=" clr">
        <div id="content" class="clr site-content" role="main">
            <form name="user_login" id="user_login" method="post">
                <?php if($_GET['action'] == base64_encode('forgotpassword')) {

                    if($mail_sent == true){
                        echo '  <article class="entry-content entry clr">
                        <h3 style=" min-height: 250px;">An email has been sent to your id, please click on the link to change your password.</h3>
                   </article><!-- .entry-content -->
                  ';
                    }else{
                        echo '
                     <article class="entry-content entry clr">
                            <h3>Enter your email id to reset password.</h3>
                       </article><!-- .entry-content -->
                        <div id="wpex-loginform-wrap" class="clr">
                         <p class="login-username">
                            <label for="user_login">E-mail</label>
                            <input type="text" name="email" class="required email">
                        </p>
                              <p class="login-submit">
                                     <input type="submit" name="forgot_submit" value="Forgot Password" class="reset_button">
                            </p>
                            </div>
                           ';
                    }


                }else{
                    ?>

                <article class="entry-content entry clr">

                    <div class="admin_inputdiv">
                        <p>&nbsp;
                            <span class="error_login">
                                <?php if ($_GET['action'] == base64_encode("inv_usr")) { echo 'Email or password do not match.';}?>
                            </span>
                        </p>
                    </div>

                </article><!-- .entry-content -->

                <div id="wpex-loginform-wrap" class="clr">

                    <p class="login-username">
                        <label for="user_login">Username</label>
                        <input type="text" name="email" class="required email">
                    </p>
                    <p class="login-password">
                        <label for="user_pass">Password</label>
                        <input type="password" name="password" class="required pass">
                    </p>

                    <p class="login-remember">
                        <label>
                        <input type="checkbox" name="rememberme" value="true" class="remember" /> Remember Me.
                        </label>
                         |
                        <label class="forgot">
                            <a href="?action=<?php echo base64_encode('forgotpassword');?>">Forgot Password ?</a>
                        </label>
                    </p>
                    <p class="login-submit">
                        <input type="submit" name="submit" value="login" class="log_in_button">                        
                    </p>

                </div><!-- #wpex-loginform-wrap -->

   <div class="grid_6">
                    <div class="user_signup_div">
                        <h3>Register or login as a publisher using social media </h3>
                    </div>
                    <div class="social_connect">
                        <ul>
		    <li><a href="<?php echo $loginUrl?>" class="facebookconnect"></a></li>
<!--
                            <li><a href="#" class="twitterconnect"></a></li>
                            <li><a href="#" class="googleconnect"></a></li>
                            <li><a href="#" class="linkedinconnect"></a></li>
                            <li><a href="#" class="pinterestconnect"></a></li>
-->
                        </ul>

                    </div>
                    <span class="sign_up"><a href="/beta/usersignup.php">SIGN UP NOW</a></span>
                </div>
    <?php } ?>
             
				</form>
				</div><!-- #content -->



				</section><!-- #primary -->

				</div><!-- .container -->

<?php
include('claspp-footer.php');
