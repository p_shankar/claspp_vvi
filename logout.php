<?php
    ob_start();
    @session_start();
    
    unset($_SESSION['id']);
    unset($_SESSION['username']);
    unset($_SESSION['oauth_provider']);

    if($_SESSION['reg']){
        unset($_SESSION['reg']);
    }
    session_destroy();

    header("location:index.php");
    ob_end_flush();
    exit;
?>
