<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Claspp</title>
		<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
<link href="css/coupon.css" media="all" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" media="screen" href="Nexa_FontKit/stylesheet.css">


<style type="text/css" media="print">
.redeem_box .redeem_box_inner p {
    color: #8CC542 !important;
    float: left;
    font-size: 20px;
    margin: 4px 0;
    position: relative;
    width: 100%;
   z-index: 2147483647;
}
.prnt_btn{
    display:none !important;
}


</style>
</head>


<?php  
require_once('config.php');
require_once('inc/functions.php');

	/***************  code to get coupon details ********/

	$id = base64_decode($_REQUEST['ad']);
	$coupon =  coupon_details($id);

	/***************  code to generate random number  details **********/

	$testing = uniqid('cl');

	function mt_rand_str ($l, $c ) {
	for ($s = '', $cl = strlen($c)-1, $i = 0; $i < $l; $s .= $c[mt_rand(0, $cl)], ++$i);
	return $s;
	}

	$testing = mt_rand_str('14',$testing);

//echo  $testing;

?>
<body>
<div id="main_wrapper">
   <div id="work_container">
      <!--Print page div-->
      <div class="print_pade_div">
         <div class="bg_image"><img src="images/logo_icon.png" /></div>
         <div class="header_main">
            <div class="logo">
               <img src="images/coupon-logo.png" />
            </div>
            <h2>Thanks for using claspp, here's all the details of this offer.</h2>
         </div>
         <!--middle_con-->
         <div class="middle_mail">
            <div class="mail_left_con">
               <div class="coupon_title">
                  <h2><?php  echo $coupon['title']; ?></h2>
                  <p><?php  echo $coupon['description'];?></p>
               </div>
               <div class="fine_print">
                  <h3>Fine Print:</h3>
                  <p><?php  echo $coupon['fine_print'];?></p>
             <h3>Disclaimer:</h3>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
               </div>
               <div class="how_mail_work">
                  <h3>HOW IT WORKS</h3>
                  <ul>
                     <li><div class="numbering"><p>1</p></div>Print this bad boy out!</li>
                     <li><div class="numbering"><p>2</p></div>Show this to the company over here.</li>
                     <li><div class="numbering"><p>3</p></div>Your're done!</li>
                  </ul>
                  <div class="mail_arrow"><img src="images/arrow_mail.png" /></div>
               </div>
               <div class="problem_here">
                  <h3>Problems? Here's how to contact us.</h3>
                  <p><!--span>(317)555-1234</span-->info@claspp.com</p>
               </div>
            </div>
            
            <div class="mail_right_con">
               <div class="barcode_con">
                  <img src="images/GRREEN_BG.png" />
                  <div class="barcode_con_inner">
                     <h2><?php echo ($coupon['redeem'] == 'store' ? 'In store' : ($coupon['redeem'] == 'online' ? ucfirst($coupon['redeem']) : '' )  );  ?></h2>

					<?php  if($testing) {  ?>
                     <img src="inc/barcode/html/image.php?filetype=PNG&dpi=72&scale=1&rotation=0&font_family=Arial.ttf&font_size=12&text=<?php echo $testing;?>&thickness=30&checksum=&code=BCGcode39" alt="Barcode Image" />
					 <?php }  ?>

                     <p>Redeem After:<span><?php  echo date('F d, Y',strtotime($coupon['start_date']));?></span></p >
                     <p>Expires On:<span><?php  echo date('F d, Y',strtotime($coupon['end_date']));?></span></p>
                  </div>
               </div>
               <div class="redeem_box">
                  <img src="images/BLACK_BG.png" />
                  <div class="redeem_box_inner">
                     <h3>Redeem at:</h3>
                     <p><?php echo  $coupon['company_name']; ?></p>
                     <p><?php echo  $coupon['company_address']; ?></p>
                     <p>Indianapolis, <?php echo $coupon['zip_code']; ?></p>
                     <p><?php echo  $coupon['company_phone']; ?></p>
                  </div>
               </div>
            </div>
		<?php                                   
			$qrcode = "https://api.qrserver.com/v1/create-qr-code/?size=150x150&amp;data=" . $site_url . "user-login/User/redeem.php?bcode=" . $testing;
                 ?> 
            <img src="<?= $qrcode; ?>" height="75px" width="75px"></img>

         </div>
         <!--/middle_con-->
         
         <!--footer_con
         <div class="legal_stuff">
            <h3>Legal Stuff</h3>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages.</p>
         </div>-->
         <!--/footer_con-->

<div class="prnt_btn" style="float:right;margin-right:30px">
<input type="button" value="Print this page" onClick="window.print()">
<div>

      </div>
      <!--/Print page div-->
   </div>
</div>
</body>
</html>
