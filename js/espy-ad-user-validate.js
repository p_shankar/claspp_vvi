
$(document).ready(function() {

    /**************************************
    * setting tabs
    ***************************************/

    $('.setting_tab').click(function(){
        $('.current').removeClass('current');
        $('.set_content_show').removeClass('set_content_show');
        $(this).addClass('current');
        var show_tab_content = $(this).attr('rel');

        $(show_tab_content).addClass('set_content_show');
    });

    $('.social_tab').click(function(){
      
        $('#profile_div').removeClass('set_content_show');
        $('#s_media_div').addClass('set_content_show');
        
    });

    
    /***********update profile************/
    $("#publisher_form").validate({
        rules: {
            'u_newpass': {
                required: true
            },
            'u_cnfmpass': {
                required: true,
                equalTo: '#u_newpass'
            }
        },
        submitHandler: function(form) {

            var form = $('#publisher_form');
            $.ajax( {
                type: "POST",
                url: form.attr( 'action' ),
                data: form.serialize(),
                success: function( response ) {
                    if(response == 1){
                        var msg = 'Profile updated.'
                    }else{
                        var msg = 'Please try again.'
                    }

                    $.blockUI({
                        message: '<h1>'+msg+'</h1>',
                        timeout: 1000
                    });

                }
            } );
        }
    });

    /***********update geographic information************/

    $("#user_info_form").validate({

        submitHandler: function(form) {

            var form = $('#user_info_form');
            $.ajax({
                url: form.attr( 'action' ),
                type: 'POST',
                data: form.serialize(),
                success: function() {
                    $.blockUI({
                        message: '<h1>Updated geographic information..</h1>',
                        timeout: 1000
                    });

                }
            });

            return false;
        }

    });
    
    /***********update email preference************/
    $("#u_notification_form").validate({

        submitHandler: function(form) {

            var form = $('#u_notification_form');
            $.ajax( {
                type: "POST",
                url: form.attr( 'action' ),
                data: form.serialize(),
                success: function( response ) {
                    $.blockUI({
                        message: '<h1>Updated email preference.</h1>',
                        timeout: 1000
                    });

                }
            } );
            return false;
        }
    });


    /************ change password*********
    $("#change_pass_form").validate({
        rules: {
            'u_oldpass': {
                required: true
            },
            'u_newpass': {
                required: true
            },
            'u_cnfmpass': {
                required: true,
                equalTo: '#u_newpass'
            }
        },
        submitHandler: function(form) {

            var form = $('#change_pass_form');
            $.ajax( {
                type: "POST",
                url: form.attr( 'action' ),
                data: form.serialize(),
                success: function(response ) {
                    if(response == -1){
                        $('#u_oldpass').val('');
                        $('#u_oldpass').attr('style','color:red');
                        $('#u_oldpass').attr('placeholder','Old pass do not match');

                        $('#u_oldpass').focus(function(){
                            $('#u_oldpass').attr('style','color:#CCCCCC');
                            $('#u_oldpass').attr('placeholder','');
                        });
                    } else if(response == -2){
                        $.blockUI({
                            message: '<h1>Password changed successfully.</h1>',
                            timeout: 1000
                        });
                    }
                }
            });
        }
    });*/

    /*********** ***********/

    $('#upload_url').click(function(){
        $('#upload_pic').trigger('click');
    });

    $('#upload_pic').change(function(){
        $.blockUI({
            message: '<h1>loading image...</h1>'
        });

        $('#publishers_pic').ajaxSubmit({
            success:  function(res) {
                if(res == -1){
                   
                    $('#prof_pic_div').load('publisher-dashboard.php #prof_pic_div');
                    $('#prof_pic_div1').load('publisher-dashboard.php #prof_pic_div1 img');
                    $('#prof_pic_div2').load('publisher-dashboard.php #prof_pic_div2 img');
                    $('#prof_pic_div2').load('publisher-dashboard.php #prof_pic_div2 img',function(){
                        $.unblockUI();
                    });
                } else if(res == -2){
                    $.blockUI({
                        message: '<h1>Invalid file type.</h1>',
                        timeout: 3000
                    });

                } else {
                    $.blockUI({
                        message: '<h1>Error in image uploading.</h1>',
                        timeout: 3000
                    });
                }
            }
        });
    });

    /*********** have referal ************/
    $("#referal_form").validate({

        submitHandler: function(form) {

            var form = $('#referal_form');
            $.ajax( {
                type: "POST",
                url: form.attr( 'action' ),
                data: form.serialize(),
                success: function( response ) {
                    $.blockUI({
                        message: '<h1>Referal Saved.</h1>',
                        timeout: 1000
                    });

                }
            } );
        }
    });


    /*********** show_hide_pass ************/

    $(".show_hide_pass").click(function(){
        $('.password_div').toggle(function(){
            if( $('.password_div').css('display') == 'block'){
                $('img.show_hide_pass').attr('src','../../images/up.png');
                $('#u_newpass').attr('disabled',false),
                $('#u_cnfmpass').attr('disabled',false)
            } else{
                $('img.show_hide_pass').attr('src','../../images/down.png');
                $('#u_newpass').attr('disabled',true),
                $('#u_cnfmpass').attr('disabled',true)
            }
        });
    });

    /*********** wallet toggle ************/

    $(".wallet_toggle").live('click',
        function(){
            var id = $(this).attr('id');
            if( $('.'+id).css('display') == 'block'){
                $('#'+id+' > img').attr('src','../../images/down.png');
                $('.'+id).hide('slow');
            } else {
                $('#'+id+' > img').attr('src','../../images/up.png');
                $('.wallet_hide').hide('slow');
                $('.'+id).show('slow');
            }
                     
        });
    /******multiple select*******/
    $(".ul_select").live('click', function(){
        var id = $(this).attr('id')
        $('.li_'+id).show('fast');
    });

    $(".ul_select").live('mouseleave' , function(){
        var id = $(this).attr('id')
        $.each($('.li_'+id+'>input:checkbox'),function(i,val){
            if($(this).is (':checked') == false){
                $(this).parent().hide('fast');
            }
        });
    });
       
   
    /*add amethod to validate.js*/
    $.validator.addMethod("alphabets", function(value,element)
    {
        return this.optional(element) || /^[a-zA-Z]+$/.test(value);
    }, "Alphabets only");


    /*display confirmation pop up*/
    $('.social_connect_click').click(function(){
        var href = $(this).attr('href');
        $.blockUI({
            message: '<div class="ui_div">\n\
                                    <p class="close_btn"><img src="images/close.png" /></p>\n\
                                    <p class="social_p">Please click continue below to link your social media account! </p>\n\
                                    <input type="button" name="continue" value="continue" class="social_continue" /></div>',
            
            onBlock: function(){
                $('.social_continue').click(function(){
                    window.location = href ;
                });

                /**/
                $('.close_btn > img').click(function(){
                    $.unblockUI();
                });

            }
        });

        return false;
    });


    /*donate button click*/
 
    $('#withdraw_submit').live('click',
        function(){
           
            if($('#pub_id').val() == 0 ){
           
                return false;

            } else if($('#withdraw_amt').val() == '' ){
                $('#withdraw_amt').css('border-color','red');
            
            } else if($("#organization > li >input:checkbox:checked").length == 0){
                $('ul.ul_select').css('border-color','red');
                $('#withdraw_amt').css('border-color','#CCCCCC');
                return false;
            
            }else {
                $('ul.ul_select').css('border-color','#CCCCCC');
                $('#withdraw_amt').css('border-color','#CCCCCC');

                $('#withdraw_form').ajaxSubmit({
                    success:  function(res) {
                        if(res == -1){
                            var htm = $('#tab-5').html();
                            var top_image_text_id = $('#top_image_text_id').html();

                            $('#tab-5').load('publisher-dashboard.php '+htm);
                            $('#top_image_text_id').load('publisher-dashboard.php '+top_image_text_id);
                        }

                    }

                });

				 
            }
        });
     
});


   