   <div id="footer-bottom" class="style-1 clr">
                <div class="container row clr">
                    <div id="copyright" class="span_10 col clr-margin" role="contentinfo">
                        <p>Copyright 2014 &middot; <a href="<?php echo $site_url; ?>" title="Claspp">Claspp</a></p>
                    </div><!-- #copyright -->
                    
                    <div id="footer-links" class="span_14 col">
                    <ul id="footerlinks" class="clr">
                    <li class="Terms of use">
                    <a href="<?php echo $site_url; ?>terms.php" title="Terms of use" target="_blank">
                    Terms
                    </a>
                    </li>
                    <li class="Privacy Policy">
                    <a href="<?php echo $site_url; ?>privacy.php" title="Privacy Policy" target="_blank">Privacy Policy
                    </a>
                    </li>

                    </ul><!-- #footerlinks -->
                    </div><!-- #footer-links -->

                     <div id="footer-social" class="span_14 col">
                    <ul id="social" class="clr">
                    <li class="twitter">
                    <a href="http://www.twitter.com/" title="Follow Us" target="_blank">
                    <img src="images/social/twitter.png" alt="Follow Us" />
                    </a>
                    </li>
                    <li class="facebook"><a href="http://www.facebook.com/" title="Like Us" target="_blank">
                    <img src="images/social/facebook.png" alt="Like Us" />
                    </a>
                    </li>

                    </ul><!-- #social -->
                    </div><!-- #footer-social -->
                </div><!-- .container -->
            </div><!-- #footer-bottom -->

        </div><!-- #wrap -->

        <script type='text/javascript' src='js/retina.js?ver=0.0.2'></script>
        <script type='text/javascript'>
            /* <![CDATA[ */
            var wpexLocalize = {"responsiveMenuText":"Menu"};
            /* ]]> */
        </script>
        <script type='text/javascript' src='js/global.js?ver=1.0'></script>
        <script type='text/javascript' src='js/symple_scroll_fade.js?ver=1.0'></script>
    </body>
</html>
