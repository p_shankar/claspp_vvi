<?php
require_once('config/lang/eng.php');
require_once('tcpdf.php');
require_once 'dbc.php';
$q=$_REQUEST["id"];
$row=''; 

  $query="select * from  tbl_property_condition_report where wid=$q" ;

  	 $result = mysql_query($query); 
     $row = mysql_fetch_assoc($result);



   $f_html = <<<EOD

<table>

    <tr style="float:left;width:100%;border:1px solid #ccc;margin:15px 0;">
      <td style="float:left;width:100%;padding:10px 0;border-bottom: 1px solid #ccc;">
        <h2 style="text-align:center;color:#151515;font-size: 22px; font-family:Trebuchet MS;font-weight: normal;">Maintenance Property Condition Report</h2>
	      </td>
    </tr>
    <tr style="float:left;width:100%;height:40px;background: -webkit-gradient(linear, left top, left bottom, from(#f1f1f1), to(#f7f7f7)) !important;background: -moz-linear-gradient(top,  #f1f1f1,  #f7f7f7) !important;filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;  -ms-filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;">
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">Property Address:</label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 16px">$row[property_address]</label>
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">Date Completed:</label>
           <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 16px">$row[date_completed]</label>
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px;margin">Work Order</label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 16px">$row[workorder_id]</label>
        </td>
      </tr>
      <tr style="float:left;width:100%;border:1px solid #ccc;margin:15px 0;">
      <td style="float:left;width:100%;padding:10px 0;border-bottom: 1px solid #ccc;">
        <h2 style="text-align:center;color:#151515;font-size: 22px; font-family:Trebuchet MS;font-weight: normal;">Security</h2>
	      </td>
    </tr>
    <tr style="float:left;width:100%;height:40px;background: -webkit-gradient(linear, left top, left bottom, from(#f1f1f1), to(#f7f7f7)) !important;background: -moz-linear-gradient(top,  #f1f1f1,  #f7f7f7) !important;filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;  -ms-filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;">
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px;margin">Occupied   </label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:15px; font-size: 16px">$row[occupied]</label>
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px;margin">If Yes, By Who:   </label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:15px; font-size: 16px">$row[occupied_who]</label>
        </td>
      </tr>
      <tr style="float:left;width:100%;height:40px;background: -webkit-gradient(linear, left top, left bottom, from(#f1f1f1), to(#f7f7f7)) !important;background: -moz-linear-gradient(top,  #f1f1f1,  #f7f7f7) !important;filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;  -ms-filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;">
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px;margin">Violations Posted  </label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:15px; font-size: 16px">$row[violations_posted]</label>
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px;margin">If Yes, By Who:   </label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:15px; font-size: 16px">$row[violations_posted_describe]</label>
        </td>
      </tr>
      <tr style="float:left;width:100%;height:40px;background: -webkit-gradient(linear, left top, left bottom, from(#f1f1f1), to(#f7f7f7)) !important;background: -moz-linear-gradient(top,  #f1f1f1,  #f7f7f7) !important;filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;  -ms-filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;">
        <td style="float:left;width:150px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px;margin">Property Type </label>
          
        </td>
        <td style="float:left;width:350px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px;margin"># of units:</label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:15px; font-size: 16px">$row[property_type_units]    ,  </label>
           <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px;margin">    Shared Utilities:  </label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:15px; font-size: 16px">$row[property_type_shared_utilities]</label>
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px;margin"> Mobile Home VIN #  </label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:15px; font-size: 16px">$row[property_type_mobile_home_vin]</label>
        </td>
      </tr>
      <tr style="float:left;width:100%;height:40px;background: -webkit-gradient(linear, left top, left bottom, from(#f1f1f1), to(#f7f7f7)) !important;background: -moz-linear-gradient(top,  #f1f1f1,  #f7f7f7) !important;filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;  -ms-filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;">
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px;margin">Property For Sale </label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:15px; font-size: 16px">$row[Property_for_sale]    ,  </label>
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px;margin">Contact Info:  </label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:15px; font-size: 16px">$row[Property_for_sale_contact_info]    ,  </label>
           
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px;margin"> Details:   </label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:15px; font-size: 16px">$row[Property_for_sale_details]</label>
          
                   
       </td>
      </tr>
      <tr style="float:left;width:100%;height:40px;background: -webkit-gradient(linear, left top, left bottom, from(#f1f1f1), to(#f7f7f7)) !important;background: -moz-linear-gradient(top,  #f1f1f1,  #f7f7f7) !important;filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;  -ms-filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;">
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px;margin">Garage/Outbuilding Present:    </label>
          
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px;margin"> $row[garage_outbuilding_present]   </label>
          
           
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          
          
          
                   
       </td>
      </tr>
      <tr style="float:left;width:100%;height:40px;background: -webkit-gradient(linear, left top, left bottom, from(#f1f1f1), to(#f7f7f7)) !important;background: -moz-linear-gradient(top,  #f1f1f1,  #f7f7f7) !important;filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;  -ms-filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;">
        <td style="float:left;width:500px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px;margin">Able to Access Interior </label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:15px; font-size: 16px">$row[able_access_interior]    ,  </label>
        </td>
       
        <td style="float:left;width:500px;height:30px;margin:4px 12px 0 5px;">
          <label style="color:#151515;font-family:Trebuchet MS; margin:4px; font-size: 20px;margin">Reason:   </label>
          <label style="color:#151515; font-family:Trebuchet MS; margin:15px; font-size: 16px">$row[able_access_interior_reason]</label>
          
                   
       </td>
      </tr>
      <tr style="float:left;width:100%;height:40px;background: -webkit-gradient(linear, left top, left bottom, from(#f1f1f1), to(#f7f7f7)) !important;background: -moz-linear-gradient(top,  #f1f1f1,  #f7f7f7) !important;filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;  -ms-filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;">
        <td style="float:left;width:500px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px;margin">Exterior A/C Unit </label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:15px; font-size: 16px">$row[exterior_ac_unit]    ,  </label>
        </td>
       
        <td style="float:left;width:500px;height:30px;margin:4px 12px 0 5px;">
          <label style="color:#151515;font-family:Trebuchet MS; margin:4px; font-size: 20px;margin">Notes:   </label>
          <label style="color:#151515; font-family:Trebuchet MS; margin:15px; font-size: 16px">$row[exterior_ac_unit_notes]</label>
          
                   
       </td>
      </tr>
       <tr style="float:left;width:100%;border:1px solid #ccc;margin:15px 0;">
      <td style="float:left;width:100%;padding:10px 0;border-bottom: 1px solid #ccc;">
        <h2 style="text-align:center;color:#151515;font-size: 22px; font-family:Trebuchet MS;font-weight: normal;">Yard Care:   </h2>
	      </td>
    </tr>
    <tr style="float:left;width:100%;height:40px;background: -webkit-gradient(linear, left top, left bottom, from(#f1f1f1), to(#f7f7f7)) !important;background: -moz-linear-gradient(top,  #f1f1f1,  #f7f7f7) !important;filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;  -ms-filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;">
        <td style="float:left;width:500px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px;margin">Completed </label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:15px; font-size: 16px">$row[yard_care_completed];    ,  </label>
        </td>
       
        <td style="float:left;width:500px;height:30px;margin:4px 12px 0 5px;">
          <label style="color:#151515;font-family:Trebuchet MS; margin:4px; font-size: 20px;margin">Lot Size/Height:   </label>
          <label style="color:#151515; font-family:Trebuchet MS; margin:15px; font-size: 16px">$row[yard_care_lot_size_height]</label>
          
                   
       </td>
      </tr>
       <tr style="float:left;width:100%;border:1px solid #ccc;margin:15px 0;">
      <td style="float:left;width:100%;padding:10px 0;border-bottom: 1px solid #ccc;">
        <h2 style="text-align:center;color:#151515;font-size: 22px; font-family:Trebuchet MS;font-weight: normal;">Lock Information   </h2>
	      </td>
    </tr>
    <tr style="float:left;width:100%;height:40px;background: -webkit-gradient(linear, left top, left bottom, from(#f1f1f1), to(#f7f7f7)) !important;background: -moz-linear-gradient(top,  #f1f1f1,  #f7f7f7) !important;filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;  -ms-filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;">
        <td style="float:left;width:500px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px;margin">Installation/Lock change </label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:15px; font-size: 16px">$row[lock_information_installation_change]    ,  </label>
        </td>
       
        <td style="float:left;width:500px;height:30px;margin:4px 12px 0 5px;">
          <label style="color:#151515;font-family:Trebuchet MS; margin:4px; font-size: 20px;margin">Key Code:   </label>
          <label style="color:#151515; font-family:Trebuchet MS; margin:15px; font-size: 16px">$row[lock_information_keycode]</label>
          
                   
       </td>
      </tr>
      <tr style="float:left;width:100%;height:40px;background: -webkit-gradient(linear, left top, left bottom, from(#f1f1f1), to(#f7f7f7)) !important;background: -moz-linear-gradient(top,  #f1f1f1,  #f7f7f7) !important;filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;  -ms-filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;">
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          
          
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px;margin">Location(s)  </label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:15px; font-size: 16px">$row[lock_information_location]    ,  </label>
           
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px;margin"> Lockbox Code:   </label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:15px; font-size: 16px">$row[lock_information_lockbox_code]</label>
          
                   
       </td>
      </tr>
      <tr style="float:left;width:100%;border:1px solid #ccc;margin:15px 0;">
      <td style="float:left;width:100%;padding:10px 0;border-bottom: 1px solid #ccc;">
        <h2 style="text-align:center;color:#151515;font-size: 22px; font-family:Trebuchet MS;font-weight: normal;">Boarding Information   </h2>
	      </td>
    </tr>
    <tr style="float:left;width:100%;height:40px;background: -webkit-gradient(linear, left top, left bottom, from(#f1f1f1), to(#f7f7f7)) !important;background: -moz-linear-gradient(top,  #f1f1f1,  #f7f7f7) !important;filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;  -ms-filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;">
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px;margin">Boarding Needed/Completed:   </label>
          
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
         <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px;margin">Lot Size/Height:    </label> 
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px;margin">$row[boarding_information_location_size]   </label>        
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          
         
        </td>
      </tr>
      <tr style="float:left;width:100%;border:1px solid #ccc;margin:15px 0;">
      <td style="float:left;width:100%;padding:10px 0;border-bottom: 1px solid #ccc;">
        <h2 style="text-align:center;color:#151515;font-size: 22px; font-family:Trebuchet MS;font-weight: normal;">Roof Information   </h2>
	      </td>
    </tr>
      <tr style="float:left;width:100%;height:40px;background: -webkit-gradient(linear, left top, left bottom, from(#f1f1f1), to(#f7f7f7)) !important;background: -moz-linear-gradient(top,  #f1f1f1,  #f7f7f7) !important;filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;  -ms-filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;">
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">Roof Repair/Tarp:    </label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 16px">$row[roof_information_required_complete];</label>
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">Dimensions:   </label>
           <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 16px">$row[roof_information_dimensions]</label>
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          
          
        </td>
      </tr>
      <tr style="float:left;width:100%;border:1px solid #ccc;margin:15px 0;">
      <td style="float:left;width:100%;padding:10px 0;border-bottom: 1px solid #ccc;">
        <h2 style="text-align:center;color:#151515;font-size: 22px; font-family:Trebuchet MS;font-weight: normal;">Pool Information:   </h2>
	      </td>
      
      </tr>
      <tr style="float:left;width:100%;height:40px;background: -webkit-gradient(linear, left top, left bottom, from(#f1f1f1), to(#f7f7f7)) !important;background: -moz-linear-gradient(top,  #f1f1f1,  #f7f7f7) !important;filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;  -ms-filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;">
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">Swimming Pool Secure:</label>
          
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">Date Completed:</label>
           <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 16px">$row[swimming_pool_size]</label>
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          
          
        </td>
      </tr>
      <tr style="float:left;width:100%;height:40px;background: -webkit-gradient(linear, left top, left bottom, from(#f1f1f1), to(#f7f7f7)) !important;background: -moz-linear-gradient(top,  #f1f1f1,  #f7f7f7) !important;filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;  -ms-filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;">
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">Interior A/C Unit:</label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 16px">$row[interior_ac_unit]</label>
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">Notes:</label>
           <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 16px">$row[interior_ac_unit_notes]</label>
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          
          
        </td>
      </tr>
      <tr style="float:left;width:100%;height:40px;background: -webkit-gradient(linear, left top, left bottom, from(#f1f1f1), to(#f7f7f7)) !important;background: -moz-linear-gradient(top,  #f1f1f1,  #f7f7f7) !important;filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;  -ms-filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;">
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">Appliances Present:</label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 16px">$row[appliances_present]</label>
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">Appliances Missing:</label>
           <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 16px">$row[appliances_missing]</label>
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          
          
        </td>
      </tr>
      <tr style="float:left;width:100%;height:40px;background: -webkit-gradient(linear, left top, left bottom, from(#f1f1f1), to(#f7f7f7)) !important;background: -moz-linear-gradient(top,  #f1f1f1,  #f7f7f7) !important;filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;  -ms-filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;">
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">Sump Pump Present:</label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 16px">$row[sump_pump_present]</label>
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">Sump Pump Operational:</label>
           <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 16px">$row[sump_pump_operational]</label>
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          
          
        </td>
      </tr>
      <tr style="float:left;width:100%;border:1px solid #ccc;margin:15px 0;">
      <td style="float:left;width:100%;padding:10px 0;border-bottom: 1px solid #ccc;">
        <h2 style="text-align:center;color:#151515;font-size: 22px; font-family:Trebuchet MS;font-weight: normal;"> Winterization  </h2>
	      </td>
      
      </tr>
      <tr style="float:left;width:100%;height:40px;background: -webkit-gradient(linear, left top, left bottom, from(#f1f1f1), to(#f7f7f7)) !important;background: -moz-linear-gradient(top,  #f1f1f1,  #f7f7f7) !important;filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;  -ms-filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;">
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">Completed:</label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 16px">$row[winterization_completed]</label>
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">If No, Reason:</label>
           <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 16px">$row[winterization_completed_no_reason]</label>
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">Water Off At:</label>
           <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 16px">$row[water_off_at]</label>
          
        </td>
      </tr>
      <tr style="float:left;width:100%;height:40px;background: -webkit-gradient(linear, left top, left bottom, from(#f1f1f1), to(#f7f7f7)) !important;background: -moz-linear-gradient(top,  #f1f1f1,  #f7f7f7) !important;filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;  -ms-filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;">
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">Type Of System:</label>
          
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">$row[winterization_type_system]</label>
           
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          
          
          
        </td>
      </tr>
      <tr style="float:left;width:100%;border:1px solid #ccc;margin:15px 0;">
      <td style="float:left;width:100%;padding:10px 0;border-bottom: 1px solid #ccc;">
        <h2 style="text-align:center;color:#151515;font-size: 22px; font-family:Trebuchet MS;font-weight: normal;"> Debris  </h2>
	      </td>
      
      </tr>
      <tr style="float:left;width:100%;height:40px;background: -webkit-gradient(linear, left top, left bottom, from(#f1f1f1), to(#f7f7f7)) !important;background: -moz-linear-gradient(top,  #f1f1f1,  #f7f7f7) !important;filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;  -ms-filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;">
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">Health & Hazardous Material Removed:   </label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">$row[health_hazardous_material_removed]   </label>
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">Cyds:  </label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px"> $row[health_hazardous_material_removed_cyds]  </label> 
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          
          
          
        </td>
      </tr>
      <tr style="float:left;width:100%;height:40px;background: -webkit-gradient(linear, left top, left bottom, from(#f1f1f1), to(#f7f7f7)) !important;background: -moz-linear-gradient(top,  #f1f1f1,  #f7f7f7) !important;filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;  -ms-filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;">
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">Exterior Debris Removed:   </label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">$row[exterior_debris_removed]   </label>
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">Cyds:  </label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px"> $row[exterior_debris_removed_cyds]  </label> 
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          
          
          
        </td>
      </tr>
      <tr style="float:left;width:100%;height:40px;background: -webkit-gradient(linear, left top, left bottom, from(#f1f1f1), to(#f7f7f7)) !important;background: -moz-linear-gradient(top,  #f1f1f1,  #f7f7f7) !important;filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;  -ms-filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;">
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">Interior Debris Removed:   </label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">$row[interior_debris_removed]   </label>
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">Cyds:  </label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px"> $row[interior_debris_removed_cyds]  </label> 
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          
          
          
        </td>
      </tr>
      <tr style="float:left;width:100%;height:40px;background: -webkit-gradient(linear, left top, left bottom, from(#f1f1f1), to(#f7f7f7)) !important;background: -moz-linear-gradient(top,  #f1f1f1,  #f7f7f7) !important;filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;  -ms-filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;">
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">Personal Property Removed:   </label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">$row[personal_property_removed]   </label>
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">Cyds:  </label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px"> $row[personal_property_removed_cyds]  </label> 
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          
          
          
        </td>
      </tr>
      <tr style="float:left;width:100%;height:40px;background: -webkit-gradient(linear, left top, left bottom, from(#f1f1f1), to(#f7f7f7)) !important;background: -moz-linear-gradient(top,  #f1f1f1,  #f7f7f7) !important;filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;  -ms-filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;">
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">Personal Property Stored:   </label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">$row[personal_property_stored]   </label>
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">Cyds:  </label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px"> $row[personal_property_stored_cyds]  </label> 
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          
          
          
        </td>
      </tr>
      <tr style="float:left;width:100%;height:40px;background: -webkit-gradient(linear, left top, left bottom, from(#f1f1f1), to(#f7f7f7)) !important;background: -moz-linear-gradient(top,  #f1f1f1,  #f7f7f7) !important;filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;  -ms-filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;">
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">Description of items::   </label>
          
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px"> $row[description_items]  </label> 
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          
          
          
        </td>
      </tr>
      <tr style="float:left;width:100%;border:1px solid #ccc;margin:15px 0;">
      <td style="float:left;width:100%;padding:10px 0;border-bottom: 1px solid #ccc;">
        <h2 style="text-align:center;color:#151515;font-size: 22px; font-family:Trebuchet MS;font-weight: normal;"> Cleaning Services  </h2>
	      </td>
      
      </tr>
   <tr style="float:left;width:100%;height:40px;background: -webkit-gradient(linear, left top, left bottom, from(#f1f1f1), to(#f7f7f7)) !important;background: -moz-linear-gradient(top,  #f1f1f1,  #f7f7f7) !important;filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;  -ms-filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;">
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">Cleaning Completed:</label>
          
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          
           <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 16px">$row[cleaning_completed]</label>
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          
          
        </td>
      </tr>
      <tr style="float:left;width:100%;border:1px solid #ccc;margin:15px 0;">
      <td style="float:left;width:100%;padding:10px 0;border-bottom: 1px solid #ccc;">
        <h2 style="text-align:center;color:#151515;font-size: 22px; font-family:Trebuchet MS;font-weight: normal;">Damages</h2>
	      </td>
    </tr>
    
      <tr style="float:left;width:100%;height:40px;background: -webkit-gradient(linear, left top, left bottom, from(#f1f1f1), to(#f7f7f7)) !important;background: -moz-linear-gradient(top,  #f1f1f1,  #f7f7f7) !important;filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;  -ms-filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;">
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">Mold Present:</label>
          
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          
           <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 16px">$row[mold_present]</label>
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          
          
        </td>
      </tr>
      
    <tr style="float:left;width:100%;height:40px;background: -webkit-gradient(linear, left top, left bottom, from(#f1f1f1), to(#f7f7f7)) !important;background: -moz-linear-gradient(top,  #f1f1f1,  #f7f7f7) !important;filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;  -ms-filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;">
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">List Locations, Sources, and Dimensions (L x W) of Each Instance of Mold Below:</label>
          
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">$row[list_locations_direction]</label>
           <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 16px">$row[list_locations_direction1]</label>
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          
          
        </td>
      </tr>
      <tr style="float:left;width:100%;height:40px;background: -webkit-gradient(linear, left top, left bottom, from(#f1f1f1), to(#f7f7f7)) !important;background: -moz-linear-gradient(top,  #f1f1f1,  #f7f7f7) !important;filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;  -ms-filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;">
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">Other Damages To Report:  </label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 16px">$row[other_damages_report]</label>
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          
          
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          
          
        </td>
      </tr>
      <tr style="float:left;width:100%;height:40px;background: -webkit-gradient(linear, left top, left bottom, from(#f1f1f1), to(#f7f7f7)) !important;background: -moz-linear-gradient(top,  #f1f1f1,  #f7f7f7) !important;filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;  -ms-filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;">
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">Interior:</label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 16px">$row[other_damages_report_interior]</label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">$row[other_damages_report_interior1]</label>
                  
          
           </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          
          
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          
          
        </td>
      </tr>
      <tr style="float:left;width:100%;height:40px;background: -webkit-gradient(linear, left top, left bottom, from(#f1f1f1), to(#f7f7f7)) !important;background: -moz-linear-gradient(top,  #f1f1f1,  #f7f7f7) !important;filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;  -ms-filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;">
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">Exterior:</label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 16px">$row[other_damages_report_exterior]</label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">$row[other_damages_report_exterior1]</label>
                  
          
           </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          
          
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          
          
        </td>
      </tr>
      <tr style="float:left;width:100%;height:40px;background: -webkit-gradient(linear, left top, left bottom, from(#f1f1f1), to(#f7f7f7)) !important;background: -moz-linear-gradient(top,  #f1f1f1,  #f7f7f7) !important;filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;  -ms-filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#f1f1f1, endColorstr=#f7f7f7) !important;">
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">Others:</label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 16px">$row[other_damages_report_other]</label>
          <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">$row[other_damages_report_other1]</label>
           <label style="color: #151515;font-family:Trebuchet MS; margin:4px; font-size: 20px">$row[other_damages_report_other2]</label>       
          
           </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          
          
        </td>
        <td style="float:left;width:250px;height:30px;margin:4px 12px 0 5px;">
          
          
        </td>
      </tr>
      
    
  
EOD;
$f_html.="</table>"; 
	



// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('TCPDF Example 021');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');



// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 9);

// add a page
$pdf->AddPage();

// create some HTML content
 


$pdf->Ln(5);
 

 
 
 


// output the HTML content
$pdf->writeHTML(  $f_html, true, 0, true, 0);

// reset pointer to the last page
$pdf->lastPage();
//}
// ---------------------------------------------------------







$pdf->Output('workorder.pdf', 'I');


