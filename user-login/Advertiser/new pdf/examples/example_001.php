<?php
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2012-07-25
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               Manor Coach House, Church Hill
//               Aldershot, Hants, GU12 4RQ
//               UK
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */

require_once('../config/lang/eng.php');
require_once('../tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('TCPDF Example 001');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
$pdf->setFooterData($tc=array(0,64,0), $lc=array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 14, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

// Set some content to print
$html = <<<EOD
 <div id="dialog1" class="web_dialog1">
        <table style="width: 100%; border: 0px;" cellpadding="3" cellspacing="0">
            <tr>
                <td class="web_dialog_title1">EDIT WORK ORDER</td>
				
            </tr>
        </table>
	<div class="add_note_div1">
	
      <form action="workorder-detail.php" method="post" name="myform" id="myform">
        <table  border="0" align="center" cellpadding="3" cellspacing="3" class="forms">
          <tr>
             <td>Workorder ID</td> 
              <td><input name="workorder_id" type="text" id="workorder_id" value="fdghfdhfgh" class="required digits"></td>
          </tr>
		  <tr>
             <td>Customer</td> 
              <td><input name="customer" type="text" id="customer" value="hfhfghfhfg" class="required digits"></td>
          </tr>
		  <tr>
             <td>Customer #</td> 
              <td><input name="customer_id" type="text" id="customer_id" value="fhfhfhfh" class="required digits"></td>
          </tr>
		  <tr> 
            <td>Workorder Type</td>
            <td width="73%"><input name="workorder_type" type="text" id="workorder_type" value="fghfhfgh" class="required"></td>
          </tr>
          <tr> 
             <td>Rush Order</td> 
              
		  </tr>
		      <td>Due Date</td> 
              <td><input name="due_date" type="text" id="due_date" value="fhfhfghgf" class="required" onclick ='ds_sh(this);'></td>
          </tr>
		    <td>Assigned Date</td> 
              <td><input name="assigned_date" type="text" id="assigned_date" value="hfhfhfh" class="required" onclick ='ds_sh(this);'></td>
          </tr>
		  <tr> 
             <td>Notes (Work To Be Done)</td> 
              <td><textarea rows="4" cols="32" class="required" name="notes1" id="notes1"  value="">fhfhfhfhfhfhf</textarea></td>
		  </tr>
		  <tr> 
             <td>Notes (Special)</td>
              <td><textarea rows="4" cols="32" class="required" name="notes2" id="notes2"  value="">hfhfhfhfhfh</textarea></td>
          </tr>
          <input type="hidden" name="user_id" value="fhfhfhf">
          <div class="loginbtn">
		  <tr> 
          <td><p align="center"> 
          <input name="doSave" type="submit" id="doSave" value="Update">
        </p></td>
          </tr>
        </table>
        
      </form>
	 
	</div>	
EOD;

// Print text using writeHTMLCell()
$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $html, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('example_001.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
