<?php


require_once('../config/lang/eng.php');
require_once('../tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('TCPDF Example 015');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 015', PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// Bookmark($txt, $level=0, $y=-1, $page='', $style='', $color=array(0,0,0))

// set font
$pdf->SetFont('times', 'B', 20);




$pdf->AddPage();

// set a bookmark for the current position
$pdf->Bookmark('column1', 0, 0, '', 'B', array(0,64,128));

// print a line using Cell()
$pdf->Cell(0, 10, 'column1', 0, 1, 'L');

$pdf->SetFont('times', 'I', 14);

$hello='firstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirst
firstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirs tfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirs     tfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfi
rstfirstfirstfirstfirstfirsrstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirstfirst';
$pdf->Write(0,$hello);





/*
// add a page
$pdf->AddPage();

// set a bookmark for the current position
$pdf->Bookmark('Policy1', 0, 0, '', 'B', array(0,64,128));

// print a line using Cell()
$pdf->Cell(0, 10, 'Policy1', 0, 1, 'L');

$pdf->SetFont('times', 'I', 14);

$hello='dfdfdfd This is first This is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is firstThis is first  ';
$pdf->Write(0,$hello);









// add other pages and bookmarks




/*

$pdf->AddPage();
// add a named destination so you can open this document at this page using the link: "example_015.pdf#chapter2"
$pdf->setDestination('Policy2', 0, '');
$pdf->Bookmark('Policy2', 0, 0, '', 'BI', array(128,0,0));
$pdf->Cell(0, 10, 'Policy2', 0, 1, 'L');
$pdf->SetFont('times', 'I', 14);
$pdf->Write(0, 'This is Second This is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is SecondThis is Second ');







$pdf->AddPage();
$pdf->setDestination('Policy3', 0, '');
$pdf->SetFont('times', 'B', 20);
$pdf->Bookmark('Policy3', 0, 0, '', 'B', array(0,64,128));
$pdf->Cell(0, 10, 'Policy3', 0, 1, 'L');
$pdf->SetFont('times', 'I', 14);
$pdf->Write(0, 'This is third This is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is thirdThis is third ');




$pdf->AddPage();
$pdf->setDestination('Policy4', 0, '');
$pdf->SetFont('times', 'I', 14);
$pdf->Write(0, 'This is fourth  This is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourth ');
$pdf->Bookmark('Policy4', 0, 0, '', 'B', array(0,64,128));
$pdf->Cell(0, 10, 'Policy4', 0, 1, 'L');



$pdf->AddPage();
$pdf->setDestination('Policy4', 0, '');
$pdf->SetFont('times', 'I', 14);
$pdf->Write(0, 'This is fourth  This is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourth ');
$pdf->Bookmark('Policy4', 0, 0, '', 'B', array(0,64,128));
$pdf->Cell(0, 10, 'Policy4', 0, 1, 'L');




$pdf->AddPage();
$pdf->setDestination('Policy4', 0, '');
$pdf->SetFont('times', 'I', 14);
$pdf->Write(0, 'This is fourth  This is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourth ');
$pdf->Bookmark('Policy4', 0, 0, '', 'B', array(0,64,128));
$pdf->Cell(0, 10, 'Policy4', 0, 1, 'L');






$pdf->AddPage();
$pdf->setDestination('Policy4', 0, '');
$pdf->SetFont('times', 'I', 14);
$pdf->Write(0, 'This is fourth  This is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourth ');
$pdf->Bookmark('Policy4', 0, 0, '', 'B', array(0,64,128));
$pdf->Cell(0, 10, 'Policy4', 0, 1, 'L');





$pdf->AddPage();
$pdf->setDestination('Policy4', 0, '');
$pdf->SetFont('times', 'I', 14);
$pdf->Write(0, 'This is fourth  This is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourth ');
$pdf->Bookmark('Policy4', 0, 0, '', 'B', array(0,64,128));
$pdf->Cell(0, 10, 'Policy4', 0, 1, 'L');





$pdf->AddPage();
$pdf->setDestination('Policy4', 0, '');
$pdf->SetFont('times', 'I', 14);
$pdf->Write(0, 'This is fourth  This is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourthThis is fourth ');
$pdf->Bookmark('Policy4', 0, 0, '', 'B', array(0,64,128));
$pdf->Cell(0, 10, 'Policy4', 0, 1, 'L');






*/





// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('example_015.pdf', 'I');


               