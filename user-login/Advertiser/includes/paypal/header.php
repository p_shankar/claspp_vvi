<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Advertiser Dashboard</title>
    <link href="../../css/style.css" rel="stylesheet" type="text/css"  />
    <link href="../../css/dashboard.css" rel="stylesheet" type="text/css" />
    <link href="../../Font_Kit/stylesheet.css" rel="stylesheet" type="text/css" />
    <link href="../../css/tabs.css" rel="stylesheet" type="text/css" media="screen" />
     <link rel="stylesheet" type="text/css" media="screen" href="../../Lato_Font_Kit/stylesheet.css">
	 <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">

    <!--********** include jquery core ************-->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>

    <!--********** include validate.js************-->
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>

    <!--********** include blockUIs.js************-->
    <script src="../../js/blockUI.js"></script>

    <!--********** include form.js************-->
    <script type="text/javascript" src="../../js/jquery.form.js"></script>

    <!--********** custom js file on dashboard side************-->
    <script type="text/javascript" src="../../js/espy-advertiser-validate.js"></script>

    <!-- tabs js --->
    <script type="text/javascript" src="../../js/tabs.js"></script>

</head>

<body>
<!--scripot for tooltip-->
<script type="text/javascript" src="<?php echo $site_url;?>/tooltip/wz_tooltip.js"></script>

<div id="main_wrapper">
<div class="top_panel">
    <div id="work_container">
        <div class="welcome_text">
            <p>
             
            </p>
        </div>
    <a href="../../logout.php" class="logout"></a> </div>
</div>
<div id="work_container">
<div id="header">
    <div id="logo"><a href="../../index.php"><img src="../../images/logo.png" /></a></div>
    <div class="top_image">
	  <div class="top_image_div">
<?php 
$thumbfile= 'image_thumb_guest.php';  

?>

      		<?php  if(!empty($pub_data['image_url'])){ ?>
        <img src="<?php echo $thumbfile; ?>?image_url=<?php  echo $site_url.'/user-login/Advertiser/images/u_images/'.$pub_data['image_url']; ?>"  alt="Profile Picture">
		<?php  } else { ?>
  <img src="images/avatar.png"  alt="Profile Picture">

	<?php	} ?>
	</div>
        <div class="top_image_text">
            <p><?php echo $pub_data['name'].' '.$pub_data['last_name'] ;?></p>
			<p><?php echo $pub_data['company'];  ?></p>

        </div>
</div>
