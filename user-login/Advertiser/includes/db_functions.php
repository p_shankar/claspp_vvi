<?php

/**
*  this file contains functions to update or retrieve database
*
*
*/

if(!function_exists('get_duration')){
    function get_duration(){
        $duration_query = 'select * from espy_duration' ;
        $duration_results = mysql_query($duration_query);
         return $duration_results;
    }
}

if(!function_exists('get_gender')){
    function get_gender(){
        $gender_query = 'select * from espy_gender order by id DESC ' ;
        $gender_results = mysql_query($gender_query);
         return $gender_results;
    }
}

if(!function_exists('get_age_groups')){
    function get_age_groups(){
        $age_query = 'select * from espy_age_groups' ;
        $age_results = mysql_query($age_query);
         return $age_results;
    }
}


if(!function_exists('get_radius')){
    function get_radius(){
        $radius_query = 'select * from espy_radius' ;
        $radius_results = mysql_query($radius_query);
         return $radius_results;
    }
}

if(!function_exists('get_zip')){
    function get_zip(){
        $zipcode_query = 'select * from espy_zip' ;
        $zipcode_results = mysql_query($zipcode_query);
         return $zipcode_results;
    }
}

if(!function_exists('get_household_income')){
    function get_household_income(){
        $income_query = 'select * from espy_household_income' ;
        $income_results = mysql_query($income_query);
         return $income_results;
    }
}

if(!function_exists('get_countries')){
    function get_countries(){
        $country_query = 'select * from espy_countries' ;
        $country_results = mysql_query($country_query);
        return $country_results;
    }
}

if(!function_exists('get_cities')){
    function get_cities(){
        $city_query = 'select * from espy_cities where city ="Indianapolis"' ;
        $city_results = mysql_query($city_query );
        return $city_results;
    }
}
if(!function_exists('get_campaign_categories')){
    function get_campaign_categories(){
        $category_query = 'select * from espy_campaign_categories' ;
        $category_results = mysql_query($category_query);
         return $category_results;
    }
}

if(!function_exists('get_campaigns_list')){
    function get_campaigns_list($u_id='',$start='',$end='' ){
        $query .= 'select
                        cp.id,
                        cp.title,
                        cp.url ,
                        cp.description ,
                        cp.status ,
                        cp.target_category,
                        cp.created,
						cp.campaign_type,
                        cp.campaign_image,
                        cpb.budget,
                        cpb.impressions,
                        cpb.posted,
                        cpb.remaining_posts,
                        cpb.per_post_payment,
                        click.count
                from
                    espy_campaign as cp
                left join
                    espy_campaign_budget as cpb
                on
                    cp.id = cpb.campaign_id
                left join
                    espy_click_counts as click
                on
                    cp.id = click.ad_id ';

        if(!empty($u_id)){
            $query .= 'where cp.advertiser_id = "'.$u_id.'"';
        }

		if(!empty($start) && !empty($end)) {

		  $query.=' and created between "'.$start.'"  and   "'.$end.'"';



		}
           $query.=' order by cp.created  desc,cp.id  desc ';
         

            $results = mysql_query($query);
            
         return $results;
    }
}




	function get_categories(){
$category_query = 'select * from comp_category' ;
$category_results = mysql_query($category_query);
return $category_results;
}

function get_sub_categories($s){
$category_query = 'select * from comp_sub_cat where cat_id='.$s.'';
$category_results = mysql_query($category_query);
return $category_results;
}




/************ To Calulate  impression  for campgihns 
Creted on 8/27/2013

*******************/

function impression($id1)
{

 $result2 = mysql_query("SELECT sum(`impression`)as total_impression  FROM `espy_postedads` WHERE `pads_adid`= '$id1'"); 
$row2 = mysql_fetch_assoc($result2); 
$sum = $row2['total_impression'];

if(!empty($sum)) {

$total = $sum;

} else {

$total = "0";


} 
return $total;
}
