
$(document).ready(function() {






   $.validator.addMethod("valueNotEquals", function(value, element, arg){
  return arg != value;
 }, "Value must not equal arg.");
   /***********update profile validation ************/
              $("#advertiser_form").validate({
							rules: {
							first_name: {
							required: true,
							alphabets:  true

							},
							last_name: {
							required: true,
							alphabets:  true

							},
							phone: {
							required: true,
							number:  true,
							minlength:8

							},
							website:{
							required:true
							},  
							email:"email",   
							city: "required",
							state:{ valueNotEquals: "default" }	
							},
					messages: {

					first_name: {
					required  : "Please enter your firstname",
					alphabets: "Please enter alphabets only "
					},
					last_name: {
					required  : "Please enter your lastname",
					alphabets: "Please enter alphabets only "
					},
					phone: {
					required:"Please Enter  Phone Number",
					number:"Please enter numbers only",
					minlength:"Please enter at least 8 characters. "
					}, 
					website:{
					required:"Please enter url of your website"
					},		
					email:"Ener a valid email ",

					city: "Please enter City name ",
					state:{ valueNotEquals: "Please select a State" }
					},
                 submitHandler: function(form) {
                    form.submit();
                }

			  });













   
    /**************************************
    * setting tabs
    ***************************************/



   $("#direct_payment").validate({
                rules: {
                    firstName: "required",
                    lastName: "required",
                    creditCardType: "required",
                     expDateMonth:"required", 
						 creditCardNumber: {
                        required: true,
                        minlength: 10
                    },
                     expDateYear:"required", 
                      cvv2Number:"required"
					
                },
                messages: {
                    firstname: "Please enter your firstname",
                    lastname: "Please enter your lastname",
                     creditCardType: "Please select card type ",
                    creditCardNumber: {
                        required: "Please select card number   ",
                        minlength: "Please enter valid card number  "
                    },
                    expDateMonth: "Please select month for expiry",
                    expDateYear: "Please select  year for expiry",

					cvv2Number: "Please enter cwc nuymber "
                },
                submitHandler: function(form) {
                    form.submit();
                }
            });

 









    $('.setting_tab').click(function(){

        $('.current').removeClass('current');
        $('.set_content_show').removeClass('set_content_show');
        $(this).addClass('current');
        var show_tab_content = $(this).attr('rel');

        $(show_tab_content).addClass('set_content_show');
    });


   

    /***********update geographic information************/

    $("#advt_info_form").validate({
         
        submitHandler: function(form) {

            var form = $('#advt_info_form');
            $.ajax({
                url: form.attr( 'action' ),
                type: 'POST',
                data: form.serialize(),
                success: function() {
                    $.blockUI({
                        message: '<h1 class="pop_message">Updated geographic information..</h1>',
						css: {
                        left:500 
                         },
                        timeout: 1000
                    });

                }
            });

            return false;
        }

    });


   $('.the_button').click(function(){



$('.the_button').removeClass('active');
$(this).addClass('active');


});







    /***********update email preference***********
    $("#active_emails_form").validate({

        submitHandler: function(form) {

            var form = $('#active_emails_form');
            $.ajax( {
                type: "POST",
                url: form.attr( 'action' ),
                data: form.serialize(),
                success: function( response ) {
                    $.blockUI({
                        message: '<h1>Updated email preference.</h1>',
                        timeout: 1000
                    });

                }
            } );
            return false;
        }
    });*/

    /************ change password*********
    $("#change_pass").validate({
        rules: {
            'ad_0ldpass': {
                required: true
            },
            'ad_newpass': {
                required: true
            },
            'ad_cnfmpass': {
                required: true,
                equalTo: '#ad_newpass'
            }
        },
        submitHandler: function(form) {

            var form = $('#change_pass');

            $.ajax( {
                type: "POST",
                url: form.attr( 'action' ),
                data: form.serialize(),
                success: function(response ) {
                    if(response == -1){
                        $('#ad_oldpass').val('');
                        $('#ad_oldpass').attr('style','color:red');
                        $('#ad_oldpass').attr('placeholder','Old pass do not match');

                        $('#ad_oldpass').focus(function(){
                            $('#ad_oldpass').attr('style','color:#CCCCCC');
                            $('#ad_oldpass').attr('placeholder','');
                        });

                    }else if(response == -2){
                        $('#ad_oldpass').val('');
                        $('#ad_newpass').val('');
                        $('#ad_cnfmpass').val('');
                        $.blockUI({
                            message: '<h1>Password changed successfully.</h1>',
                            timeout: 1000
                        });
                        

                    }

                }
            });
            return false;
        }
    });*/
    /**********************/



    $('#upload_pic').change(function(){

				$.blockUI({
				message: "<div id='thank' class='pop_message' >Please wait while image is uploading....</div>",
				 css: {
                 left:500 
                  },
				timeout: 3000
				});
       
        $('#publishers_pic').ajaxSubmit({
            success:  function(res) {
                       
 $.unblockUI();
				if(res=="no") {
         alert("no");
				} else {

                      $("#profile_right_img img ").attr('src','images/temp/'+res+'');
					   $("#profile_right_img img ").attr('height','60');
					    $("#profile_right_img img ").attr('width','80');
						$("#up_image").val('1');
						$("#image_name").val(res);
						
				}

            }
        });
    });

/*    $('#upload_adpic').change(function(){
        $.blockUI({
                        message: '<h1>loading image...</h1>'
                    });
      
        $('#advertiser_pic').ajaxSubmit({
            success:  function(res) {
                if(res == -1){
                    
                    $('#prof_pic_div').load('index.php #prof_pic_div');
                    $('#prof_pic_div1').load('index.php #prof_pic_div1 img');
                    $('#prof_pic_div2').load('index.php #prof_pic_div2 img',function(){
                        $.unblockUI();
                    });
                   
                    

                } else if(res == -2){
                    $.blockUI({
                        message: '<h1>Invalid file type.</h1>',
                        timeout: 3000
                    });

                } else {
                    $.blockUI({
                        message: '<h1>Error in image uploading./h1>',
                        timeout: 3000
                    });
                }
            }

        });

    }); */

    
    /*****************************************
     *create campaign
     ****************************************/
    $('#campaign_create').click(function(){
								
		
               
        var loc = window.location;
        var redirect_url = loc.protocol + "//" + loc.host;
        var paths = loc.pathname.split('/');
        var total = paths.length;
        $.each(paths, function(key, path) {
            if(key < (total-1) ){
                redirect_url = redirect_url+path+'/';
            }
        });
        
        var okk = $("#create_campaign_form").valid();

       if($(".social_ckeckbox>input:checkbox:checked").length == 0){
            $('.media_error').show();
            return false;
        }

      if($("#c_target_category >li> input:checkbox:checked").length == 0){

            $('.cat_error').show();
            return false;
        }	 


		  if($("#c_target_city >li> input:checkbox:checked").length == 0){
              
				$('.city_error').show();
				$('.cat_error').hide();
      
            return false;
        }	 

			$('.city_error').hide();
				$('.cat_error').hide();
 
           
        if(okk == true){
             $('#campaign_img').focus();

                    $.blockUI({
                            message: '<div class="pop_message">Please wait while we are publishing your campaign, after publishing you can view the same in Campaign list page.</div>'
                        });
					
                        
            $('#create_campaign_form').ajaxSubmit({


                success:  function(res) {
				
			//alert(res);

                    if(res == -2){
                         $('#campaign_img').focus();
						  $.unblockUI();
                        $.blockUI({
                            message: '<div class="pop_message2">Uploaded Image type not supported, please try again.</div>',
							css: {
                           left:500 
                            },
                            timeout: 3000
                        });
                        window.location= redirect_url ;
                    }else if(res == -5){
						 $.unblockUI();
                       $('#campaign_img').focus();
                        $.blockUI({
                            message: '<div class="pop_message2">Image Height and Width should be greater than 200px.</div>',
							css: {
          					left:500 
          					},
                            timeout: 3000
                        });
                    }  else if(res == -3 || res == -4){
						 $.unblockUI();
                       $('#campaign_img').focus();
                        $.blockUI({
                            message: '<h1 class="pop_message2">Error in file uploading, please try again.</h1>',
							css: {
          					left:500 
          					},
                            timeout: 3000
                        });
                    } else {
                       $('#payment_redirect').html(res);
                    }
                }

            });
        }
        return false;
    });








	   /*******************************   Update compain function ******************/

	    $('#cam_update').click(function(){
								
				
               
        var loc = window.location;
        var redirect_url = loc.protocol + "//" + loc.host;
        var paths = loc.pathname.split('/');
        var total = paths.length;
        $.each(paths, function(key, path) {
            if(key < (total-1) ){
                redirect_url = redirect_url+path+'/';
            }
        });
        
        var okk = $("#create_campaign_form").valid();

           if($(".social_ckeckbox>input:checkbox:checked").length == 0){
            $('.media_error').show();
            return false;
        }

      if($("#c_target_category >li> input:checkbox:checked").length == 0){
            $('.cat_error').show();
            return false;
        }	 


		  if($("#c_target_city >li> input:checkbox:checked").length == 0){
              
				$('.city_error').show();
				$('.cat_error').hide();
      
            return false;
        }	 

			$('.city_error').hide();
				$('.cat_error').hide();

		
           
        if(okk == true){
             $('#campaign_img').focus();

			   $.blockUI({
                            message: '<div class="pop_message"> Please wait while campaign is updating....</div>'
                        });
                     
                        
            $('#create_campaign_form').ajaxSubmit({

                success:  function(res) {

                    if(res == -2){
						 $.unblockUI();
                         $('#campaign_img').focus();
                        $.blockUI({
                            message: '<div class="pop_message2">Uploaded Image type not supported, please try again.</div>',
							 css: {
                             left:500 
                             },
                            timeout: 3000
                        });
                        window.location= redirect_url ;
                    } else if(res == -3 || res == -4){
						 $.unblockUI();
                       $('#campaign_img').focus();
                        $.blockUI({
                            message: '<div class="pop_message2">Error in file uploading, please try again.</div>',
							css: {
                             left:500 
                             },
                            timeout: 3000
                        });
                    } else if(res == -5){
						 $.unblockUI();
                       $('#campaign_img').focus();
                        $.blockUI({
                            message: '<div class="pop_message2">Image Height and Width should be greater than 200px.</div>',
							css: {
                             left:500 
                             },
                            timeout: 3000
                        });
                    } 	else {
						 $.unblockUI();
                        $.blockUI({
                            message: '<div class="pop_message">Campaign updated successfully</div>',                            
                            onBlock:function(){
                                location.href= 'http://claspp.com/beta/admin/compaign.php';
                            },
							css: {
                             left:500 
                             },
                            timeout: 3000
                        });

					setTimeout(function(){
		opener.location.reload(); 
		window.close();
		},'3000');

                    }
                }

            });
        }
        return false;
    });


 /****************************   Ens compain update function **********************






    /*****************************************
       Relaunch  campaign
     ****************************************/
    $('#campaign_relaunch').click(function(){
								
	

			$('#deliver_case').val(2);
               
        var loc = window.location;
        var redirect_url = loc.protocol + "//" + loc.host;
        var paths = loc.pathname.split('/');
        var total = paths.length;
        $.each(paths, function(key, path) {
            if(key < (total-1) ){
                redirect_url = redirect_url+path+'/';
            }
        });
        
        var okk = $("#create_campaign_form").valid();

           if($(".social_ckeckbox>input:checkbox:checked").length == 0){
            $('.media_error').show();
            return false;
        }

      if($("#c_target_category >li> input:checkbox:checked").length == 0){
            $('.cat_error').show();
            return false;
        }	 


		  if($("#c_target_city >li> input:checkbox:checked").length == 0){
            $('.city_error').show();
            return false;
        }	 

	
           
        if(okk == true){
             $('#campaign_img').focus();
                       $.blockUI({
                            message: '<div class="pop_message">Please wait as we redirect you to checkout page.</div>'
                        });
                    
     $('#create_campaign_form').ajaxSubmit({

                success:  function(res) {

                    if(res == -2){
                         $('#campaign_img').focus();
                        $.blockUI({
                            message: '<div class="pop_message2">Uploaded Image type not supported, please try again.</div>',
							css: {
                            left:500 
                            },
                            timeout: 3000
                        });
                        window.location= redirect_url ;
                    } else if(res == -5){
						 $.unblockUI();
                       $('#campaign_img').focus();
                        $.blockUI({
                            message: '<div class="pop_message2">Image Height and Width should be greater than 200px.</div>',
							css: {
                            left:500 
                            },
                            timeout: 3000
                        });
                    }else if(res == -3 || res == -4){
                       $('#campaign_img').focus();
                        $.blockUI({
                            message: '<div class="pop_message2">Error in file uploading, please try again.</div>',
							css: {
                            left:500 
                            },
                            timeout: 3000
                        });
                    } else {
                       $('#payment_redirect').html(res);
                    }
                }

            });			


        }
        return false;
    });







    $(".social_ckeckbox>input:checkbox").click(function(){
        $('.media_error').hide();
    });

    /********************************************/
    $("#ad_type").change(function(){
        var val = $(this).val();
        if(val == 'small_ad'){
            $('#on_medium').hide();
        } else {
            $('#on_medium').show();
        }
    });

    /********************************************/
    $("#post_campaign_budget").blur(function(){
        var val = $(this).val();
        var fee = $('#campaign_fee_hidden').val();

        var comp = (val-(val*fee)/100) ;
        $('#post_pub_comp').val(comp);
        $('#post_comp_per_post').removeAttr('readonly');

    });

    /*********************     Pre compaing budhet changed               ***********************/
    $("#pre_comp_per_post").change(function(){
        var val = $(this).val();
		var pro = $('#pre_projected_posts').val();
        
        $.ajax( {
            type: "POST",
            url: 'includes/espy-advertiser-ajax.php',
            data: 'action=calculateBData&comp_post='+val+'&projected='+pro,
            success: function( response ) {

                var array = response.split('&');
                var comp_per_post = array[0].split('=')
                var posts = array[1];
                var net_imp = array[2];



            
				  $('#pre_net_imp').val(net_imp);
				    $('#pre_campaign_budget').val(posts);
 
                  
            }
        } );

    });


    $("#pre_projected_posts").change(function(){
        var val = $(this).val();
		var pro = $('#pre_comp_per_post').val();
        
        $.ajax( {
            type: "POST",
            url: 'includes/espy-advertiser-ajax.php',
            data: 'action=calculateBData2&comp_post='+pro+'&projected='+val,
            success: function( response ) {

                var array = response.split('&');
                var comp_per_post = array[0].split('=')
                var posts = array[1];
                var net_imp = array[2];



            
				  $('#pre_net_imp').val(net_imp);
				    $('#pre_campaign_budget').val(posts);
 
                  
            }
        } );

    });








    /********************************************/
    $("#post_comp_per_post").blur(function(){
        var valPerPost = $(this).val();
   
        if(valPerPost != '' ){
            var pubComp = $('#post_pub_comp').val();
            
            var userBase = $('#user_base_hidden').val();

            var pPosts = Math.round(pubComp/valPerPost);

            var net_imp = pPosts*userBase;

            $('#post_projected_posts').val(pPosts );

            $('#post_net_imp').val(net_imp );
        }

    });

    /**********************/
    $('#campaign_img_btn').click(function(){
        $('#upload_campaign_img').trigger('click');
    });


    $('#upload_campaign_img').change(function(){
        var a = $(this).val();
        $('#campaign_img').val(a)
    });


    /********* .pre_toggle_geo *******/
    $('.pre_toggle_geo').click(function(){

       
        

         if( $('.toggle_geo1').css('display') == 'block'){
             $('.pre_toggle_geo > img').attr('src','../../images/down.png');
              $('.toggle_geo1').hide('slow');

              $('.toggle_geo2').show('slow');
              $( '.c_toggle_geo > img').attr('src','../../images/up.png');
        }else{
            $('.pre_toggle_geo > img').attr('src','../../images/up.png');
            $('.toggle_geo1').show('slow');

             $('.toggle_geo2').hide('slow');
              $( '.c_toggle_geo > img').attr('src','../../images/down.png');
        }


        $('#post_campaign_budget').attr('disabled',true);
        $('#post_comp_per_post').attr('disabled',true);
        $('#post_projected_posts').attr('disabled',true);
        $('#post_net_imp').attr('disabled',true);
        $('#post_pub_comp').attr('disabled',true);

        $('#pre_campaign_budget').attr('disabled',false);
        $('#pre_comp_per_post').attr('disabled',false);
        $('#pre_projected_posts').attr('disabled',false);
        $('#pre_net_imp').attr('disabled',false);


    });
    $('.c_toggle_geo').click(function(){

        $('.toggle_geo1').hide('slow');


       

         if( $('.toggle_geo2').css('display') == 'block'){
             $( '.c_toggle_geo > img').attr('src','../../images/down.png');
              $('.toggle_geo2').hide('slow');
              
                $('.toggle_geo1').show('slow');
                $('.pre_toggle_geo > img').attr('src','../../images/up.png');

        }else{
            $( '.c_toggle_geo > img').attr('src','../../images/up.png');
            $('.toggle_geo2').show('slow');

              $('.toggle_geo1').hide('slow');
              $('.pre_toggle_geo > img').attr('src','../../images/down.png');
        }

        
        $('#pre_campaign_budget').attr('disabled',true);
        $('#pre_comp_per_post').attr('disabled',true);
        $('#pre_projected_posts').attr('disabled',true);
        $('#pre_net_imp').attr('disabled',true);

        $('#post_campaign_budget').attr('disabled',false);
        $('#post_comp_per_post').attr('disabled',false);
        $('#post_projected_posts').attr('disabled',false);
        $('#post_net_imp').attr('disabled',false);
        $('#post_pub_comp').attr('disabled',false);


    });

    /*********  *******/
    /*$('.ul_select').hover(
    
    function(){
        var id = $(this).attr('id')
        $('.li_'+id).show('fast');

   } ,

    function(){
       
       var id = $(this).attr('id')
        $.each($('.li_'+id+'>input:checkbox'),function(i,val){
            if($(this).is (':checked') == false){
                 $(this).parent().hide('fast');
               }
        });
       
        
   });*/

/******multiple select*******/
    $(".ul_select").on(
    {
        click: function()
        {
            var id = $(this).attr('id')
            $('.li_'+id).show('fast');
        },
        mouseleave: function()
        {
            var id = $(this).attr('id')
            $.each($('.li_'+id+'>input:checkbox'),function(i,val){
                if($(this).is (':checked') == false){
                    $(this).parent().hide('fast');
                }
            });
        }
    });
    
   /*********** show_hide_pass ************/

    $(".show_hide_pass").click(function(){
        $('.password_div').toggle(function(){
            if( $('.password_div').css('display') == 'block'){
                 $('img.show_hide_pass').attr('src','../../images/up.png');
                $('#ad_newpass').attr('disabled',false),
                $('#ad_cnfmpass').attr('disabled',false)
            }else{
                 $('img.show_hide_pass').attr('src','../../images/down.png');
                 $('#ad_newpass').attr('disabled',true),
                $('#ad_cnfmpass').attr('disabled',true)
            }

        });
    });

 /*********** add method ************/
    $.validator.addMethod("alphabets", function(value,element)
        {
           return this.optional(element) || /^[a-zA-Z]+$/.test(value);
        }, "Alphabets only");



});
   

  function  redirect_to() {

if(confirm('Are you sure you want to cancel editting your campaign?')) {

window.location.href = 'http://debutinfotech.com/espylink/user-login/Advertiser/index.php';


} else {


return false;

}


  }

/******************************************** show image preview before upload function ***************************/
  function ShowImagePreview( files )
{
	 
    if( !( window.File && window.FileReader && window.FileList && window.Blob ) )
    {
     
      return false;
    }

    if( typeof FileReader === "undefined" )
    {
        alert( "Filereader undefined!" );
        return false;
    }

    var file = files[0];

    if( !( /image/i ).test( file.type ) )
    {
        alert( "File is not an image." );
        return false;
    }

    reader = new FileReader();
    reader.onload = function(event) 
            { var img = new Image; 
              img.onload = UpdatePreviewCanvas; 
              img.src = event.target.result;  }
    reader.readAsDataURL( file );
}

  function ShowImagePreviews( files )
{
	 
    if( !( window.File && window.FileReader && window.FileList && window.Blob ) )
    {
     
      return false;
    }

    if( typeof FileReader === "undefined" )
    {
        alert( "Filereader undefined!" );
        return false;
    }

    var file = files[0];

    if( !( /image/i ).test( file.type ) )
    {
        alert( "File is not an image." );
        return false;
    }

    reader = new FileReader();
    reader.onload = function(event) 
            { var img = new Image; 
              img.onload = UpdatePreviewCanvas1; 
              img.src = event.target.result;  }
    reader.readAsDataURL( file );
}





function UpdatePreviewCanvas()
{
    var img = this;
    var canvas = document.getElementById( 'previewcanvas' );

    if( typeof canvas === "undefined" 
        || typeof canvas.getContext === "undefined" )
        return;

    var context = canvas.getContext( '2d' );

    var world = new Object();
    world.width = canvas.offsetWidth;
    world.height = canvas.offsetHeight;

    canvas.width = world.width;
    canvas.height = world.height;

    if( typeof img === "undefined" )
        return;

    var WidthDif = img.width - world.width;
    var HeightDif = img.height - world.height;

    var Scale = 0.0;
    if( WidthDif > HeightDif )
    {
        Scale = world.width / img.width;
    }
    else
    {
        Scale = world.height / img.height;
    }
    if( Scale > 1 )
        Scale = 1;

    var UseWidth = Math.floor( img.width * Scale );
    var UseHeight = Math.floor( img.height * Scale );

    var x = Math.floor( ( world.width - UseWidth ) / 2 );
    var y = Math.floor( ( world.height - UseHeight ) / 2 );

    context.drawImage( img, x, y, UseWidth, UseHeight );  
}

function UpdatePreviewCanvas1()
{
    var img = this;
    var canvas = document.getElementById( 'previewcanvas1' );

    if( typeof canvas === "undefined" 
        || typeof canvas.getContext === "undefined" )
        return;

    var context = canvas.getContext( '2d' );

    var world = new Object();
    world.width = canvas.offsetWidth;
    world.height = canvas.offsetHeight;

    canvas.width = world.width;
    canvas.height = world.height;

    if( typeof img === "undefined" )
        return;

    var WidthDif = img.width - world.width;
    var HeightDif = img.height - world.height;

    var Scale = 0.0;
    if( WidthDif > HeightDif )
    {
        Scale = world.width / img.width;
    }
    else
    {
        Scale = world.height / img.height;
    }
    if( Scale > 1 )
        Scale = 1;

    var UseWidth = Math.floor( img.width * Scale );
    var UseHeight = Math.floor( img.height * Scale );

    var x = Math.floor( ( world.width - UseWidth ) / 2 );
    var y = Math.floor( ( world.height - UseHeight ) / 2 );

    context.drawImage( img, x, y, UseWidth, UseHeight );  
}
/********************************************  End show image preview before upload function ***************************/