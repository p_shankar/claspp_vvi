</div>
<div class="footerdiv">
<div class="wrapper_main_adv">
 
  <div class="footerinner">
    <div class="footer_logo"><img src="images/footerlogo.png" alt="Claspp"></div>
    <div class="copyright_div">
       <p>&copy; 2013. All Rights Reserved to <a href="http://www.claspp.com/" target="_blank">claspp.com </a> </p>
    </div>
    <div class="term_condition_div">
      <ul>
        <li><a href="<?php echo $site_url; ?>terms.php">Terms of Use</a></li>
        <li>|</li>
        <li class="no"><a href="<?php echo $site_url; ?>privacy.php">Privacy Policy</a></li>
        <li>|</li>
        <li class=""><a href="<?php echo $site_url; ?>contact.php">Contact Us</a></li>
        <li>|</li>
        <li class=""><a href="<?php echo $site_url; ?>faq.php">Faq</a></li>
      </ul>
    </div>
    </div>
  </div>
</div>

</div>
</body>
</html>