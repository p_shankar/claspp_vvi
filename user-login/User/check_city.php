<?php


/**
 *Ravi
 * @param <type> $address
 * @return <type>
 */
if(!function_exists('lat_lang_from_city')){
function lat_lang_from_city($address){

    $return = array();

    $address = urlencode($address);

    $request_url = "http://maps.googleapis.com/maps/api/geocode/xml?address=".$address."&sensor=true"; // the request URL you'll send to google to get back your XML feed
    $xml_txt = file_get_contents($request_url) or die("url not loading");// XML request

    $xml = simplexml_load_string($xml_txt, 'SimpleXMLElement', LIBXML_NOCDATA);
    $f1 = $xml->result->geometry->location->lat;


    $f2 =  $xml->result->geometry->location->lng;

    $return[lat] = floatval($f1);
    $return[lang] = floatval($f2);

    return $return;
}
}

/**
 *Ravi
 * @param <type> $address
 * @return <type>
 */
if(!function_exists('distance')){
    function distance($lat1, $lon1, $lat2, $lon2, $unit) {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }
}

/**
 *Ravi
 * @return <type>
 */
if(!function_exists('distance_from_ip')){
function distance_from_ip(){

$ip = $_SERVER['REMOTE_ADDR'];

$details = json_decode(file_get_contents("http://ipinfo.io/{$ip}"));

$loc = $details->loc;

$lat_langs = explode(',',$loc);

return  distance($lat_langs[0],$lat_langs[1], 39.768403,-86.158068, "K");
}
}


/**
 * 
 */
if(!empty($_POST['city']) && $_POST['action'] == 'ajax') {


    $address = $_POST['city'];

    //$address= 'indianapolis';

   $lat_langs = lat_lang_from_city($address);

//    distance($lat_langs[lat],$lat_langs[lang], 39.768403,-86.158068, "M") . " Miles<br>";
    
    $distance = distance($lat_langs[lat],$lat_langs[lang], 39.768403,-86.158068, "K") ;


    $distance1=round($distance, 2);

    if($distance1 < 31){

        echo "yes";

    } else{

        echo "no";

    }

}


?>