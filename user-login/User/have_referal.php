<?php
/*  Publisher's dashboard */
session_start();
include('../../config.php');
if ($_SESSION['log'] == 'true')
{
    header('location:../../admin/index.php');
}
else if($_SESSION['userlog'] == 'true')
{
    if(!empty($_SESSION['loggedin_id']))
    {
        $current_id = $_SESSION['loggedin_id'];
    }
}
else if ($_SESSION['advertiserlog'] == 'true')
{
    header('location:../Advertiser/index.php');
}
else 
{
    header('location:../../espy-login.php');
}
include('../../inc/functions.php');
include('includes/db_functions.php');
include ('header.php');
?>
<div id="middle">
  <div class="dashboard_area"> 
    <!--left menu bar starts from here-->
    <div class="dashboard_area_left">
      <div class="dash_listdiv">
        <div class="dashboardtabs">
          <nav class="top_buttons">
            <ul class="nav_div">
              <li class="big_button selected">
                <div class="big_count"> <span>Dashboard</span> </div>
                <div class="out_border">
                  <div class="button_wrapper">
                    <div class="in_border"> <a href="publisher-dashboard.php" title="DashBoard Home" class="the_button"> <span class="homeicon"></span> </a> </div>
                  </div>
                </div>
              </li>
              <li class="big_button selected">
                <div class="big_count"> <span>Advertisements</span> </div>
                <div class="out_border">
                  <div class="button_wrapper">
                    <div class="in_border"> <a href="add_listing.php" title="Advertisements" class="the_button"> <span class="campaign"></span> </a> </div>
                  </div>
                </div>
              </li>
              <li class="big_button">
                <div class="big_count">  <span>Free Coupons</span> </div>
                <div class="out_border">
                  <div class="button_wrapper">
                    <div class="in_border"> <a href="wallet.php" title="Wallet" class="the_button"> <span class="payment"></span> </a> </div>
                  </div>
                </div>
              </li>
              <li class="big_button">
                <div class="big_count"> <span>Settings</span> </div>
                <div class="out_border">
                  <div class="button_wrapper">
                    <div class="in_border"> <a href="settings.php" title="Settings" class="the_button"> <span class="settings"></span> </a> </div>
                  </div>
                </div>
              </li>
              <li class="big_button">
                <div class="big_count"> <span>Have Referral?</span> </div>
                <div class="out_border">
                  <div class="button_wrapper">
                    <div class="in_border"> <a href="have_referal.php" title="Have Referral?" class="the_button active"> <span class="ref"></span> </a> </div>
                  </div>
                </div>
              </li>
            </ul>
          </nav>
          <div class="fixeddiv">

            <div id="tab-8" class="dashboardtabscontent">
              <div class="dashboard_detail">
                <div class="" id="profile_div">
                  <div class="referal_box">
               <p>Do you have a referral who should advertise on claspp ? </p>
                  </div>
                  <div class="input_con input_error">
                    <form name="referal_form"  action="#"  id="referal_form"  method="post" enctype="multipart/form-data">
                      <input type="hidden" name="save_referal" value="1">
                      <div class="profile_input">
                        <div class="profile_left_con">
                          <label>Company Name*</label>
                        </div>
                        <div class="profile_right_con">
                          <input type="text" name="company_name" id="" class="required" value="" />
                        </div>
                        <div class="profile_left_con">
                          <label>Company Website*</label>
                        </div>
                        <div class="profile_right_con">
                          <input type="text" name="company_website" id="" class="required" value="" />
                        </div>
                      </div>
                      <div class="profile_input">
                        <div class="profile_left_con">
                          <label>Contact Email*</label>
                        </div>
                        <div class="profile_right_con">
                          <input type="text" name="company_email" id="" class="required email" value="" />
                        </div>
                        <div class="profile_left_con">
                          <label>Address*</label>
                        </div>
                        <div class="profile_right_con">
                          <textarea class="required" id=""  name="company_address"></textarea>
                        </div>
                      </div>
                      <div class="profile_input">
                        <div class="profile_left_con">
                          <label>Message*</label>
                        </div>
                        <div class="profile_right_con">
                          <textarea class="required" id=""  name="message"></textarea>
                        </div>
                      </div>
                      <div class="profile_input">
                        <div class="profile_right_con">
                          <input type="submit" value="submit" id="referal_submit" class="ssend" name="referal_submit" />
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--left menu bar ends here--> 
    </div>
  </div>
</div>



</div>
<?php
include('footer.php');
