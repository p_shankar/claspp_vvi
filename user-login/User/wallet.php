<?php
/*  Publisher's dashboard */
session_start();


include('../../config.php');
//include resizing class
include('../../inc/resizeClass.php');

if ($_SESSION['log'] == 'true')
{
header('location:../../admin/index.php');
}
else if($_SESSION['userlog'] == 'true')
{
if(!empty($_SESSION['loggedin_id']))
{
$current_id = $_SESSION['loggedin_id'];
}
}
else if ($_SESSION['advertiserlog'] == 'true')
{
header('location:../Advertiser/index.php');
}
else 
{
header('location:../../espy-login.php');
}
include('../../inc/functions.php');
include('includes/db_functions.php');
include ('header.php');
?>

<script type="text/javascript" src="js/modernizr.custom.53451.js"></script>
<link rel="stylesheet" href="css/flyout.css" />
<script type="text/javascript" src="js/flyout.js"></script>
<script type="text/javascript" src="js/sliding.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="css/div_sliding.css">
<input type="hidden" id="current_id" value="<?php echo $_SESSION['loggedin_id']; ?>" >

<?php
$testing = uniqid('cl');

	function mt_rand_str ($l, $c ) {
	for ($s = '', $cl = strlen($c)-1, $i = 0; $i < $l; $s .= $c[mt_rand(0, $cl)], ++$i);
	return $s;
	}

	$testing = mt_rand_str('14',$testing);

echo  $testing;?>

<div id="middle">
  <div class="dashboard_area"> 
    <!--left menu bar starts from here-->
    <div class="dashboard_area_left">
      <div class="dash_listdiv">
        <div class="dashboardtabs">
				<nav class="top_buttons">
				<ul class="nav_div">
				<li class="big_button selected">
				<div class="big_count"> <span>Dashboard</span> </div>
				<div class="out_border">
				<div class="button_wrapper">
				<div class="in_border"> <a href="publisher-dashboard.php" title="DashBoard Home" class="the_button"> <span class="homeicon"></span> </a> </div>
				</div>
				</div>
				</li>
				<li class="big_button selected">
				<div class="big_count"> <span>Advertisements</span> </div>
				<div class="out_border">
				<div class="button_wrapper">
				<div class="in_border"> <a href="add_listing.php" title="Advertisements" class="the_button"> <span class="campaign"></span> </a> </div>
				</div>
				</div>
				</li>

				<li class="big_button">
				<div class="big_count"> <span>Free Coupons</span> </div>
				<div class="out_border">
				<div class="button_wrapper">
				<div class="in_border"> <a href="wallet.php" title="Wallet" class="the_button active"> <span class="payment"></span> </a> </div>
				</div>
				</div>
				</li>
				<li class="big_button">
				<div class="big_count"> <span>Settings</span> </div>
				<div class="out_border">
				<div class="button_wrapper">
				<div class="in_border"> <a href="settings.php" title="Settings" class="the_button"> <span class="settings"></span> </a> </div>
				</div>
				</div>
				</li>
				<li class="big_button">
				<div class="big_count"> <span>Have Referral?</span> </div>
				<div class="out_border">
				<div class="button_wrapper">
				<div class="in_border"> <a href="have_referal.php" title="Have Referral?" class="the_button"> <span class="ref"></span> </a> </div>
				</div>
				</div>
				</li>
				</ul>
				</nav>
          <div class="fixeddiv">
								<div id="tab-5" class="dashboardtabscontent">
								<div class="dashboard_detail">
								<!--Transaction History-->
								<div class="profile_box wallet_toggle" id="transaction_history"> <img class="" src="../../images/up.png">
								<p>Your Free Coupons</p>
								</div>
								<div  class="usertable transaction_history wallet_hide">
								<div class="headingdiv">
								<ul>
								<li>Tile</li>
								<li>Campaign </li>
								<li>Company</li>
								<li>Print</li>
								</ul>
								</div>
								<div class="detaildiv">
									<?php
									$results = get_coupon_list();
									if(@mysql_num_rows($results)>0){
									while($row = mysql_fetch_assoc($results))
									{
									$orig_campaign_image = '../Advertiser/images/coupon/'.$row['image'];
									$resized_campaign_image = saveNewResizedImage($orig_campaign_image,250,200,'exact',true);
									$cp_url = $site_url."/print_free_coupon.php?ad=".base64_encode($row['campaign_id'])."";
									$print_coupon = '<a href="'.$cp_url.'" target="_blank" style=\'color: #FFFFFF; font-family: "Antic Slab";font-size: 14px;font-weight: bold;  margin-left:20px; line-height:28px;background: none repeat scroll 0 0 #91A136;  border:1px  solid #000; padding:0px 5px;\'>Print </a>';
									?>

									<ul>
									<li><?php echo $row['title']; ?></li>
									<li><img src="<?php echo $orig_campaign_image; ?>"  height="50" width="50" /></li>
									<li><?php echo $row['company_name']; ?></li>
									<li> <?php echo $print_coupon; ?></li>
									</ul>

									<?php           
									}
									} else {

									echo '<ul><li class="empty_li">Your transaction history is empty.</li></ul>';
									}
									?>
                  </div>
                </div>
        
          
              </div>
            </div>
            
            </div>
          </div>
        </div>
      </div>
      <!--left menu bar ends here--> 
    </div>
  </div>
</div>

<?php
include('footer.php');
