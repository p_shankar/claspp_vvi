<?php
		session_start();
/**
*  this file contains functions to update or retrieve database
*
*
*/

if(!function_exists('get_duration')){
    function get_duration(){
        $duration_query = 'select * from espy_duration' ;
        $duration_results = mysql_query($duration_query);
         return $duration_results;
    }
}

if(!function_exists('get_gender')){
    function get_gender(){
        $gender_query = 'select * from espy_gender' ;
        $gender_results = mysql_query($gender_query);
         return $gender_results;
    }
}

if(!function_exists('get_age_groups')){
    function get_age_groups(){
        $age_query = 'select * from espy_age_groups' ;
        $age_results = mysql_query($age_query);
         return $age_results;
    }
}


if(!function_exists('get_radius')){
    function get_radius(){
        $radius_query = 'select * from espy_radius' ;
        $radius_results = mysql_query($radius_query);
         return $radius_results;
    }
}

if(!function_exists('get_zip')){
    function get_zip(){
        $zipcode_query = 'select * from espy_zip' ;
        $zipcode_results = mysql_query($zipcode_query);
         return $zipcode_results;
    }
}

if(!function_exists('get_household_income')){
    function get_household_income(){
        $income_query = 'select * from espy_household_income' ;
        $income_results = mysql_query($income_query);
         return $income_results;
    }
}

if(!function_exists('get_countries')){
    function get_countries(){
        $country_query = 'select * from espy_countries' ;
        $country_results = mysql_query($country_query);
        return $country_results;
    }
}

if(!function_exists('get_cities')){
    function get_cities(){
        $city_query = 'select * from espy_cities' ;
        $city_results = mysql_query($city_query );
        return $city_results;
    }
}
if(!function_exists('get_campaign_categories')){
    function get_campaign_categories(){
        $category_query = 'select * from espy_campaign_categories  ';
        $category_results = mysql_query($category_query);
         return $category_results;
    }
}


if(!function_exists('get_campaigns_list')){
    function get_camp_list($u_id=''){


        $query .= 'select
                        cp.id,
                        cp.title,
                        cp.url ,
						cp.sharetype,
                        cp.description ,
                        cp.campaign_type ,
                        cp.status ,
                        cp.target_category,
                        cp.created,
                        cp.social_media,
                        cp.campaign_image,
                        cpb.budget,
                        cpb.impressions,
						cpb.projected_posts,
                        cpb.posted,
                        cpb.remaining_posts,
                        cpb.per_post_payment,
                        fbclick.count as fbcount,
                        twclick.count as twcount
                from
                    espy_campaign as cp
                left join
                    espy_campaign_budget as cpb
                on
                    cp.id = cpb.campaign_id
                left join
                    espy_click_counts as fbclick
                on
                    cp.id = fbclick.ad_id and fbclick.type="facebook"
                left join
                    espy_click_counts as twclick
                on
                    cp.id = twclick.ad_id and twclick.type="twitter"';
		

   

		 $q_per_post = 'select *   from user_sur_category where user_id = '.$_SESSION['loggedin_id'];
              $q_result = mysql_query($q_per_post);
		
             if(mysql_num_rows($q_result ) >0) {

				 $test = "SELECT comp_id FROM espy_comp_category WHERE sub_cat_id IN (SELECT sub_cat_id from user_sur_category where user_id=".$_SESSION['loggedin_id'].")";

				     $q_test= mysql_query($test);
	
             if(mysql_num_rows($q_test) >0) {

		   $query .=" WHERE cp.id IN ( SELECT comp_id FROM espy_comp_category WHERE sub_cat_id IN (SELECT sub_cat_id from user_sur_category where user_id=".$_SESSION['loggedin_id']."))  and cp.status =1   and DATEDIFF(CURDATE(), cp.end_date) < 1 and DATEDIFF(cp.start_date, CURDATE()) < 1 " ;
			 } else {
                      $query .=" where     cp.status ='1'   and DATEDIFF(CURDATE(), cp.end_date) < 1 and DATEDIFF(cp.start_date, CURDATE()) < 1 ";
			 }

			 }	  else {

            $query .=" where cp.status ='1'   and DATEDIFF(CURDATE(), cp.end_date) < 1 and DATEDIFF(cp.start_date, CURDATE()) < 1 ";
		         	 }


            
         $results = mysql_query($query);

       return $results;
    }
}

 function get_coupon_list($u_id=''){


        $query .= 'select                       
                        cp.campaign_type ,
                        cp.status ,
                        cp.target_category,
                        cp.created,
                        efc.*
                from
                    espy_campaign as cp
                inner join
                    espy_free_coupans as efc
                on
                    cp.id = efc.campaign_id
				inner join 
espy_postedads as ep
 on
                    cp.id = ep.pads_adid
					
					where cp.status ="1"   AND  ep.pads_postedby = "'.$_SESSION['loggedin_id'].'" and DATEDIFF(CURDATE(), cp.end_date) < 1 and DATEDIFF(cp.start_date, CURDATE()) < 1';
		

         $results = mysql_query($query);

       return $results;
    }


    function get_slider_list($u_id=''){

        $query .= 'select
                        cp.id,
                        cp.title,
                        cp.campaign_type,
                        cp.url ,
                        cp.description ,
                        cp.status ,
                        cp.target_category,
                        cp.created,
						cp.sharetype,
                        cp.social_media,
                        cp.campaign_image,
                        cpb.budget,
                        cpb.impressions,
                        cpb.posted,
						cpb.projected_posts,
                        cpb.remaining_posts,
                        cpb.per_post_payment,
                        fbclick.count as fbcount,
                        twclick.count as twcount
                from
                    espy_campaign as cp
                left join
                    espy_campaign_budget as cpb
                on
                    cp.id = cpb.campaign_id
                left join
                    espy_click_counts as fbclick
                on
                    cp.id = fbclick.ad_id and fbclick.type="facebook"
                left join
                    espy_click_counts as twclick
                on
                    cp.id = twclick.ad_id and twclick.type="twitter"
					where       cp.status ="1"  and DATEDIFF(CURDATE(), cp.end_date) < 1  and DATEDIFF(cp.start_date, CURDATE()) < 1';

   


         $results = mysql_query($query);

       return $results;
    }




   /********************   
   
   get survey categories 
By harpartap singh
on 20 march 2013

**************/

	function get_categories(){
$category_query = 'select * from comp_category' ;
$category_results = mysql_query($category_query);
return $category_results;
}

function get_sub_categories($s){
$category_query = 'select * from comp_sub_cat where cat_id='.$s.'';
$category_results = mysql_query($category_query);
return $category_results;
}



   /********************   
   
   get survey categories 
By harpartap singh
on 20 march 2013

**************/

/*********************** email for referal********************************/

 

 if(!function_exists('referal_success')){
    function referal_success($email, $name, $website, $address,$message){

$admin_email=mysql_query("select * from admin");

 $admin_e = mysql_fetch_array($admin_email);

       $to  = $admin_e['paypal_email']; // note the comma
		
	

        $subject = 'Referal email from the claspp.';
      
           

   $message="<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
    <head>
        <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
        <title>Referal Email From The Claspp</title>
    </head>

    <body style='width:800px; background-repeat:no-repeat; background-position:right'>
         <!----> <div style='background-color:#fff;'>
          <!--[if gte mso 9]>
          <v:background xmlns:v='urn:schemas-microsoft-com:vml' fill='t' style='display:inline-block; top:100;left:200;' >
            <v:fill type='tile' src='http://www.claspp.com/beta/images/class_main_bg.png' color='#fff'/>
          </v:background>
          <![endif]-->
          <table height='100%' width='100%' cellpadding='0' cellspacing='0' border='0'>
            <tr>
              <td valign='top' align='left' background='http://www.claspp.com/beta/images/class_main_bg.png' style='background-repeat: no-repeat;background-position-x:390px;background-position-y:100px;'>
        <!---->
        <table width='100%' cellspacing='0' cellpadding='0' style='padding:20px'>
          
            <tr>
                <td>
                    <a href='#'><img src='http://www.claspp.com/beta/images/class_logo.png' /></a>
                </td>
            </tr>
            <tr>
                <td style='color: rgb(146, 162, 63); font-family: helvetica;display:block; font-size: 41.77px; font-weight: bold;padding: 30px 0 0; float:left;' width='59%'>
                  ".$header_email."
                </td>
            </tr>
            <tr>
                <td style='color: rgb(146, 162, 63); font-family: helvetica;display:block; font-size: 24px;font-weight: bold; font-weight: bold;padding: 30px 0 0; float:left;' width='59%'>
                  Company Name:&nbsp;  ".$name.",
                </td>
            </tr>

             <tr>
                <td style='color: rgb(0, 0, 0); font-family: helvetica;display:block; font-size: 24px;font-weight: bold; padding: 30px 0px 0px; float: left; ' width='59%'>
                  Website Url:&nbsp; ".$website.".
                </td>
            </tr>
            <tr>
                <td style='color: rgb(0, 0, 0); font-family: helvetica;display:block; font-size: 24px;font-weight: bold; padding: 10px 0px 0px; float: left; ' width='59%'>
                 Email Id :&nbsp; ".$email."
                </td>
            </tr>
            <tr>
                <td style='color: rgb(0, 0, 0); font-family: helvetica;display:block; font-size: 24px;font-weight: bold; padding: 10px 0px 0px; float: left; ' width='59%'>
                    Address :&nbsp;".$address."
                </td>
            </tr>

            <tr>
                <td style='color: rgb(0, 0, 0); font-family: helvetica;display:block; font-size: 24px;font-weight: bold; padding: 30px 0px 0px; float: left; ' width='59%'>
                  ".$message."
                </td>
            </tr>

             <tr>
                <td style='color: rgb(0, 0, 0); font-family: helvetica;display:block; font-size: 16px;font-weight: bold; padding: 30px 0px 0px; float: left; ' width='59%'>
                 Thanks for Using Claspp.
                </td>
            </tr>
            <tr>
                <td style='color: rgb(0, 0, 0); font-family: helvetica;display:block; font-size: 16px;font-weight: bold; padding: 10px 0px 0px; float: left; ' width='59%'>
                   Copyright &copy; 2014 claspp.com
                </td>
            </tr>
              <tr>
                <td style='color: rgb(0, 0, 0); font-family: helvetica;display:block; font-size: 16px;font-weight: bold; padding: 10px 0px 0px; float: left; ' width='59%'>
                 ".$disclaimer."
                </td>
            </tr>
        </table>
 <!----></td>
    </tr>
  </table>
</div><!---->
    </body>
</html>

";
   
   

        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .='From: Claspp'. "\r\n";
//echo $message;
//die();
        mail($to, $subject, $message, $headers);

    }

}



/***************  function to check user currentr fb logged in status ************/


function check_status() {

  $q_ins = "select * from espy_socialmedia where sm_userid = $_SESSION[loggedin_id] and sm_smtype='facebook'  and status='active' ";
    $result = mysql_query($q_ins);

   $row = mysql_num_rows($result);
	   if($row>0) {

return  true;

   } else {

return  false;

   }



}


function add_facebbok_login($uname,$user) {

 	$smquery = "insert into espy_socialmedia(sm_userid,sm_smtype,sm_username,sm_smadded,status,unique_id) values($_SESSION[loggedin_id],'facebook','$uuname','Y','active',$user)";
		$query = mysql_query($smquery);


return $query ;


}







function update_facebbok_login() {

	$update_query="update espy_socialmedia set sm_smadded = 'N', status='inactive' where sm_userid =".$_SESSION['loggedin_id']." and sm_smtype = 'Facebook'  and status='active' ";
		$query = mysql_query($update_query);


return $query ;


}










function get_facebook_details() {

$f_query="select *  from espy_socialmedia where sm_userid =".$_SESSION['loggedin_id']." and sm_smtype = 'Facebook' and status='active'";
$fresult = mysql_query($f_query);
if(@mysql_num_rows($fresult)>0){
    $id = @mysql_fetch_array($fresult);

return $id;
} else {

$id= "";
return $id;

}


}

		function set_string($s_row) {


		$title = str_replace('"', '&quot;',$s_row);
		$title = str_replace  ("'", "\'\ ",$title);
		$title = preg_replace( "/\s+/", " ", $title);

		return $title;
		}

		function check_add_posted($add_id,$postedby) {

		$fbb = @mysql_query('select * from espy_postedads where pads_adid = "'.$add_id.'" and pads_postedby = "'.$postedby.'" and pads_smtype = "facebook" ');

		$first = mysql_num_rows($fbb);

		return   $first;

		}