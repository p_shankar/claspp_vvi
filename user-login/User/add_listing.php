<?php
/*  Publisher's dashboard */
if($_REQUEST['PHPSESSID']){
    session_id($_REQUEST['PHPSESSID']);
}
session_start();
include('../../config.php');
include('../../inc/resizeClass.php');
if ($_SESSION['log'] == 'true')
{
    header('location:../../admin/index.php');
}
else if($_SESSION['userlog'] == 'true')
{
    if(!empty($_SESSION['loggedin_id']))
    {
        $current_id = $_SESSION['loggedin_id'];
    }
}
else if ($_SESSION['advertiserlog'] == 'true')
{
    header('location:../Advertiser/index.php');
}
else 
{
    header('location:../../index.php');
}
include('../../inc/functions.php');
include('includes/db_functions.php');
include ('facebook/fbmain.php');
if($user)
$friends = $facebook->api('/me/friends');
$total_friends = sizeof($friends['data']);
include ('header.php');
?>
<script type="text/javascript" src="http://connect.facebook.net/en_US/all.js"></script>
<script type="text/javascript">
    FB.init({
        appId  : '<?=$fbconfig['appid']?>',
        status : true, // check login status
        cookie : true, // enable cookies to allow the server to access the session
        xfbml  : true  // parse XFBML
    });
    FB.getLoginStatus(function(response) {

        var test = response.status;
        //alert(test);

        if(response.status != 'unknown' && 0 == '<?php echo $user ?>' &&  window.location.search.indexOf('code=')>=1){
            location.reload();
            return false;
        }
    });
</script>
<!--scripot for tooltip-->

<div id="fab" class="pop_message" style="display:none;">
    Facebook is connected successfully.
</div>
<div id="fab1" class="pop_message" style="display:none;">
    You Are  Connected  with facebook .
</div>
<div id="fab3" class="pop_message" style="display:none;">
    You should have atleast 200 friends to connect via facebook.
</div>
<?php

/* Start facebook */
if(isset($_GET['state']) && empty($_POST['disconnect']))
{

    if(!check_status())
    {
        if($user){


            add_facebbok_login($uuname,$user);
            echo "<script type='text/javascript'>
$('document').ready(function(){

$('#tab-6').css('display','block');
$.blockUI({ message:$('#fab') ,
css: {
left:500
},
timeout: 3000
});});
</script>";

        }
    }
    else {

        echo "<script type='text/javascript'>
$('document').ready(function(){

$('#tab-6').css('display','block');
$.blockUI({ message: $('#fab1'),
css: {
left:500
},
timeout: 3000});});
</script>";



    }
}
$id = get_facebook_details();
if($id) {
    $unique_fbid = $id['unique_id'];
}
?>
<script type="text/javascript" src="<?php echo $site_url;?>/tooltip/wz_tooltip.js"></script>
<script type="text/javascript" src="js/ajax.js"></script>
<script  type="text/javascript" src="js/publisher_dashboard.js"></script>
<script type="text/javascript" src="js/dough.js"></script>

<div id="dis" class="pop_message2" style="display:none;">
    Disconnection from Facebook is successfully.
</div>

<?php
$categories = get_categories();
$category_results = get_campaign_categories();
$gender_results = get_gender();
$age_results = get_age_groups();


?>
<script type="text/javascript" src="js/modernizr.custom.53451.js"></script>
<link rel="stylesheet" href="css/flyout.css" />
<script type="text/javascript" src="js/flyout.js"></script>
<script type="text/javascript" src="js/sliding.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="css/div_sliding.css">
<input type="hidden" id="fb_id" value="<?php echo $unique_fbid; ?>">
<input type="hidden" id="current_id" value="<?php echo $_SESSION['loggedin_id']; ?>" >


<div id="middle">
    <div class="dashboard_area">
        <!--left menu bar starts from here-->
        <div class="dashboard_area_left">
            <div class="dash_listdiv">
                <div class="dashboardtabs">
                    <nav class="top_buttons">
                        <ul class="nav_div">
                            <li class="big_button selected">
                                <div class="big_count"> <span>Dashboard</span> </div>
                                <div class="out_border">
                                    <div class="button_wrapper">
                                        <div class="in_border"> <a href="publisher-dashboard.php" title="DashBoard Home" class="the_button"> <span class="homeicon"></span> </a> </div>
                                    </div>
                                </div>
                            </li>
                            <li class="big_button selected">
                                <div class="big_count"> <span>Advertisements</span> </div>
                                <div class="out_border">
                                    <div class="button_wrapper">
                                        <div class="in_border"> <a href="add_listing.php" title="Advertisements" class="the_button active"> <span class="campaign"></span> </a> </div>
                                    </div>
                                </div>
                            </li>

                            <li class="big_button">
                                <div class="big_count">  <span>Free Coupons</span>  </div>
                                <div class="out_border">
                                    <div class="button_wrapper">
                                        <div class="in_border"> <a href="wallet.php" title="Wallet" class="the_button"> <span class="payment"></span> </a> </div>
                                    </div>
                                </div>
                            </li>
                            <li class="big_button">
                                <div class="big_count"> <span>Settings</span> </div>
                                <div class="out_border">
                                    <div class="button_wrapper">
                                        <div class="in_border"> <a href="settings.php" title="Settings" class="the_button"> <span class="settings"></span> </a> </div>
                                    </div>
                                </div>
                            </li>
                            <li class="big_button">
                                <div class="big_count"> <span>Have Referral?</span> </div>
                                <div class="out_border">
                                    <div class="button_wrapper">
                                        <div class="in_border"> <a href="have_referal.php" title="Have Referral?" class="the_button"> <span class="ref"></span> </a> </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </nav>
                    <div class="fixeddiv">

                        <div id="tab-2" class="dashboardtabscontent">
                            <div class="dashboard_detail">
                                <div class="dashboard_ads_list">
                                    <div class="input_con">
                                        <div class="profile_input">
                                            <div class="profile_left_con">
                                                <p class="fnt">Advertisements</p>
                                            </div>
                                            <div class="dropdown">
                                                <select name="city_sort" class="city_sort">
                                                    <option selected ="selected" disabled value="">Select City</option>
                                                    <option value="-1">All cities</option>
                                                    <option value="25">Indianapolis </option>

                                                </select>
                                            </div>
                                            <div class="dropdown">
                                                <select name="cat_sort" class="cat_sort" value="">
                                                    <option selected="selected" value="0" disabled>Select Category</option>
                                                    <?php
                                                    $category_results = get_campaign_categories();
                                                    if(@mysql_num_rows($category_results)>0){
                                                        while($category_result = mysql_fetch_assoc($category_results))
                                                        {
                                                            echo '<option value="'.$category_result['id'].'">'.$category_result['category_name'].'</option>';
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mainadlistdiv" id="mainadlistdiv">
                                        <div class="page_navigation"></div>
                                        <?php

                                        echo '<ul class="content " id="p'.$page.'">';

                                        $results = get_camp_list();
                                        if(@mysql_num_rows($results)>0){
                                            while($row = mysql_fetch_assoc($results))
                                            {
                                                $orig_campaign_image = '../Advertiser/images/campaign/'.$row['campaign_image'];
                                                $resized_campaign_image = saveNewResizedImage($orig_campaign_image,250,200,'exact',true);
                                                $p_url = $site_url."/coupon.php?ad=".base64_encode($row['id'])."";
                                                $fbb =check_add_posted($row['id'],$current_id);
                                                $cp_url = $site_url."/coupon.php?ad=".base64_encode($row['id'])."";
                                                $print_coupon = '<a href="'.$cp_url.'" target="_blank" style=\'color: #FFFFFF; font-family: "Antic Slab";font-size: 14px;font-weight: bold;  margin-left:20px; line-height:28px;background: none repeat scroll 0 0 #91A136;  border:1px  solid #000; padding:0px 5px;\'>Print </a>';
                                                ?>
                                        <li>
                                            <div class="imagetitle">
                                                <h4 class="img_len">
                                                    <?php echo $row['title']; ?>
                                                </h4>
                                                <!--------------------akash end by aug 23------------------>

                                            </div>
                                            <div class="imagediv"><img src="<?php echo $resized_campaign_image; ?>" /></div>
                                            <div class="discriptiondiv">
                                                <div class="ribbondiv"></div>
                                                <div class="socialdiv">
                                                    <p>
                                                    <?= $print_coupon; ?></p>
                                                    <br/>
                                                    <ul>
                                                        <?php
                                                        $frows = get_facebook_details();
                                                        if($frows<1 || $frows['status']=='inactive')
                                                        { ?>



                                                        <li>  <a id="<?php echo base64_encode($row['id']); ?>"   onclick="connect_alert()"  ><img src="images/fcbk.png" /></a> </li>

                                                                    <?php
                                                                }	else {

                                                                    $title = 	set_string($row[title]);

                                                                    $test = set_string($row[description]);

                                                                    ?>
                                                        <li> <a id="<?php echo base64_encode($row['id']); ?>" onclick="postAdlisting('<?php echo $title; ?>','<?php echo $p_url; ?>','<?php echo $test; ?>','<?php echo $site_url.'/user-login/Advertiser/images/campaign/'.$row[campaign_image]; ?>','<?php echo $row['id']; ?>','facebook','<?php echo $fbb == 1 ? 'posted' : 'notposted';?>' , 'false');" ><img src="images/fcbk.png" /></a> </li>



                                                                    <?php
                                                                }

                                                                ?>


                                                    </ul>
                                                    <br/>
                                                </div>
                                            </div>
                                        </li>
                                        <?php
                                        $imgcount++;

                                    }
                                }

                                echo '</ul>';

                                echo '<div class="page_navigation"></div>  ';


                                ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!--left menu bar ends here-->
    </div>
</div>


<div id="reload_script">

    <script src="../../js/jquery.pajinate.js" type="text/javascript"></script>
    <script type="text/javascript">


        $(document).ready(function(){


            $('#mainadlistdiv').pajinate({
                items_per_page : 9,
                nav_label_prev : '',
                nav_label_next : '',
                num_page_links_to_display: 10,
                nav_label_first : '',
                nav_label_last : ''
            });
        });




        $(document).ready(function(){
            $('#non_profit_bottom').pajinate({
                items_per_page : 9,
                nav_label_prev : '',
                nav_label_next : '' ,
                num_page_links_to_display: 10,
                nav_label_first : '',
                nav_label_last : ''
            });
        });

    </script>
</div>
<?php
include('footer.php');
