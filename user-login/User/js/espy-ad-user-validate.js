
$(document).ready(function() {

    /**************************************
    * setting tabs
    ***************************************/

    $('.setting_tab').click(function(){
        $('.current').removeClass('current');
        $('.set_content_show').removeClass('set_content_show');
        $(this).addClass('current');
        var show_tab_content = $(this).attr('rel');

        $(show_tab_content).addClass('set_content_show');
    });

    $('.social_tab').click(function(){
      
        $('#profile_div').removeClass('set_content_show');
        $('#s_media_div').addClass('set_content_show');
        
    });

    $.validator.addMethod("valueNotEquals", function(value, element, arg){
  return arg != value;
 }, "Value must not equal arg.");

/********** akash  8 aug ***************/
    /***********update profile************/
    $("#publisher_form").validate({

			rules: {
			first_name: {
			required: true,
			alphabets:  true

			},
			last_name: {
			required: true,
			alphabets:  true

			},
			u_cnfmpass: {
			equalTo: '#u_newpass'
			},
            paypal_email:"required",
			email:"email",   
			City: "required",
			State:{ valueNotEquals: "default" }		
			},
			messages: {

			first_name: {
			required  : "Please enter your firstname",
			alphabets: "Please enter alpabets only "
			},
			last_name: {
			required  : "Please enter your lastname",
			alphabets: "Please enter alphabets only "
			},
				  
			paypal_email:"Please Enter Paypal email",
			email:"Enter a valid email ",
           	City: "Please enter City name ",
		  	 State:{ valueNotEquals: "Please select a State" }
			}

    });
    /***********update geographic information************/

    $("#cat_form").validate({


		 rules: {
            'c_target_category': {
                required: true
            },
            'c_target_city': {
                required: true
            }
        }




	});





    
    /***********update email preference************/
    $("#u_notification_form").validate({

        submitHandler: function(form) {

            var form = $('#u_notification_form');
            $.ajax( {
                type: "POST",
                url: form.attr( 'action' ),
                data: form.serialize(),
                success: function( response ) {
                    $.blockUI({
                        message: '<h1>Updated email preference.</h1>',
                        timeout: 1000
                    });

                }
            } );
            return false;
        }
    });


    /************ change password*********
    $("#change_pass_form").validate({
        rules: {
            'u_oldpass': {
                required: true
            },
            'u_newpass': {
                required: true
            },
            'u_cnfmpass': {
                required: true,
                equalTo: '#u_newpass'
            }
        },
        submitHandler: function(form) {

            var form = $('#change_pass_form');
            $.ajax( {
                type: "POST",
                url: form.attr( 'action' ),
                data: form.serialize(),
                success: function(response ) {
                    if(response == -1){
                        $('#u_oldpass').val('');
                        $('#u_oldpass').attr('style','color:red');
                        $('#u_oldpass').attr('placeholder','Old pass do not match');

                        $('#u_oldpass').focus(function(){
                            $('#u_oldpass').attr('style','color:#CCCCCC');
                            $('#u_oldpass').attr('placeholder','');
                        });
                    } else if(response == -2){
                        $.blockUI({
                            message: '<h1>Password changed successfully.</h1>',
                            timeout: 1000
                        });
                    }
                }
            });
        }
    });*/

    /*********** **********

    $('#upload_url').click(function(){
        $('#upload_pic').trigger('click');
    });

 /*   $('#upload_pic').change(function(){
        $.blockUI({
            message: '<h1>loading image...</h1>'
        });

        $('#publishers_pic').ajaxSubmit({
            success:  function(res) {
                if(res == -1){
                   
                    $('#prof_pic_div').load('publisher-dashboard.php #prof_pic_div');
                    $('#prof_pic_div1').load('publisher-dashboard.php #prof_pic_div1 img');
                    $('#prof_pic_div2').load('publisher-dashboard.php #prof_pic_div2 img');
                    $('#prof_pic_div2').load('publisher-dashboard.php #prof_pic_div2 img',function(){
                        $.unblockUI();
                    });
                } else if(res == -2){
                    $.blockUI({
                        message: '<h1>Invalid file type.</h1>',
                        timeout: 3000
                    });

                } else {
                    $.blockUI({
                        message: '<h1>Error in image uploading.</h1>',
                        timeout: 3000
                    });
                }
            }
        });
    });  */


	    $('#upload_url').click(function(){


        $('#upload_pic').trigger('click');
    });

    $('#upload_pic').change(function(){

				$.blockUI({
				message: "<div id='thank' class='pop_message' >Please wait while image is uploading.....</div>",
				timeout: 3000
				});
       
        $('#publishers_pic').ajaxSubmit({
            success:  function(res) {
                       
 $.unblockUI();
				if(res=="no") {
         
				} else {

                      $("#profile_right_img img ").attr('src','images/temp/'+res+'');
					   $("#profile_right_img img ").attr('height','60');
					    $("#profile_right_img img ").attr('width','80');
						$("#up_image").val('1');
						$("#image_name").val(res);
						
				}

            }
        });
    });


    /*********** have referal ************/
     $("#referal_form").validate({
	rules: {
			company_name: {
			required: true
			},
			company_website: {
			required: true
			},
			company_email: {
			required: true
			},
			company_address: {
			required: true
			},
			message: {
			required: true
			},
          	},
			messages: {

			company_name: {
			required  : "Please enter Company name"
			},
			company_website: {
			required  : "Please enter Company Website URL"
			},
			company_email: {
			required:"Please enter email"
			},
			company_address:{
			required:"Please enter Company Address"
			},
			 message:{
            		required:"Please enter Message"
		  	}, 
			}

    });

    /*********** show_hide_pass ************/

    $(".show_hide_pass").click(function(){
        $('.password_div').toggle(function(){
            if( $('.password_div').css('display') == 'block'){
                $('img.show_hide_pass').attr('src','../../images/up.png');
                $('#u_newpass').attr('disabled',false),
                $('#u_cnfmpass').attr('disabled',false)
            } else{
                $('img.show_hide_pass').attr('src','../../images/down.png');
                $('#u_newpass').attr('disabled',true),
                $('#u_cnfmpass').attr('disabled',true)
            }
        });
    });

    /*********** wallet toggle ************/

    $(".wallet_toggle").live('click',
        function(){
            var id = $(this).attr('id');
            if( $('.'+id).css('display') == 'block'){
                $('#'+id+' > img').attr('src','../../images/down.png');
                $('.'+id).hide('slow');
            } else {
                $('#'+id+' > img').attr('src','../../images/up.png');
                $('.wallet_hide').hide('slow');
                $('.'+id).show('slow');
            }
                     
        });
    /******multiple select*******/
    $(".ul_select").live('click', function(){
        var id = $(this).attr('id')
        $('.li_'+id).show('fast');
    });

    $(".ul_select").live('mouseleave' , function(){
        var id = $(this).attr('id')
        $.each($('.li_'+id+'>input:checkbox'),function(i,val){
            if($(this).is (':checked') == false){
                $(this).parent().hide('fast');
            }
        });
    });
       
   
    /*add amethod to validate.js*/
    $.validator.addMethod("alphabets", function(value,element)
    {
        return this.optional(element) || /^[a-zA-Z]+$/.test(value);
    }, "Alphabets only");


    /*display confirmation pop up*/
 $('.social_connect_click').click(function(){
        var href = $(this).attr('href');
        $.blockUI({
            message: '<div class="ui_div">\n\
                                    <p class="close_btn"><img src="images/close.png" /></p>\n\
                                    <p class="social_p">Please click continue below to link your social media account! </p>\n\
                                    <input type="button" name="continue" value="continue" class="social_decline" id="social_continue" /></div>',
            
            onBlock: function(){
                $('#social_continue').click(function(){
                    window.location = href ;
                });

                
                $('.close_btn > img').click(function(){
                    $.unblockUI();
                });

            }
        });

        return false;
    });



	$('.the_button').click(function(){



	$('.the_button').removeClass('active');
	$(this).addClass('active');


	});



 

     
});

    /* function for donation validation    click*/

           function withdraw (){

		
           
            if($('#pub_id').val() == 0 ){
           
                return false;

            } else if($('#withdraw_amt').val() == '' ){
                $('#withdraw_amt').css('border-color','red');
				return false;
            
            } else if($("#organization > li >input:checkbox:checked").length == 0){
                $('ul.ul_select').css('border-color','red');
                $('#withdraw_amt').css('border-color','#CCCCCC');
                return false;
            
            }else {
                $('ul.ul_select').css('border-color','#CCCCCC');
                $('#withdraw_amt').css('border-color','#CCCCCC');
 
            }

		if (confirm(' Do you want to donate to charity? ')) {
        
		}
		   else {
               $("#organization > li >input:checkbox:checked").removeAttr('checked');
			  
				
				
        $.each($('#organization li >input:checkbox'),function(i,val){
            if($(this).is (':checked') == false){
                $(this).parent().hide('fast');
            }
        });
		
	       $("#withdraw_amt").val('');
		

         return false;

		   }

		   }


/***************   validation function for survey form *******************/

function survey_validation() {



	if($("#c_target_category > li >input:checkbox:checked").length == 0){
				$('#c_target_category ').css('border-color','red');

				return false;

				}
            

			       		if($("#c_target_city > li >input:checkbox:checked").length == 0){
				$('#c_target_city ').css('border-color','red');

				return false;

				}



}









/*********************  confirm  Before Disconnect **********************/

function fb_confirm() {

	if (confirm('Are You Sure To  Disconnect  From  Facebook ? ')) {

		}
		   else {

         return false;

		   }




}


/******************************************** show image preview before upload function ***************************/
  function ShowImagePreview( files )
{
	 
    if( !( window.File && window.FileReader && window.FileList && window.Blob ) )
    {
     
      return false;
    }

    if( typeof FileReader === "undefined" )
    {
        alert( "Filereader undefined!" );
        return false;
    }

    var file = files[0];

    if( !( /image/i ).test( file.type ) )
    {
        alert( "File is not an image." );
        return false;
    }

    reader = new FileReader();
    reader.onload = function(event) 
            { var img = new Image; 
              img.onload = UpdatePreviewCanvas; 
              img.src = event.target.result;  }
    reader.readAsDataURL( file );
}







function UpdatePreviewCanvas()
{
    var img = this;
    var canvas = document.getElementById( 'previewcanvas' );

    if( typeof canvas === "undefined" 
        || typeof canvas.getContext === "undefined" )
        return;

    var context = canvas.getContext( '2d' );

    var world = new Object();
    world.width = canvas.offsetWidth;
    world.height = canvas.offsetHeight;

    canvas.width = world.width;
    canvas.height = world.height;

    if( typeof img === "undefined" )
        return;

    var WidthDif = img.width - world.width;
    var HeightDif = img.height - world.height;

    var Scale = 0.0;
    if( WidthDif > HeightDif )
    {
        Scale = world.width / img.width;
    }
    else
    {
        Scale = world.height / img.height;
    }
    if( Scale > 1 )
        Scale = 1;

    var UseWidth = Math.floor( img.width * Scale );
    var UseHeight = Math.floor( img.height * Scale );

    var x = Math.floor( ( world.width - UseWidth ) / 2 );
    var y = Math.floor( ( world.height - UseHeight ) / 2 );

    context.drawImage( img, x, y, UseWidth, UseHeight );  
}

/********************************************  End show image preview before upload function ***************************/

