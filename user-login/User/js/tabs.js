$(function(){
	tabs.init();
});	
tabs = {
	init : function(){
		$('.tabs').each(function(){
			$(this).find('.tab-content').hide();
			$($(this).find('ul.nav .selected a').attr('href')).fadeIn(300);
			$(this).find('ul.nav a').click(function(){
				$(this).parents('.tabs').find('.tab-content').hide();
				$($(this).attr('href')).fadeIn(300);
				$(this).parent().addClass('selected').siblings().removeClass('selected');
				return false;
			});
		});
	}
}

$(function(){
	tabs2.init2();
});	
tabs2 = {
	init2 : function(){
		$('.dashboardtabs').each(function(){
			$(this).find('.dashboardtabscontent').hide();
			$($(this).find('ul.nav_div .selected a').attr('href')).fadeIn(300);
			$(this).find('ul.nav_div a').click(function(){
				$(this).parents('.dashboardtabs').find('.dashboardtabscontent').hide();
				$($(this).attr('href')).fadeIn(300);
				$(this).parent().addClass('selected').siblings().removeClass('selected');
				return false;
			});
		});
	}
}


$(function(){
	tabs1.init1();
});	
tabs1 = {
	init1 : function(){
		$('.itinerarytabs').each(function(){
			$(this).find('.itinerarycontent').hide();
			$($(this).find('ul.navdiv .selected a').attr('href')).fadeIn(300);
			$(this).find('ul.navdiv a').click(function(){
				$(this).parents('.itinerarytabs').find('.itinerarycontent').hide();
				$($(this).attr('href')).fadeIn(300);
				$(this).parent().addClass('selected').siblings().removeClass('selected');
				return false;
			});
		});
	}
}