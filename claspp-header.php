<?php error_reporting(E_ALL & ~E_NOTICE) ?>

<html lang="en-US">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta name="description" content="Claspp is a social media advertising platform that empowers publishers to promote what they like and engage their audiences while making money." />
        <meta name="keywords" content="Claspp, Claspp.com, video advertising, cost-per-click advertising, cpc advertising, internet advertising, publishers, advertising" />
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
        <title> Claspp</title>

        <link rel='stylesheet' id='rs-settings-css'  href='rs-plugin/css/settings.css' type='text/css' media='all' />

        <link rel='stylesheet' id='rs-captions-css'  href='rs-plugin/css/dynamic-captions.css' type='text/css' media='all' />

        <link rel='stylesheet' id='rs-plugin-static-css'  href='rs-plugin/css/static-captions.css' type='text/css' media='all' />

        <link rel='stylesheet' id='wpex-style-css'  href='claspp-style.css' type='text/css' media='all' />

        <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">

        <link rel="stylesheet" type="text/css" media="screen" href="css/grid_12.css">

        <script type='text/javascript' src='js/jquery.js'></script>

        <script type='text/javascript' src='js/jquery-migrate.min.js'></script>

        <script type='text/javascript' src='rs-plugin/js/jquery.themepunch.plugins.min.js'></script>

        <script type='text/javascript' src='rs-plugin/js/jquery.themepunch.revolution.min.js'></script>

        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>


        <script type="text/javascript" language="javascript" src="js/jquery.carouFredSel-6.2.1-packed.js"></script>

        <!-- fire plugin onDocumentReady -->
        <script type="text/javascript" language="javascript">
            jQuery(function() {

                //	Basic carousel + timer, using CSS-transitions
                jQuery('#foo1').carouFredSel({
                    auto: {
                        pauseOnHover: 'resume',
                        progress: '#timer1'
                    }
                }, {
                    transition: true
                });

            });
        </script>

        <style type="text/css">
            body, #navbar li[class^="icon-"] > a,#navbar li[class*=" icon-"] > a{
                font-family:'Open Sans';
            }
        </style>
        <style type="text/css">
            h1,h2,h3,h4,h5,h6, .page-header-title{font-family:'Molengo'; }
        </style>
        <link href="http://fonts.googleapis.com/css?family=Open%20Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&amp;subset=latin,cyrillic-ext,greek-ext,greek,vietnamese,latin-ext,cyrillic" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Molengo:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&amp;subset=latin,cyrillic-ext,greek-ext,greek,vietnamese,latin-ext,cyrillic" rel="stylesheet" type="text/css">
        <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <!--[if IE 7]><link rel="stylesheet" type="text/css" href="http://espylink.com/claspp/wp-content/themes/thunder/css/font-awesome-ie7.min.css" media="screen" /><![endif]-->
        <!-- Custom CSS -->
        <style type="text/css">
            .logo img { margin-top: 7px; }body .logo-icon { border: none; padding: 0; }body a { color: #6d6d6d; }#navbar .dropdown-menu > li > a:hover, #navbar .dropdown-menu > .current-menu-item > a, #navbar .dropdown-menu .current-menu-parent > a, #navbar .dropdown-menu .current-menu-parent > a:hover, #navbar .dropdown-menu > .current-menu-item > a:hover, .single-portfolio #navbar .dropdown-menu li.portfolio-link a, .single-services #navbar .dropdown-menu li.services-link a { color:#0a0a0a!important; }.wcmenucart-contents:hover{border-color: #0a0a0a !important; }.portfolio-archives-filter li a:hover { background-color:#686868!important; }.portfolio-post-pagination li a:hover { background-color:#7a7a7a!important; }.services-archives-tabs li a:hover { background-color:#5e5e5e!important; }.services-post-slider .flex-direction-nav a:hover { background-color:#828282; }.gallery-format-post-slider .flex-direction-nav a:hover { background-color:#727272!important; }#sidebar .widget_nav_menu .current-menu-item a { background-color:#686868!important; }.theme-button:hover, button:hover, input[type="button"]:hover, input[type="submit"]:hover, #commentsbox input[type="submit"]:hover, .symple-recent-posts-entry-readmore:hover { background-color:#a3b786!important; }.client-entry a, .client-entry, .home .client-entry { padding: 0; border: none; background: none; }.site-main .wpb_tour .wpb_tabs_nav li a:hover { background-color:#515151 !important; }.site-main .wpb_tour .wpb_tabs_nav li.ui-tabs-active a { background-color: #79C277; }
        </style>

    </head>

    <!-- Begin Body -->
    <body class="home page page-id-7 page-template page-template-templateshome-php wpex-theme full-width-main-layout default-main-skin light-green-theme-skin wpex-responsive page-with-slider wpb-js-composer js-comp-ver-3.7.1 vc_responsive">

        <div id="wrap" class="clr">

            <!-- Top Bar -->


            <!-- Header  -->
            <header id="masthead" class="site-header clr fixed-scroll" role="banner">
                <div id="masthead-inner" class="container clr">
                    <div class="logo">
                        <a href="<?php echo $site_url; ?>" title="Claspp" rel="home">
                            <img src="images/logo.png" alt="" /></a>
                    </div><!-- .logo -->
                    <div id="navbar" class="clr">
                        <nav id="site-navigation" class="navigation main-navigation clr" role="navigation">
                            <div class="menu-main-container">
                                <ul id="menu-main" class="nav-menu dropdown-menu">
                                    <li id="menu-item-27" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-7 current_page_item menu-item-27">
                                        <a href="<?php echo $site_url; ?>">Home</a>
                                    </li>
                                    <li id="menu-item-25" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-25">
                                        <a href="http://claspp.com/blog/">Blog</a>
                                    </li>
                                    <li id="menu-item-30" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-30">

                                        <?php if (($_SESSION['log'] == 'true') || ($_SESSION['userlog'] == 'true') || ($_SESSION['advertiserlog'] == 'true')) { ?>
                                            <a href="<?php echo $site_url; ?>logout.php">Logout</a>
                                        </li>
                                        <li id="menu-item-31" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-31">
                                            <a href="<?php echo $site_url; ?>redirect-dashboard.php">Back to dashboard</a>
                                        <?php } else { ?>
                                            <a href="<?php echo $site_url; ?>claspp-login.php">Log In</a>
                                                                                <li id="menu-item-25" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-25">
                                        <a href="<?php echo $site_url; ?>usersignup.php">Sign Up</a>
                                    </li>
                                        <?php } ?>                                
                                    </li>
                                </ul>
                            </div>
                        </nav><!-- #site-navigation -->
                    </div><!-- #navbar -->
                </div><!-- #masthead-inner -->
            </header><!-- #masthead-two -->

