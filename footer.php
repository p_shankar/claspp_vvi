<?php
/**
 * over footer.php with new footer file
 */
return include 'claspp-footer.php'
?>

<footer>
    <div class="top_footer">
        <div class="wrapper_main">
            <div class="container_12">
                <div class="grid_13">
                    <div class="grid_3 suffix_1">
                        <div class="footer_heading">
                            <h3>latest tweets</h3>
                        </div>
                        <div class="tweetdiv" id="twitter_update_list">
                         <?php
					 require_once 'twitter/twitteroauth.php';
     $twitterConnection = new TwitterOAuth(
					'QuhuYfKpU7d23Vjd4QNBQ',	// Consumer Key
					'HjR8l2bG61RrLDdjLtkZHMP4HtVICcJIVFyzItE4NFc',   	// Consumer secret
					'1468901779-67kqWrmaIROuUgXnQuq6T4Pmpbasl7kt1HMreur',       // Access token
					'Df7Ot1JXLnzykb4cWJIUvbJ8FeiulqsV7US1l6kshDo'    	// Access token secret
					);


$twitterData = $twitterConnection->get('statuses/user_timeline', array('screen_name'     => 'Espylink','count' => '3', 'exclude_replies' => false));

echo "<ul>";
$l = "1";
foreach($twitterData as $tweets)
{
	if($l=="3") {
echo "<li class='border_none' >".$tweets->text."</br>by <a href='https://twitter.com//Espylink' target='_blank' > Claspp</a> on ".date('Y-m-d',strtotime($tweets->created_at))."</li>";
	} else {

echo "<li>".$tweets->text." </br>by <a href='https://twitter.com/Espylink' target='_blank'> Claspp</a> on ".date('Y-m-d',strtotime($tweets->created_at))."</li>";

	}
$l++;
}

echo "</ul>";
 ?>
                        </div>
                    </div>
                    <div class="grid_3 suffix_1">
                        <div class="footer_heading">
                            <h3>online support</h3>
                        </div>
                        <div class="online_support"> <img src="images/user_help.png" alt="Online Support">
                            <div class="online_number">
                                 <h4></h4>
                                <p>Contact:
                                <a href="mailto:info@espylink.com">info@Claspp.com</a></p>
                            </div>
                        </div>
                    </div>

                    <div class="grid_2 suffix_1">
                        <div class="footer_heading">
                            <h3>Links</h3>
                        </div>
                        <div class="navigationdiv">
                            <ul>
								<li><a href="index.php">Home</a></li>
								<li><a href="about.php">About Us</a></li>
								<li><a href="faq.php">FAQ</a></li>                         
								<li><a href="contact.php">Contact Us</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="grid_3 flr">
                        <div class="footer_heading">
                         <h3>Blog</h3>
                        </div>
                        <div class="newsletterdiv">
                            <p>Subscribe for Blog:</p>
                            <!---------validation script--------->

                            
                            <script type="text/javascript" >
                                $(function() {
                                    $(".subscribebutton").click(function() {
                                        var name = $("#name").val();
                                        var dataString = 'name='+ name ;
                                        var x=document.forms["news"]["name"].value
                                        var atpos=x.indexOf("@");
                                        var dotpos=x.lastIndexOf(".");
                                        if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
                                        {
                                            $('.success').fadeOut(200).hide();
                                            $('.error').fadeOut(200).show();
                                        }
                                        else
                                        {
                                            $.ajax({
                                                type: "POST",
                                                url: "subscription.php",
                                                data: dataString,
                                                success: function(abc){
												    if(abc== 1){
						             			    $('.error_r').fadeIn(200).show();
													 $('.success').fadeOut(200).hide();
													 }
													 else
														 {

													$('.success').show();
													$('.error_r').hide();
													$('.error').hide();
										

													 }
                                                }
                                            });
                                        }
                                        return false;
                                    });
                                });
                            </script>

                            <!----------script end------------------>


                            <div class="newsletter_inputdiv">
                                <?php
                                $current_page_URL = $_SERVER["SERVER_NAME"];
                                ?>
                                <form name="news" method="post" onsubmit="return validateForm();">
                                    <input type="text" id="name" name="name_subscrip" onclick="this.value='';" onfocus="this.select()" onblur="this.value=!this.value?'Enter Email To Get Updates':this.value;" value="Enter Email To Get Updates" />
                                    <input type="submit" src="images/subcribebutton.png" value="subscribe" name="submit" class="subscribebutton">
                                    <span class="error" style="display:none"> Please Enter Valid Email</span>
                                    <span class="success" style="display:none"> Subscribed Successfully</span>
									  <span class="error_r" style="display:none"> Email Already Exists </span>
                                </form>

                            </div>

       
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="lower_footer">
        <div class="wrapper_main">
            <div class="container_12">
                <div class="grid_13">
                    <div class="grid_7">
                        <div class="footer_logo"><img src="images/footerlogo.png" alt="EspyLink"></div>
                        <div class="copyright_div">
                            <p>&copy; 2013. All Rights Reserved to <a href="<?php  echo $site_url;  ?>" target="_blank">Claspp.com </a></p>
                        </div>
                    </div>
                    <div class="grid_5 flr">
                        <div class="termandpolicy">
                            <ul>
                                <li><a href="terms.php">Terms of Use</a></li>
                                <li>|</li>
                                <li><a href="privacy.php">Privacy Policy</a></li>
                                <!--<li class=""><a href="contact.php">Contact Us</a></li>
                                <li>|</li>
                                <li class=""><a href="faq.php">Help</a></li>-->

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>