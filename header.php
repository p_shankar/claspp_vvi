<?php
/**
 * over header.php with new header file
 */
return include 'claspp-header.php'
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Claspp</title>
    <meta charset="utf-8">
    <meta name="description" content="Espylink is a social media advertising platform that empowers publishers to promote what they like and engage their audiences while making money." />
    <meta name="keywords" content="Espylink, Espylink.com, video advertising, cost-per-click advertising, cpc advertising, internet advertising, publishers, advertising" />
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">


    <link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/grid_12.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/nivo-slider.css">
    
    <!--<link rel="stylesheet" type="text/css" media="screen" href="Lato_Font_Kit/stylesheet.css">-->
    
    
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,700,500italic,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.nivo.slider.pack.js"></script>
    <script type="text/javascript">
        $(window).load(function() {
            $('#slider').nivoSlider();
        });



    </script>

    <!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
<img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
</a>
</div>
<![endif]-->
<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">


<![endif]-->
    <!--script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script-->
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>


</head>
<body>

<header>
    <div class="wrapper_main">
        <div class="container_12">
            <h1 class="logo"><a href=<?php echo "index.php";?>>
                    <img src="images/logo.png" alt="EspyLink">
            </a></h1>
            <div class="grid_4 flr">
                 <div class="social_menu">
                    <ul>
                        <li><a href="http://www.facebook.com/pages/Espylink/440494809349141?ref=stream" target="_blank" class="facebook"></a></li>
                        <li><a href="http://twitter.com/Espylink" target="_blank" class="twitter"></a></li>
                        <li><a href="https://plus.google.com" target="_blank" class="googleplus"></a></li>
                     <li><a href="http://www.linkedin.com/company/2969287?trk=tyah" target="_blank" class="linkedin"></a></li>
                        <li><a href="http://www.pinterest.com/espylink" target="_blank" class="pinterest"></a></li>
                    </ul>
                </div>
            </div>
            <!--<div class="grid_13">
                <div class="social_menu">
                    <ul>
                        <li><a href="http://www.facebook.com/pages/Espylink/440494809349141?ref=stream" target="_blank" class="facebook"></a></li>
                        <li><a href="http://twitter.com/Espylink" target="_blank" class="twitter"></a></li>
                        <li><a href="https://plus.google.com" target="_blank" class="googleplus"></a></li>
                     <li><a href="http://www.linkedin.com/company/2969287?trk=tyah" target="_blank" class="linkedin"></a></li>
                        <li><a href="http://www.pinterest.com/espylink" target="_blank" class="pinterest"></a></li>
                    </ul>
                </div>
            </div>-->

        </div>
    </div>
</header>

<?php include('config.php'); ?>
