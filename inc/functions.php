<?php


/***
 * mail function for registration success
 * @param <email> $email_id
 * @param <string> $uname
 * @param $img_url
 *
 */
if(!function_exists('reg_success')){
    function reg_success($email_id, $name, $img_url,$password,$type,$subject,$content,$header_email,$footer,$disclaimer,$gen_promo_code){

        $to  = ''.$email_id.'' . ', '; // note the comma
		
		if(empty($subject)){

        $subject = 'Registered successfully on claspp.';
        }      
           

   $message="<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
    <head>
        <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
        <title>Registered successfully on claspp</title>
    </head>

    <body style='width:800px; background-repeat:no-repeat; background-position:right'>
  <!----> <div style='background-color:#fff;'>
  <!--[if gte mso 9]>
  <v:background xmlns:v='urn:schemas-microsoft-com:vml' fill='t' style='display:inline-block; top:100;left:200;' >
    <v:fill type='tile' src='http://www.claspp.com/beta/images/class_main_bg.png' color='#fff'/>
  </v:background>
  <![endif]-->
  <table height='100%' width='100%' cellpadding='0' cellspacing='0' border='0'>
    <tr>
      <td valign='top' align='left' background='http://www.claspp.com/beta/images/class_main_bg.png' style='background-repeat: no-repeat;background-position-x:390px;background-position-y:100px;'>
<!---->
        <table width='100%' cellspacing='0' cellpadding='0' style='padding:20px'>
       
            <tr>
                <td>
                    <a href='#'><img src='http://www.claspp.com/beta/images/class_logo.png' /></a>
                </td>
            </tr>
            <tr>
                <td style='color: rgb(146, 162, 63); font-family: helvetica;display:block; font-size: 43.77px; font-weight: bold;padding: 30px 0 0; float:left;' width='59%'>
                   ".$header_email."
                </td>
            </tr>
            <tr>
                <td style='color: rgb(146, 162, 63); font-family: helvetica;display:block; font-size: 41.77px; font-weight: bold;padding: 20px 0 0; float:left;' width='59%'>
                   Hello  ".$name.",
                </td>
            </tr>
            <tr>
                <td style='color: rgb(0, 0, 0); font-family: helvetica;display:block; font-size: 24px;font-weight: bold; padding: 30px 0px 0px; float: left; ' width='59%'>
                   You are registered successfully as a ".$type.".
                </td>
            </tr>
            <tr>
                <td style='color: rgb(0, 0, 0); font-family: helvetica;display:block; font-size: 24px;font-weight: bold; padding: 10px 0px 0px; float: left; ' width='59%'>
                    Your log-in Id is:&nbsp; ".$email_id."
                </td>
            </tr>

            <tr>
                <td style='color: rgb(0, 0, 0); font-family: helvetica;display:block; font-size: 24px;font-weight: bold; padding: 30px 0px 0px; float: left; ' width='59%'>
                   ".$content."
                </td>
            </tr>
            <tr>
                <td style='color: rgb(0, 0, 0); font-family: helvetica;display:block; font-size: 24px;font-weight: bold; padding: 30px 0px 0px; float: left; ' width='59%'>
                  <span >Your Promo Code is: ".$gen_promo_code."</span>
                </td>
            </tr>
             <tr>
                <td style='color: rgb(0, 0, 0); font-family: helvetica;display:block; font-size: 16px;font-weight: bold; padding: 30px 0px 0px; float: left; ' width='59%'>
                  ".$footer."
                </td>
            </tr>
            <tr>
                <td style='color: rgb(0, 0, 0); font-family: helvetica;display:block; font-size: 16px;font-weight: bold; padding: 10px 0px 0px; float: left; width: 59%;'>
                   Copyright &copy; 2014 <a href ='http://www.claspp.com/' target='_blank' style='color: rgb(146, 162, 63);text-decoration:none;'>claspp.com</a>
                </td>
            </tr>
            <tr>
                <td style='color: rgb(0, 0, 0); font-family: helvetica;display:block; font-size: 16px;font-weight: bold; padding: 10px 0px 0px; float: left; width: 59%;'>
                   ".$disclaimer."
                </td>
            </tr>
        </table>
 <!----></td>
    </tr>
  </table>
</div><!---->
    </body>
</html>

";
   
   

        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .='From: Claspp'. "\r\n";
//echo $message;
//die();
        mail($to, $subject, $message, $headers);

    }

}


/**
 *@function to get list of all ads
 *
 * @return array
 *
 */

if(!function_exists('get_campaign_list')){
    function get_campaign_list(){

        $query = 'select
                        cp.id,
                        cp.title,
                        cp.url ,
                        cp.description ,
                        cp.target_category,
                        cp.campaign_image,
                        cp.social_media,
                        cpb.per_post_payment
                from
                    espy_campaign as cp
                inner join
                    espy_campaign_budget as cpb
                on
                    cp.id = cpb.campaign_id
                where
                    cp.status = "1"
					 and DATEDIFF(CURDATE(), cp.end_date) < 1  and DATEDIFF(cp.start_date, CURDATE()) < 1
                ';

        $result = mysql_query($query);

        return $result ;
        
    }
}

/**
*
 *
 *
 *
 */
if(!function_exists('campaign_sort_by_cat')){
    function campaign_sort_by_cat($cat = 0, $city = 0){

        $query = 'select
                        cp.id,
                        cp.title,
                        cp.url ,
                        cp.description ,
                        cp.target_category,
                        cp.target_city,
                        cp.social_media,
                        cp.campaign_image,
						cpb.budget,
						cpb.impressions,
						cpb.projected_posts,
						cpb.posted,
						cpb.remaining_posts,
                        cpb.per_post_payment
                from
                    espy_campaign as cp
                 left join
                    espy_campaign_budget as cpb
                on
                    cp.id = cpb.campaign_id
                where
                    cp.status = "1"  and DATEDIFF(CURDATE(), cp.end_date) < 1  and DATEDIFF(cp.start_date, CURDATE()) < 1';

               if((($cat != 0) || ($city != 0)) && ($city != -1) || (($cat != 0) && ($city != 0)) && ($city == -1)){
                    $query .=  ' and ' ;
                }

        if($cat != 0){
            $query .=      'cp.target_category = "'.$cat.'"';
        }

        if((($cat != 0) && ($city != 0))  && $city != -1) {
            $query .=  ' and ' ;
        }
        if($city != 0 && $city != -1){
                $query .= 'cp.target_city = "'.$city.'"';
        }


    
        $result = mysql_query($query);

        return $result ;

    }
}

 /*    function to display totaol user on advertiser dashboard 
 Created by harpartap singh
On 16 march 2013 
*/
function total_user(){



$sql = "SELECT * FROM `ad_signup` WHERE type='user' ";

$result = mysql_query($sql);
 $num_rows = mysql_num_rows($result);
 return   $num_rows;
}


 /*    function to display total impression per month on advertiser dashboard 
 Created by harpartap singh
On 16 march 2013 
*/
function total_impression(){

	$current_month=date('n');   

	$sql = "SELECT sum(eb.impression) as sum  FROM  espy_campaign  as ec LEFT JOIN espy_postedads as eb ON
	ec.id =eb.pads_adid where MONTH(eb.pads_posteddate)='".$current_month."' ";

	$result = mysql_query($sql); 
	$row = mysql_fetch_assoc($result); 
	$sum = $row['sum'];
	return $sum; 

}




function subscription_success($email_id){

        $to  = ''.$email_id.'' . ', '; // note the comma

        $subject = 'Thanks for subscribed for Newsletter ';

  

   $message="<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
    <head>
        <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
        <title>Subscribed for Newsletter</title>
    </head>

    <body style='width:800px; background-repeat:no-repeat; background-position:right'>
      <!----> <div style='background-color:#fff;'>
      <!--[if gte mso 9]>
      <v:background xmlns:v='urn:schemas-microsoft-com:vml' fill='t' style='display:inline-block; top:100;left:200;' >
        <v:fill type='tile' src='http://www.claspp.com/beta/images/class_main_bg.png' color='#fff'/>
      </v:background>
      <![endif]-->
      <table height='100%' width='100%' cellpadding='0' cellspacing='0' border='0'>
        <tr>
          <td valign='top' align='left' background='http://www.claspp.com/beta/images/class_main_bg.png' style='background-repeat: no-repeat;background-position-x:390px;background-position-y:100px;'>
    <!---->
        <table width='100%' cellspacing='0' cellpadding='0' style='padding:20px'>
           
            <tr>
                <td>
                    <a href='#'><img src='http://www.claspp.com/beta/images/class_logo.png' /></a>
                </td>
            </tr>
            <tr>
                <td style='color: rgb(146, 162, 63); font-family: helvetica;display:block; font-size: 41.77px; font-weight: bold;padding: 50px 0 0; float:left;' width='59%'>
                     Welcome To Claspp
                </td>
            </tr>
            <tr>
                <td style='color: rgb(0, 0, 0); font-family: helvetica;display:block; font-size: 24px;font-weight: bold; padding: 30px 0px 0px; float: left; ' width='59%'>
                    You  are successfully subscribed for newsletter.
                </td>
            </tr>

             <tr>
                <td style='color: rgb(0, 0, 0); font-family: helvetica;display:block; font-size: 16px;font-weight: bold; padding: 30px 0px 0px; float: left; ' width='59%'>
                   Thanks for Using Claspp.
                </td>
            </tr>
            <tr>
                <td style='color: rgb(0, 0, 0); font-family: helvetica;display:block; font-size: 16px;font-weight: bold; padding: 10px 0px 0px; float: left; ' width='59%'>
                   Copyright &copy; 2014 claspp.com
                </td>
            </tr>
        </table>
 <!----></td>
    </tr>
  </table>
</div><!---->
    </body>
</html>

";
   
   

        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .='From: Claspp'. "\r\n";

        mail($to, $subject, $message, $headers);

    }


	/************************** Contact us email ***************************/
 if(!function_exists('email_contact')){
    function email_contact($name, $email,$business,$role, $role1,$textarea){

$admin_email=mysql_query("select * from admin");

 $admin_e = mysql_fetch_array($admin_email);

       $to  = $admin_e['paypal_email']; // note the comma
		
	

        $subject = 'Contact email from claspp.';
      
           

   $message="<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
    <head>
        <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
        <title>Contact email from Claspp</title>
    </head>

    <body style='width:800px; background-repeat:no-repeat; background-position:right'>
          <!----> <div style='background-color:#fff;'>
          <!--[if gte mso 9]>
          <v:background xmlns:v='urn:schemas-microsoft-com:vml' fill='t' style='display:inline-block; top:100;left:200;' >
            <v:fill type='tile' src='http://www.claspp.com/beta/images/class_main_bg.png' color='#fff'/>
          </v:background>
          <![endif]-->
          <table height='100%' width='100%' cellpadding='0' cellspacing='0' border='0'>
            <tr>
              <td valign='top' align='left' background='http://www.claspp.com/beta/images/class_main_bg.png' style='background-repeat: no-repeat;background-position-x:390px;background-position-y:100px;'>
        <!---->
        <table width='100%' cellspacing='0' cellpadding='0' style='padding:20px'>
           
            <tr>
                <td>
                    <a href='#'><img src='http://www.claspp.com/beta/images/class_logo.png' /></a>
                </td>
            </tr>
            <tr>
                <td style='color: rgb(146, 162, 63); font-family: helvetica;display:block; font-size: 41.77px; font-weight: bold;padding: 30px 0 0; float:left;' width='59%'>
                    Name:&nbsp;  ".$name.",
                </td>
            </tr>
            <tr>
                <td style='color: rgb(0, 0, 0); font-family: helvetica;display:block; font-size: 24px;font-weight: bold; padding: 30px 0px 0px; float: left; ' width='59%'>
                    Email Id :&nbsp; ".$email.".</td>
                     <td style='color: rgb(0, 0, 0); font-family: helvetica;display:block; font-size: 24px;font-weight: bold; padding: 10px 0px 0px; float: left; ' width='59%'>
                   Address  :&nbsp; ".$business."
                   </td>
                     <td style='color: rgb(0, 0, 0); font-family: helvetica;display:block; font-size: 24px;font-weight: bold; padding: 10px 0px 0px; float: left; ' width='59%'>
                    AD Request :&nbsp;".$role."
                    </td>
                     <td style='color: rgb(0, 0, 0); font-family: helvetica;display:block; font-size: 24px;font-weight: bold; padding: 10px 0px 0px; float: left; ' width='59%'>
                    Issues with Website :&nbsp;".$role1."
                    </td>


                </td>
            </tr>
             <tr>
                <td style='color: rgb(0, 0, 0); font-family: helvetica;display:block; font-size: 24px;font-weight: bold;padding: 20px 0 0; float:left;' width='59%'>
                    ".$textarea."
                </td>
            </tr>
             <tr>
                <td style='color: rgb(0, 0, 0); font-family: helvetica;display:block; font-size: 16px;font-weight: bold; padding: 30px 0px 0px; float: left; ' width='59%'>
                   Thanks for Using Claspp.
                </td>
            </tr>
            <tr>
                <td style='color: rgb(0, 0, 0); font-family: helvetica;display:block; font-size: 16px;font-weight: bold; padding: 10px 0px 0px; float: left; ' width='59%'>
                   Copyright &copy; 2014 claspp.com
                </td>
            </tr>
             <tr>
                <td style='color: rgb(0, 0, 0); font-family: helvetica;display:block; font-size: 16px;font-weight: bold;padding: 10px 0 0; float:left;' width='59%'>
                   ".$disclaimer."
                </td>
            </tr>
        </table>
 <!----></td>
    </tr>
  </table>
</div><!---->
    </body>
</html>

";
   
   

        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .='From: Claspp'. "\r\n";
//echo $message;
//die();
        mail($to, $subject, $message, $headers);

    }

}



/********************************   Coupon Details ***********************/

function  coupon_details($Comp_id) {

$query = "SELECT * FROM espy_campaign  WHERE id=". $Comp_id."";
$result = mysql_query($query);
$comapningdata = mysql_fetch_assoc($result);
return  $comapningdata;

}


/************************  Front end functions *****************/


Function send_forgot_email($to) {

		$to = $to;

			$sql = "select * from ad_signup where email ='" . $to . "'";
			$res = mysql_query($sql);
			$random = mysql_fetch_assoc($res);




			if ($random > 0){


			$subject = 'Claspp Forgot Password.';



			$message="<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
			<html xmlns='http://www.w3.org/1999/xhtml'>
			<head>
			<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
			<title>Claspp forgot password</title>
			</head>

			<body style='width:800px; background-repeat:no-repeat; background-position:right'>
			<!----> <div style='background-color:#fff;'>
			<!--[if gte mso 9]>
			<v:background xmlns:v='urn:schemas-microsoft-com:vml' fill='t' style='display:inline-block; top:100;left:200;' >
			<v:fill type='tile' src='http://www.claspp.com/beta/images/class_main_bg.png' color='#fff'/>
			</v:background>
			<![endif]-->
			<table height='100%' width='100%' cellpadding='0' cellspacing='0' border='0'>
			<tr>
			<td valign='top' align='left' background='http://www.claspp.com/beta/images/class_main_bg.png' style='background-repeat: no-repeat;background-position-x:390px;background-position-y:100px;'>
			<!---->

			<table width='100%' cellspacing='0' cellpadding='0' style='padding:20px;'>

			<tr>
			<td>
			<a href='#'><img src='http://www.claspp.com/beta/images/class_logo.png' /></a>
			</td>
			</tr>
			<tr>
			<td style='color: rgb(146, 162, 63); font-family: helvetica;display:block; font-size: 41.77px; font-weight: bold;padding: 30px 0 0; float:left;' width='59%'>
			Hello   ".$random['name']."&nbsp;".$random['last_name'].",
			</td>
			</tr>
			<tr>
			<td style='color: rgb(0, 0, 0); font-family: helvetica;display:block; font-size: 24px;font-weight: bold; padding: 30px 0px 0px; float: left; ' width='59%'>
			New password requested for account. If you did not request this password, please ignore this Email.
			</td>
			</tr>
			<tr>
			<td style='color: rgb(0, 0, 0); font-family: helvetica;display:block; font-size: 24px;font-weight: bold; padding: 30px 0px 0px; float: left; ' width='59%'>
			Click on the link to change your password:
			<a href='http://www.claspp.com/beta/changepassword.php?user=".base64_encode($random["forgot_random"])."'  style='color: rgb(146, 162, 63);text-decoration:none; '>http://www.claspp.com/beta/changepassword.php?user=".base64_encode($random["forgot_random"])."</a>
			</td>
			</tr>

			<tr>
			<td style='color: rgb(0, 0, 0); font-family: helvetica;display:block; font-size: 16px;font-weight: bold; padding: 30px 0px 0px; float: left; ' width='59%'>
			Thanks for Using Claspp.
			</td>
			</tr>
			<tr>
			<td style='color: rgb(0, 0, 0); font-family: helvetica;display:block; font-size: 16px;font-weight: bold; padding: 10px 0px 0px; float: left; ' width='59%'>
			Copyright &copy; 2014 claspp.com
			</td>
			</tr>
			</table>

			<!----></td>
			</tr>
			</table>
			</div><!---->

			</body>
			</html>

			";

			$message;
			$headers = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= 'From: claspp.com' . "\r\n" ;

			$mail_sent = mail($to, $subject, $message, $headers);

				return true;


			} else {


            
			return false;

			}





}


/**********  Function to check user login **********/

       function check_user($email,$pass) {

		$sql = "select * from ad_signup where email ='" . $email . "' and password='" . md5($pass) . "' and status!='no' ";

		$res = mysql_query($sql);
		$rs = mysql_fetch_assoc($res);

        return $rs; 
	   }